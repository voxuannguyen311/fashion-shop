<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\BaiVietController;
use App\Http\Controllers\BinhLuanController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\GioHangController;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\LichSuMuaHang;
use App\Http\Controllers\ThanhToanControllerr;
use App\Http\Controllers\LienHeController;
use App\Http\Controllers\PayController;
use App\Http\Controllers\QuanLyBaiVietController;
use App\Http\Controllers\SanphamController;
use App\Http\Controllers\TheLoaiController;
use App\Http\Controllers\ThongKe;
use Illuminate\Support\Facades\Route;

Route::get('/update-password/{hash}', [CustomerController::class, 'viewUpdatePassword']);
Route::post('/update-password', [CustomerController::class, 'actionUpdatePassword']);
// quên mật khẩu
Route::post('/reset-password', [CustomerController::class, 'actionResetPassword']);
// đăng nhập đăng ký
Route::get('/login_register', [CustomerController::class, 'viewRegister']);
Route::post('/register', [CustomerController::class, 'actionRegister']);
Route::post('/login', [CustomerController::class, 'actionLogin']);
Route::get('/active/{hash}', [CustomerController::class, 'actionActive']);

// end đăng nhập đăng ký
Route::get('/chi-tiet-san-pham/{theloai}/{slug}/{id}', [HomepageController::class, 'chiTietSanPham']);
Route::get('/the-loai/{id}', [HomepageController::class, 'TheLoai']);

Route::get('/', [HomepageController::class, 'home']);

// logout
Route::get('/logout', [CustomerController::class, 'actionLogout']);
//

Route::get('/cua-hang', [HomepageController::class, 'SanPham']);

Route::get('/bai-viet', [BaiVietController::class, 'BaiViet']);
Route::get('/chi-tiet-bai-viet/{id}', [BaiVietController::class, 'ChiTietBaiViet']);

Route::get('/gioi-thieu', [HomepageController::class, 'GioiThieu']);
Route::get('/lien-he', [HomepageController::class, 'LienHe']);
Route::post('/gui-lien-he', [HomepageController::class, 'GuiLienHe']);

Route::post('/tim-kiem', [HomepageController::class, 'actionTimKiem']);

Route::group(['prefix' => '/client', 'middleware' => 'loginCustomer'], function () {
    Route::get('/cap-nhap-thong-tin', [CustomerController::class, 'viewCapNhapThongTin']);
    Route::get('/thong-tin-nguoi-dang-nhap', [CustomerController::class, 'thong_tin_nguoi_dang_nhap']);
    Route::post('/cap-nhap-thong-tin', [CustomerController::class, 'capNhapThongTin']);

    Route::get('/cap-nhap-mat-khau', [CustomerController::class, 'viewCapNhapMatKhau']);
    Route::post('/cap-nhap-mat-khau', [CustomerController::class, 'capNhapMatKhau']);

  // giỏ hàng
  Route::get('/hien-thi-ds-gio-hang', [GioHangController::class, 'HienThiDsGioHang']);
  Route::post('/them-so-luong/{id}', [GioHangController::class, 'ThemSoLuong']);
  Route::post('/tru-so-luong/{id}', [GioHangController::class, 'TruSoLuong']);
  Route::post('/xoa-san-pham-gio-hang/{id}', [GioHangController::class, 'XoaSanPhamGioHang']);
  Route::post('/mua-hang-ngay/{id}', [GioHangController::class, 'MuaHangNgay']);
  //thanh toán
  Route::post('/thanh-toan', [ThanhToanControllerr::class, 'ThanhToan']);
  Route::get('/lich-su-mua-hang', [ThanhToanControllerr::class, 'LichSuMuaHang']);
  Route::get('/ds-lich-su-mua-hang', [ThanhToanControllerr::class, 'DsLichSuMuaHang']);


  // cmt
  Route::post('/binh-luan-san-pham', [BinhLuanController::class, 'BinhLuan']);


});


Route::get('/gio-hang', [GioHangController::class, 'GioHang']);
Route::get('/thanh-toan', [GioHangController::class, 'ThanhToan']);
// admin
Route::get('/admin/login', [AdminController::class, 'viewLogin']);
Route::post('/admin/login', [AdminController::class, 'actionLogin']);
Route::get('/admlogout', [AdminController::class, 'actionLogout']);

Route::group(['prefix' => '/admin', 'middleware' => 'loginAdmin'], function () {

    Route::get('/', [AdminController::class, 'viewHome']);

    Route::group(['prefix' => '/san-pham'], function () {
        Route::get('/index', [SanphamController::class, 'index']);
        Route::post('/index', [SanphamController::class, 'store']);
        Route::get('/data', [SanphamController::class, 'getData']);
        Route::post('/update', [SanphamController::class, 'update']);
        Route::post('/delete', [SanphamController::class, 'destroy']);
        Route::get('/change-status/{id}', [SanphamController::class, 'changeStatus']);
    });

    Route::group(['prefix' => '/khach-hang'], function() {
        Route::get('/thong-tin', [CustomerController::class, 'viewThongTin']);
        Route::get('/data', [CustomerController::class, 'getData']);
        Route::post('/update', [CustomerController::class, 'update']);
        Route::post('/delete', [CustomerController::class, 'destroy']);
        Route::get('/change-status/{id}', [CustomerController::class, 'changeStatus']);
        Route::get('/kich-hoat/{id}', [CustomerController::class, 'kichHoat']);
        Route::post('/change-password', [CustomerController::class, 'changePassword']);
    });

    Route::group(['prefix' => '/bai-viet'], function() {
        Route::get('/index', [QuanLyBaiVietController::class, 'index']);
        Route::post('/create', [QuanLyBaiVietController::class, 'createBaiViet']);
        Route::post('/update', [QuanLyBaiVietController::class, 'updateBaiViet']);
        Route::get('/data', [QuanLyBaiVietController::class, 'getData']);
        Route::get('/status/{id}', [QuanLyBaiVietController::class, 'doiTrangThai']);
        Route::post('/delete', [QuanLyBaiVietController::class, 'delete']);
    });

    Route::group(['prefix' => '/lien-he'], function() {
        Route::get('/index', [LienHeController::class, 'index']);
        Route::get('/data', [LienHeController::class, 'getData']);
        Route::post('/delete', [LienHeController::class, 'deleteLienHe']);
    });

    Route::group(['prefix' => '/the-loai'], function() {
        Route::get('/index', [TheLoaiController::class, 'index']);
        Route::post('/index', [TheLoaiController::class, 'store']);
        Route::get('/data', [TheLoaiController::class, 'getData']);
        Route::post('/delete', [TheLoaiController::class, 'deleteLienHe']);
    });

    Route::group(['prefix' => '/lich-su-mua-hang'], function() {
        Route::get('/index', [LichSuMuaHang::class, 'index']);
        Route::post('/index', [LichSuMuaHang::class, 'store']);
        Route::get('/data', [LichSuMuaHang::class, 'getData']);

    });

    Route::group(['prefix' => '/danh-sach-binh-luan'], function() {
        Route::get('/index', [BinhLuanController::class, 'index']);
        Route::get('/data', [BinhLuanController::class, 'getData']);
    });

    Route::group(['prefix' => '/thong-ke'], function() {
        Route::get('/index', [ThongKe::class, 'index']);
        Route::get('/tong-don-hang', [ThongKe::class, 'tongSoDonHang']);
        Route::get('/tong-doanh-thu', [ThongKe::class, 'TongDoanhThu']);
        Route::get('/tong-so-luong-san-pham', [ThongKe::class, 'tongSoSanPham']);
        Route::get('/tong-khach-hang', [ThongKe::class, 'tongSoKhachHang']);
    });

});



Route::group(['prefix' => 'laravel-filemanager'], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});
