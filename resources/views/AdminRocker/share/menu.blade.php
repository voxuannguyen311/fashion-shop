<div class="nav-container primary-menu">
    <div class="mobile-topbar-header">
        <div>
            <img src="/assets_admin_rocker/images/logo-icon.png" class="logo-icon" alt="logo icon">
        </div>
        <div>
            <h4 class="logo-text">Rukada</h4>
        </div>
        <div class="toggle-icon ms-auto"><i class='bx bx-arrow-to-left'></i>
        </div>
    </div>
    <nav class="navbar navbar-expand-xl w-100">
        <ul class="navbar-nav justify-content-start flex-grow-1 gap-1">
            <li class="nav-item">
                <a class="nav-link" href="/admin/thong-ke/index">
                    <div class="parent-icon"><i class="bx bx-cookie"></i>
                    </div>
                    <div class="menu-title">Thống kê</div>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/the-loai/index">
                    <div class="parent-icon"><i class="bx bx-cookie"></i>
                    </div>
                    <div class="menu-title">Thể Loại</div>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/san-pham/index">
                    <div class="parent-icon"><i class="bx bx-cookie"></i>
                    </div>
                    <div class="menu-title">Sản Phẩm</div>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/khach-hang/thong-tin">
                    <div class="parent-icon"><i class="bx bx-cookie"></i>
                    </div>
                    <div class="menu-title">Quản lý khách hàng</div>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/lich-su-mua-hang/index">
                    <div class="parent-icon"><i class="bx bx-cookie"></i>
                    </div>
                    <div class="menu-title">Lịch sử mua Hàng</div>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/bai-viet/index">
                    <div class="parent-icon"><i class="bx bx-cookie"></i>
                    </div>
                    <div class="menu-title">Bài viết</div>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/lien-he/index">
                    <div class="parent-icon"><i class="bx bx-cookie"></i>
                    </div>
                    <div class="menu-title">Liên hệ</div>
                </a>
            </li>


            <li class="nav-item">
                <a class="nav-link" href="/admin/danh-sach-binh-luan/index">
                    <div class="parent-icon"><i class="bx bx-cookie"></i>
                    </div>
                    <div class="menu-title">Bình luận sản phẩm</div>
                </a>
            </li>
        </ul>
    </nav>

</div>


