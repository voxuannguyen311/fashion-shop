@extends('AdminRocker.share.master')
@section('noi_dung')

<div class="row row-cols-1 row-cols-md-2 row-cols-xl-4" id="app">
    <div class="col">
      <div class="card radius-10 border-start border-0 border-3 border-info">
        <div class="card-body">
          <div class="d-flex align-items-center">
            <div>
              <p class="mb-0 text-secondary">Tổng số đơn đặt hàng</p>
              <h4 class="my-1 text-info">@{{ tong_don_hang }}</h4>

            </div>
            <div class="widgets-icons-2 rounded-circle bg-gradient-scooter text-white ms-auto"><i
                class="bx bxs-cart"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col">
      <div class="card radius-10 border-start border-0 border-3 border-danger">
        <div class="card-body">
          <div class="d-flex align-items-center">
            <div>
              <p class="mb-0 text-secondary">Tổng doanh thu</p>
              <h4 class="my-1 text-danger">$@{{ formatCurrency(tong_doanh_thu) }}</h4>

            </div>
            <div class="widgets-icons-2 rounded-circle bg-gradient-bloody text-white ms-auto"><i
                class="bx bxs-wallet"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col">
      <div class="card radius-10 border-start border-0 border-3 border-success">
        <div class="card-body">
          <div class="d-flex align-items-center">
            <div>
              <p class="mb-0 text-secondary">Tổng sản phẩm</p>
              <h4 class="my-1 text-success">@{{ tong_so_luong_san_pham }}</h4>

            </div>
            <div class="widgets-icons-2 rounded-circle bg-gradient-ohhappiness text-white ms-auto"><i
                class="bx bxs-basket"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col">
      <div class="card radius-10 border-start border-0 border-3 border-warning">
        <div class="card-body">
          <div class="d-flex align-items-center">
            <div>
              <p class="mb-0 text-secondary">Tổng số khách hàng</p>
              <h4 class="my-1 text-warning">@{{ Tong_khach_hang }}</h4>
            </div>
            <div class="widgets-icons-2 rounded-circle bg-gradient-blooker text-white ms-auto"><i
                class="bx bxs-group"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
@section('js')
<script>
    new Vue({
        el: '#app',
        data: {
            tong_don_hang: 0,
            tong_so_luong_san_pham: 0,
            Tong_khach_hang: 0,
            tong_doanh_thu: 0,
        },

        created() {
            this.TongDonHang();
            this.TongSoLuongSanPham();
            this.TongKhachHang();
            this.TongDoanhThu();
        },
        methods: {

            TongDonHang() {
                axios.get('/admin/thong-ke/tong-don-hang')
                    .then((res) => {
                        this.tong_don_hang = res.data.tong_don_hang;
                    });
            },
            TongSoLuongSanPham() {
                axios.get('/admin/thong-ke/tong-so-luong-san-pham')
                    .then((res) => {
                        this.tong_so_luong_san_pham = res.data.tong_so_luong_san_pham;
                    });
            },
            TongKhachHang() {
                axios.get('/admin/thong-ke/tong-khach-hang')
                    .then((res) => {
                        this.Tong_khach_hang = res.data.Tong_khach_hang;
                    });
            },
            TongDoanhThu() {
                axios.get('/admin/thong-ke/tong-doanh-thu')
                    .then((res) => {
                        this.tong_doanh_thu = res.data.tong_doanh_thu;
                    });
            },
            formatCurrency(value) {
                const formatter = new Intl.NumberFormat('vi-VN', {
                    style: 'currency',
                    currency: 'VND',
                });
                return formatter.format(value);
            },
        },
    });
</script>

@endsection
