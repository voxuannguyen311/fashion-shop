@extends('AdminRocker.share.master')
@section('noi_dung')
    <div class="row" id="app">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Thêm thể loại
                </div>
                <div class="body">
                    <div class="form-group mt-3">
                        <input type="text" v-model="them_the_loai.the_loai" class="form-control"
                            placeholder="Nhập vào Thể loại">
                    </div>
                </div>
                <div class="footer">
                    <button v-on:click="ThemTheLoai()" class="btn btn-primary">Thêm</button>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Danh sách liên hệ chờ xử lý
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th class="text-center">Thể Loại</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(value, key) in list_the_loai">
                                <th>@{{ key + 1 }}</th>
                                <td>@{{ value.the_loai }}</td>
                                <td class="text-center">
                                    <button v-on:click="deletetheloai = value" data-bs-toggle="modal" data-bs-target="#deleteModal" class="btn btn-danger">Xóa</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body text-danger">
                                Bạn có chắc chắn xóa không !!!
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Đóng</button>
                              <button v-on:click="delete_theloai()" type="button" class="btn btn-primary">Xóa</button>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    new Vue({
        el      :  '#app',
        data    :  {
            them_the_loai      : {},
            list_the_loai    :   [],
            deletetheloai    :   {},
        },
        created() {
            this.loadData();
        },
        methods :   {
            ThemTheLoai() {
                    axios
                        .post('/admin/the-loai/index', this.them_the_loai)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadData();
                                this.them_the_loai = {};
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        })
                        .catch((res) => {
                            $.each(res.response.data.errors, function(k, v) {
                                toastr.error(v[0]);
                            });
                        });
                },
            loadData() {
                axios
                    .get('/admin/the-loai/data')
                    .then((res) => {
                        this.list_the_loai = res.data.data;
                    });
            },

            delete_theloai(){
                axios
                    .post('/admin/the-loai/delete', this.deletetheloai)
                    .then((res) => {
                        if(res.data.status) {
                            toastr.success(res.data.message);
                            this.loadData();
                            $('#deleteModal').modal('hide');
                        } else {
                            toastr.error('Có lỗi không mong muốn!');
                        }
                    })
                    .catch((res) => {
                        $.each(res.response.data.errors, function(k, v) {
                            toastr.error(v[0]);
                        });
                    });
            },


        }
    });
</script>
@endsection
