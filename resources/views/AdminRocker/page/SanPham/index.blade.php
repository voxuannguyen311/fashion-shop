@extends('AdminRocker.share.master')
@section('noi_dung')
    <div class="row " id="app">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header text-center">
                    <h3>Thêm Sản Phẩm</h3>
                </div>
                <div class="card-body">
                    <div class="form-group mt-3">
                        <label>Tên Sản Phẩm</label>
                        <input v-model="them_moi.ten_san_pham" v-on:keyup="chuyenTenSpSangSlug()" type="text" class="form-control"
                            placeholder="Nhập vào Tên Sản Phẩm">
                    </div>

                    <div class="form-group mt-3">
                        <label>Slug Sản Phẩm</label>
                        <input v-model="slug" type="text" class="form-control"
                            placeholder="Nhập vào Slug Sản Phẩm">
                    </div>

                    <div class="form-group mt-3">
                        <label>Thể Loại</label>
                        <select v-model="them_moi.the_loai" class="form-control" id="theLoai">
                            <option v-for="(value, key) in list_the_loai" :value="value.id">@{{ value.the_loai }}</option>
                        </select>
                    </div>

                    <div class="form-group mt-3">
                        <label>Giá Bán</label>
                        <input v-model="them_moi.gia_ban" type="text" class="form-control"
                            placeholder="Nhập vào Giá Bán">
                    </div>
                    <div class="form-group mt-3">
                        <label>Số lượng</label>
                        <input v-model="them_moi.so_luong" type="text" class="form-control"
                            placeholder="Nhập vào Số lượng">
                    </div>
                    <div class="form-group mt-3">
                        <label>Ảnh Sản Phẩm</label>
                        <div class="input-group">
                            <input id="hinh_anh" class="form-control" type="text" name="filepath">
                            <span class="input-group-prepend">
                                <a id="lfm" data-input="hinh_anh" data-preview="holder" class="btn btn-primary">
                                    <i class="fa fa-picture-o"></i> Choose
                                </a>
                            </span>
                        </div>
                        <div id="holder" style="margin-top:15px;max-height:100px;"></div>
                    </div>
                    <div class="form-group mt-3">
                        <label>Tình Trạng</label>
                        <select v-model="them_moi.tinh_trang" class="form-control">
                            <option value="1">Còn Hàng</option>
                            <option value="0">Hết Hàng</option>
                        </select>
                    </div>
                    <div class="form-group mt-3">
                        <label>Mô Tả</label>
                        <textarea name="mo_ta" id="mo_ta" class="form-control" cols="30" rows="10"></textarea>
                    </div>
                </div>
                <div class="card-footer text-end">
                    <button v-on:click="createSp()" class="btn btn-primary">Thêm Sản Phẩm</button>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">
                    <h3> Danh Sách Các Sản Phẩm</h3>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="table" class="table table-bordered">
                            <thead clas="bg-primary">
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Tên Sản Phẩm</th>
                                    <th class="text-center">Thể Loại</th>
                                    <th class="text-center">Slug</th>
                                    <th class="text-center">Giá Bán</th>
                                    <th class="text-center">Số Lượng</th>
                                    <th class="text-center">Hình Ảnh</th>
                                    <th class="text-center">Tình Trạng</th>
                                    <th class="text-center">Mô Tả</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(value, key) in ds_sanpham">
                                    <th class="align-middle text-center">@{{ key + 1 }}</th>
                                    <td class="align-middle text-center">@{{ value.ten_san_pham }}</td>
                                    <td class="align-middle text-center">
                                        <template v-for="item in list_the_loai">
                                            <template v-if="value.the_loai === item.id">
                                                @{{ item.the_loai }}
                                            </template>
                                        </template>
                                    </td>
                                    <td class="align-middle text-center">@{{ value.slug_san_pham }}</td>
                                    <td class="align-middle text-center">@{{ value.gia_ban }}</td>
                                    <td class="align-middle text-center">@{{ value.so_luong }}</td>
                                    <td class="align-middle text-center">

                                        <button class="btn btn-light" v-on:click="anh_sanpham = value" data-bs-toggle="modal" data-bs-target="#hinhAnhModel"><i class="fa-solid fa-image text-success"></i></button>
                                        {{-- <img v-bind:src="value.hinh_anh" class="img-fluid" style="max-width: 200px;"> --}}
                                    </td>

                                    <td class="align-middle text-center">
                                        <button v-on:click="changeStatus(value.id)" class="btn btn-danger" v-if="value.tinh_trang == 0">Hết Hàng</button>
                                        <button v-on:click="changeStatus(value.id)" class="btn btn-primary" v-else>Còn Hàng</button>
                                    </td>

                                    <td class="align-middle" v-html="value.mo_ta.substring(0, 100)+ '...'"></td>
                                    <td class="align-middle text-center text-nowrap">
                                        <button v-on:click=" showUpdate(value)" class="btn btn-info" data-bs-toggle="modal"
                                            data-bs-target="#updateModal">update</button>
                                        <button v-on:click="delete_sanpham = value" class="btn btn-danger"
                                            data-bs-toggle="modal" data-bs-target="#deleteModal">delete</button>
                                    </td>
                                </tr>
                            </tbody>
                            <!-- Modal Update-->
                            <div class="modal fade" id="updateModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                                aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group mt-3">
                                                <label>Tên Sản Phẩm</label>
                                                <input v-model="update_sanpham.ten_san_pham" type="text"
                                                    class="form-control" placeholder="Nhập vào Tên Sản Phẩm">
                                            </div>
                                            <div class="form-group mt-3">
                                                <label>Thể Loai</label>
                                                <input v-model="update_sanpham.the_loai" type="text"
                                                    class="form-control" placeholder="Nhập vào Thể Loai">
                                            </div>
                                            <div class="form-group mt-3">
                                                <label>Slug Sản Phẩm</label>
                                                <input v-model="update_sanpham.slug_san_pham" type="text"
                                                    class="form-control" placeholder="Nhập vào Slug Sản Phẩm">
                                            </div>
                                            <div class="form-group mt-3">
                                                <label>Giá Bán</label>
                                                <input v-model="update_sanpham.gia_ban" type="text"
                                                    class="form-control" placeholder="Nhập vào Giá Bán">
                                            </div>
                                            <div class="form-group mt-3">
                                                <label>Hình </label>
                                                <div class="input-group">
                                                    <input id="hinh_anh_update" class="form-control" type="text" name="filepath">
                                                    <span class="input-group-prepend">
                                                        <a id="lfm_update" data-input="hinh_anh_update" data-preview="holder_update" class="btn btn-primary">
                                                            <i class="fa fa-picture-o"></i> Choose
                                                        </a>
                                                    </span>
                                                </div>
                                                <div id="holder_update" style="margin-top:15px; max-height:100px;">
                                                </div>
                                            </div>
                                            <div class="form-group mt-3">
                                                <label>Tình Trạng</label>
                                                <select v-model="update_sanpham.tinh_trang" class="form-control">
                                                    <option value="1">Còn Hàng</option>
                                                    <option value="0">Hết Hàng</option>
                                                </select>
                                            </div>
                                            <div class="form-group mt-3">
                                                <label>Mô Tả</label>
                                                <textarea name="update_mo_ta" id="update_mo_ta" class="form-control" cols="30" rows="10"></textarea>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-bs-dismiss="modal">Close</button>
                                            <button v-on:click="capNhatSpServer()" type="button" class="btn btn-primary"
                                                data-bs-dismiss="modal">Cập Nhật Sản Phẩm</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal Delete-->
                            <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                                aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body text-nowrap">
                                            Bạn Có Chắc Muốn xóa Sản Phẩm: <h5 class="text-danger"
                                                style="display: inline;"> @{{ delete_sanpham.ten_san_pham }} </h5> Này
                                            <input type="hidden" class="form-control" v-model="delete_sanpham.id">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-bs-dismiss="modal">Close</button>
                                            <button v-on:click="xoaSpTrenServer()"type="button" class="btn btn-primary"
                                                data-bs-dismiss="modal">Delete Sản Phẩm</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal Hình Ảnh-->
                            <div class="modal fade" id="hinhAnhModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalLabel">Hình Ảnh</h5>
                                      <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center">
                                            <img v-bind:src="anh_sanpham.hinh_anh" class="img-fluid" style="max-width: 200px;">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        new Vue({
            el: "#app",
            data: {
                them_moi       : {},
                ds_sanpham     : [],
                update_sanpham : {},
                delete_sanpham : {},
                slug           : '',
                anh_sanpham    : {},
                list_the_loai    :   [],
            },
            created() {
                this.loadSp();
                this.loadData();
            },
            methods: {
                showUpdate(value) {
                    $("#hinh_anh_update").val(value.hinh_anh);
                    var text = '<img src="'+ value.hinh_anh + '" style="margin-top:15px;max-height:100px;">'
                    $("#holder_update").html(text);
                    CKEDITOR.instances['update_mo_ta'].setData(value.mo_ta);
                    this.update_sanpham = value;
                },

                createSp() {
                    this.them_moi.hinh_anh = $("#hinh_anh").val();
                    this.them_moi.slug_san_pham = this.slug;
                    this.them_moi.mo_ta = CKEDITOR.instances['mo_ta'].getData();
                    axios
                        .post('/admin/san-pham/index', this.them_moi)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadSp();
                                this.slug = '';
                                this.them_moi = {};
                                $("#hinh_anh").val("");
                                CKEDITOR.instances['mo_ta'].setData('');
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        })
                        .catch((res) => {
                            $.each(res.response.data.errors, function(k, v) {
                                toastr.error(v[0]);
                            });
                        });
                },
                loadSp() {
                    axios
                        .get('/admin/san-pham/data')
                        .then((res) => {
                            this.ds_sanpham = res.data.list;
                            // console.log(this.ds_sanpham);
                        });
                },
                capNhatSpServer() {
                    this.update_sanpham.mo_ta = CKEDITOR.instances['update_mo_ta'].getData();
                    this.update_sanpham.hinh_anh = $("#hinh_anh_update").val();
                    axios
                        .post('/admin/san-pham/update', this.update_sanpham)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadSp();

                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        })
                },

                xoaSpTrenServer() {
                    axios
                        .post('/admin/san-pham/delete', this.delete_sanpham)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadSp();
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        })
                },

                toSlug(str) {
                    str = str.toLowerCase();
                    str = str
                        .normalize('NFD')
                        .replace(/[\u0300-\u036f]/g, '');
                    str = str.replace(/[đĐ]/g, 'd');
                    str = str.replace(/([^0-9a-z-\s])/g, '');
                    str = str.replace(/(\s+)/g, '-');
                    str = str.replace(/-+/g, '-');
                    str = str.replace(/^-+|-+$/g, '');

                    return str;
                },

                chuyenTenSpSangSlug() {
                    this.slug = this.toSlug(this.them_moi.ten_san_pham);
                },

                changeStatus(id) {
                    axios
                        .get('/admin/san-pham/change-status/' + id)
                        .then((res) => {
                            this.loadSp();
                            toastr.success('Đã đổi trạng thái thành công!');
                        });
                },

                loadData() {
                    axios
                        .get('/admin/the-loai/data')
                        .then((res) => {
                            this.list_the_loai = res.data.data;
                        });
                }
            }
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.19.1/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('mo_ta')
        CKEDITOR.replace('update_mo_ta'); // replace name mô tả
    </script>


<script>
    var route_prefix = "/laravel-filemanager";
</script>
<script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
<script>
    $("#lfm").filemanager('image', {prefix : route_prefix});
    $("#lfm_update").filemanager('image', {prefix : route_prefix});
</script>
@endsection
