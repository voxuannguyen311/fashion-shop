@extends('AdminRocker.share.master')
@section('noi_dung')
    <div id="app">
        <div class="collection">
            <div class="container">
                <!-- Title -->
                <h4 class="our-products__title font-family-jost text-center border-top">Đơn Hàng</h4>
                <!-- End title -->
            </div>
            <!-- Container -->
            <div class="container container--type-3 pt-5">
                <!-- Products row -->
                <div class="row products-row products-row--type-2">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="table" class="table table-bordered">
                                <thead style="background: #ffae00">
                                    <tr>
                                        <th class="text-center">Họ và tên</th>
                                        <th class="text-center">Số điện thoại</th>
                                        <th class="text-center">Địa chỉ</th>
                                        <th class="text-center">Mã hóa đơn</th>
                                        <th class="text-center">Trạng thái thanh toán</th>
                                        <th class="text-center">Sản Phẩm Chi Tiết</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(value, key) in ds_du_lieu" :key="key">
                                        <th class="align-middle text-center">@{{ value.ho_va_ten }}</th>
                                        <th class="align-middle text-center">@{{ value.so_dien_thoai }}</th>
                                        <th class="align-middle text-center">@{{ value.dia_chi }}</th>
                                        <th class="align-middle text-center">@{{ value.id }}</th>

                                        <th class="align-middle text-center">
                                            <button v-if="value.trang_thai_thanh_toan === 0" class="btn btn-danger">Chưa
                                                thanh toán</button>
                                            <button v-else class="btn btn-success">Đã thanh toán</button>
                                        </th>

                                        <th class="align-middle text-center">
                                        <button class="btn btn-info" data-bs-toggle="modal"
                                            :data-bs-target="'#exampleModal' + key">Chi Tiết</button>
                                    </th>
                                    </tr>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
                <!-- End Products row -->
            </div>
            <!-- End container -->
        </div>

<!-- Modal -->
<div v-for="(value, key) in ds_du_lieu" :key="key">
    <div class="modal fade" :id="'exampleModal' + key" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Chi Tiết Đơn Hàng</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <!-- Nội dung chi tiết đơn hàng -->
                    <table class="table table-bordered">
                        <thead style="background: #ffae00">
                            <tr>
                                <th class="text-center">Mã sản phẩm</th>
                                <th class="text-center">Tên sản phẩm</th>
                                <th class="text-center">Ảnh sản phẩm</th>
                                <th class="text-center">Giá sản phẩm</th>
                                <th class="text-center">Ngày đặt sản phẩm</th>
                                <th class="text-center">Số lượng</th>
                                <th class="text-center">Tổng tiền</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(value, key) in value.ds_hoa_don_chi_tiet" :key="key">
                                <th class="align-middle text-center">@{{ value.id }}</th>
                                <th class="align-middle text-center">@{{ value.ten_san_pham }}</th>
                                <th class="align-middle text-center">
                                    <img alt="Image"
                                         data-sizes="auto"
                                         :data-srcset="value.hinh_anh + ' 50w, ' + value.hinh_anh + ' 50w'"
                                         :src="value.hinh_anh"
                                         class="lazyload"
                                         width="50" /> <!-- Thêm thuộc tính width="50" để chỉ định kích thước ảnh -->
                                </th>

                                <th class="align-middle text-center">
                                    @{{ formatCurrency(value.gia_ban) }}
                                </th>

                                <th class="align-middle text-center">@{{ formatDate(value.created_at) }}</th>
                                <th class="align-middle text-center">@{{ value.tong_so_luong }}</th>
                                <th class="align-middle text-center">@{{ formatCurrency(value.tong_tien) }}</th>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="align-middle text-center" colspan="7">
                                    Tổng tiền bảo gồm cả thuế và phí ship: @{{ formatCurrency(value.tong_tien_tat_ca) }}
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
    </div>
@endsection
@section('js')
    <script>
        new Vue({
            el: '#app',
            data: {
                ds_du_lieu: [],
            },

            created() {
                this.lich_su_mua_hang();
            },
            methods: {

                lich_su_mua_hang() {
                    axios.get('/admin/lich-su-mua-hang/data')
                        .then((res) => {
                            console.log(res.data.du_lieu);
                            this.ds_du_lieu = res.data.du_lieu;
                        });
                },

                formatDate(timestamp) {
                    return moment(timestamp).format('YYYY-MM-DD HH:mm:ss');
                },

                formatCurrency(value) {
                    const formatter = new Intl.NumberFormat('vi-VN', {
                        style: 'currency',
                        currency: 'VND',
                    });
                    return formatter.format(value);
                },
            },
        });
    </script>
@endsection
