@extends('AdminRocker.share.master')
@section('noi_dung')

        <div class="card" id="app">
            <div class="card-header">

            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center text-nowrap">#</th>
                            <th class="text-center text-nowrap">Tên sản phẩm</th>
                            <th class="text-center text-nowrap">Hình ảnh sản phẩm</th>
                            <th class="text-center text-nowrap">Họ Và Tên</th>
                            <th class="text-center text-nowrap">Email</th>
                            <th class="text-center text-nowrap">Nội Dung</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(value, key) in list">
                            <th class="text-nowrap text-center align-middle">@{{key + 1}}</th>
                            <td class="text-nowrap align-middle">@{{value.ten_san_pham}}</td>
                            <td class="align-middle text-center">
                            <img v-bind:src="value.hinh_anh" class="img-fluid" style="max-width: 200px;">
                            </td>
                            <td class="text-nowrap align-middle">@{{value.ho_va_ten}}</td>
                            <td class="text-nowrap align-middle">@{{value.email}}</td>
                            <td class="text-nowrap text-center align-middle">@{{value.noi_dung}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>


@endsection
@section('js')
<script>
    new Vue({
        el      :  '#app',
        data    :  {
            list:  [],

        },
        created() {
            this.loadData();
        },
        methods :   {



            loadData() {
                axios
                    .get('/admin/danh-sach-binh-luan/data')
                    .then((res) => {
                        this.list = res.data.du_lieu;
                    });
            },



        }
    });
</script>
@endsection
