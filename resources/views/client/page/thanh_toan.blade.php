@extends('client.share.master')
@section('noi_dung')
@php
        $check = Auth::guard('customer')->check();
        $user = Auth::guard('customer')->user();
    @endphp
    <div class="checkout" id="app">

        <div class="container container--type-2">
            <div class="container">
                <h1 class="checkout__title">Checkout</h1>
                <div class="row">
                    <div class="col-lg-7">
                        <div class="checkout__container">
                            <div class="billing-form">
                                <h3 class="billing-form__heading">Billing Detail</h3>
                                <form method="POST" action="/client/thanh-toan" >
                                @csrf
                                    <div class="form-group">
                                        <label class="label">Họ Và Tên</label>
                                        <input type="text" name="ho_va_ten" placeholder="Họ Và Tên" value="{{ isset($user) ? $user->ho_va_ten : '' }}" class="form-group__input">

                                    </div>
                                    <div class="form-group">
                                        <label class="label">Số Điện Thoại</label>
                                        <input type="number" name="so_dien_thoai" placeholder="Số Điện Thoại" value="{{ isset($user) ? $user->so_dien_thoai : '' }}" class="form-group__input">
                                    </div>
                                    <div class="form-group">
                                        <label class="label">Địa chỉ</label>
                                        <input type="text" name="dia_chi" placeholder="Địa chỉ"  value="{{ isset($user) ? $user->so_dien_thoai : '' }}"class="form-group__input">
                                    </div>
                                    <input type="hidden" name="tong_tien_tat_ca" :value="tong_tien" />
                                    <div class="checkout__action">
                                        <button type="submit" class="second-button" name="redirect">Thanh Toán</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="checkout__summary">
                            <h3 class="checkout-summary__title">Your Order</h3>
                            <div class="checkout__products">
                                <div v-for="(value, key) in ds_cart" :key="key">
                                    <div class="checkout__product">
                                        <div class="checkout-product__image">
                                            <a>
                                                <img alt="Image" data-sizes="auto"
                                                    :data-srcset="value.hinh_anh + ' 400w, ' + value.hinh_anh + ' 800w'"
                                                    :src="value.hinh_anh" class="lazyload" />
                                            </a>
                                        </div>
                                        <div class="checkout-product__title-and-variant">
                                            <h3 class="cart-product__title"><a href="product.html">
                                                    @{{ value.ten_san_pham }} x @{{ value.tong_so_luong }}</a></h3>
                                        </div>
                                        <div class="checkout-product__price"> @{{ formatCurrency(value.gia_ban) }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="checkout__total">
                                <div class="checkout-total__title">Total</div>
                                <div class="checkout-total__price">@{{ formatCurrency(tong_tien) }}</div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <!-- Canvas cart -->
   <div class="canvas-cart js-canvas-cart">
    <div class="canvas-cart__overlay js-close-canvas-cart"></div>
    <!-- Content -->
    <div class="canvas-cart__content">
        <!-- D-flex -->
        <div class="canvas-cart__d-flex">
            <!-- Top and products -->
            <div class="canvas-cart__top-and-products">
                <!-- Heading -->
                <div class="canvas-cart__heading d-flex align-items-center">
                    <!-- H3 -->
                    <h3 class="canvas-cart__h3">Cart (3)</h3>
                    <!-- End h3 -->
                    <!-- Close -->
                    <div class="canvas-cart__close"><a href="#" class="js-close-canvas-cart"><i
                                class="lnil lnil-close"></i></a></div>
                    <!-- End close -->
                </div>
                <!-- End heading -->
                <!-- Cart items -->
                <ul class="header-cart__items">
                    <!-- Use Vue.js v-if to conditionally render the cart items -->
                    <div v-if="ds_cart">
                        <ul class="cart-items">
                            <li class="cart-item d-flex" v-for="(value, key) in ds_cart" :key="key">
                                <!-- Item image -->
                                <p class="cart-item__image">
                                    <a>
                                        <img alt="Image" data-sizes="auto"
                                            :data-srcset="value.hinh_anh + ' 400w, ' + value.hinh_anh + ' 800w'"
                                            :src="value.hinh_anh" class="lazyload" />
                                    </a>
                                </p>
                                <!-- End item image -->
                                <!-- Item details -->
                                <p class="cart-item__details">
                                    <a class="cart-item__title">
                                        @{{ value.ten_san_pham }}
                                    </a>
                                    <span class="cart-item__price">@{{ value.tong_so_luong }} <i>x</i>
                                        @{{ formatCurrency(value.gia_ban) }}</span>
                                </p>
                                <!-- End item details -->
                                <!-- Item quantity -->
                                <div class="cart-item__quantity">
                                    <div class="cart-product__quantity-field">
                                        <div class="quantity-field__minus js-quantity-down">
                                            <a href="javascript:void(0)"
                                                v-on:click="tru_so_luong(value.ma_san_pham)">-</a>
                                        </div>
                                        <input type="text" :value="value.tong_so_luong"
                                            class="quantity-field__input js-quantity-field" readonly />
                                        <div class="quantity-field__plus js-quantity-up">
                                            <a href="javascript:void(0)"
                                                v-on:click="addToCart(value.ma_san_pham)">+</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- End item quantity -->
                                <!-- Item delete -->
                                <p class="cart-item__delete">
                                    <a href="javascript:void(0)"><i class="lnil lnil-close"  v-on:click="xoa_san_pham_gio_hang(value.ma_san_pham)"></i></a>
                                </p>
                                <!-- End item delete -->
                            </li>
                        </ul>
                    </div>
                    <div v-else>
                        <p>Giỏ hàng trống.</p>
                    </div>

                </ul>
                <!-- End cart items -->
            </div>
            <!-- End top and products -->
            <!-- Bottom -->
            <div class="canvas-cart__bottom">
                <!-- Subtotal -->
                <div class="header-cart__subtotal d-flex">
                    <!-- Title -->
                    <div class="subtotal__title">Subtotal</div>
                    <!-- End title -->
                    <!-- Value -->
                    <div class="subtotal__value" v-if="tong_tien > 0">@{{ formatCurrency(tong_tien) }}</div>
                    <div class="subtotal__value" v-else>0₫</div>
                    <!-- End value -->
                </div>
                <!-- End subtotal -->
                <!-- Header cart action -->
                <div class="header-cart__action">
                    <a href="/gio-hang" class="header-cart__button">View cart</a>
                    <a href="/thanh-toan" class="header-cart__button">Checkout</a>
                </div>
                <!-- End Header cart action -->
            </div>
            <!-- End bottom -->
        </div>
        <!-- End d-flex -->
    </div>
    <!-- End content -->
</div>
<!-- End canvas cart -->
</div>
@endsection
@section('js')
<script>
    new Vue({
        el: "#app",
        data: {
            ds_cart: [],
            tong_tien: 0,
        },
        created() {
            this.loadCart();
        },
        methods: {
            addToCart(id) {
                axios
                    .post('/client/them-so-luong/' + id)
                    .then((res) => {
                        if (res.data.status) {
                            toastr.success(res.data.message);
                            this.loadCart();
                        } else {
                            toastr.error('Có lỗi không mong muốn!');
                        }
                    })
            },

            loadCart() {
                axios
                    .get('/client/hien-thi-ds-gio-hang')
                    .then((res) => {
                        this.ds_cart = res.data.gio_hang;
                        this.tong_tien = res.data.tong_tien_tat_ca
                        console.log(this.ds_cart);
                    });
            },
            formatCurrency(value) {
                const formatter = new Intl.NumberFormat('vi-VN', {
                    style: 'currency',
                    currency: 'VND',
                });
                return formatter.format(value);
            },

            tru_so_luong(id) {
                axios
                    .post('/client/tru-so-luong/' + id)
                    .then((res) => {
                        if (res.data.status) {
                            toastr.success(res.data.message);
                            this.loadCart();
                        } else {
                            toastr.error('Có lỗi không mong muốn!');
                        }
                    });
            },
            xoa_san_pham_gio_hang(id) {
                axios
                    .post('/client/xoa-san-pham-gio-hang/' + id)
                    .then((res) => {
                        if (res.data.status) {
                            toastr.success(res.data.message);
                            this.loadCart();
                        } else {
                            toastr.error('Có lỗi không mong muốn!');
                        }
                    });
            },
        }
    });
</script>
@endsection

