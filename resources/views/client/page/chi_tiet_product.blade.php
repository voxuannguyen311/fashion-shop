@extends('client.share.master')
@section('noi_dung')
@php
$check = Auth::guard('customer')->check();
$user = Auth::guard('customer')->user();
@endphp
    {{-- @php
    $imageData = $san_pham->hinh_anh; // Lấy dữ liệu hình ảnh từ cơ sở dữ liệu
    $imagePath = 'data:image/png;base64,' . base64_encode($imageData); // Tạo URL từ dữ liệu hình ảnh
@endphp --}}

    {{-- <style>
    .zoomit-container.loaded .zoomit-ghost {
        cursor: url("{{ $imagePath }}"), auto;
    }
</style> --}}

    <div class="product pt-5" id="app">

        <div class="container container--type-2">

            <div class="product__main d-flex">
                <ul class="product__main-image js-popup-gallery">
                    <div class="product-image-container">
                        <li class="active js-product-main-image">
                            <a href="{{ isset($san_pham) ? $san_pham->hinh_anh : '' }}">
                                <img alt="Image" src="{{ isset($san_pham) ? $san_pham->hinh_anh : '' }}"
                                    data-zoomed="{{ isset($san_pham) ? $san_pham->hinh_anh : '' }}" />
                            </a>
                        </li>
                    </div>
                </ul>

                <div class="product__right">
                    <div class="product__tag">Easy iron</div>
                    <h1 class="product__title">{{ isset($san_pham) ? $san_pham->ten_san_pham : '' }}</h1>
                    <div class="product__price text-danger">
                        {{ number_format($san_pham->gia_ban) }} ₫
                    </div>
                    <div class="product__status">
                        <i class="lnir lnir-package"></i>
                        <span>Tình trạng:</span>
                        <span class="status__value status__value--in-stock">
                            @if ($san_pham->tinh_trang == 1)
                                Còn Hàng
                            @else
                                Hết Hàng
                            @endif
                        </span>
                    </div>

                    <div class="product__options">
                    </div>
                    <div class="product__action js-product-action">
                        <div class="product__quantity-and-add-to-cart d-flex">
                            <div class="product__add-to-cart" style="background: #f0f0f0 ">
                                @if ($check)
                                    <a href="javascript:void(0)"class="eighth-button"
                                        v-on:click="addToCart({{ $san_pham->id }})">Add to cart</a>
                                @else
                                    <form method="POST" action="/client/them-so-luong/{{ $san_pham->id }}">
                                        @csrf
                                        <button type="submit" class="eighth-button">ADD TO CART</button>
                                    </form>
                                @endif
                            </div>
                        </div>
                        <div class="product__buy-now">
                            {{-- <a href="/client/view-cart" class="header-cart__button">View cart</a> --}}
                            <form action="/client/mua-hang-ngay/{{ $san_pham->id }}" method="post">
                                @csrf
                               <button type="submit" href="/client/mua-hang-ngay/{{ $san_pham->id }}" class="second-button">Buy now</button>
                            </form>
                        </div>
                    </div>

                    <ul class="product__socials product__socials--type-2">
                        <li><a href="https://twitter.com/" target="_blank"><i class="lnil lnil-twitter"></i></a></li>
                        <li><a href="https://facebook.com/" target="_blank"><i class="lnil lnil-facebook"></i></a></li>
                        <li><a href="https://instagram.com/" target="_blank"><i class="lnil lnil-Instagram"></i></a></li>
                    </ul>
                </div>
            </div>

            <div class="product__tabs-2">


                <div class="product__desktop-tabs">
                    <ul class="tabs__nav">
                        <li>
                            <a href="#" class="active js-tab-link" data-id="1">Mô tả</a>
                        </li>
                        <li>
                            <a href="#" class="js-tab-link" data-id="3">Bình luận</a>
                        </li>

                    </ul>
                    <div class="tabs__content">

                        <div class="tab-content tab-content__active tab-content__show js-tab-content" data-id="1">
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <h3>Mô Tả</h3>
                                    <p>
                                        {!! isset($san_pham) ? html_entity_decode($san_pham->mo_ta) : '' !!}
                                    </p>

                                </div>

                            </div>
                        </div>



                        <div class="tab-content js-tab-content" data-id="3">
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <h3 class="review__title">Tất Cả Bình Luận</h3>
                                    @foreach ($ds_binh_luan as $key => $value)
                                        <div class="review d-flex">
                                            <div class="review__details">
                                                <div class="review__title-and-rating d-flex">

                                                </div>
                                                <div class="review__content">{{$value->noi_dung}}
                                                </div>
                                                <div class="review__meta">
                                                    <span>{{$value->ho_va_ten}}</span> - {{$value->created_at}}
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                                <div class="col-12 col-lg-6">
                                    <h3>Bình Luận</h3>
                                    <form class="review__form"  action="/client/binh-luan-san-pham" method="post">
                                        @csrf
                                        <div class="form__required-fields">Đăng nhập để bình luận<span>*</span></div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text"  disabled value="{{ isset($user->ho_va_ten) ? $user->ho_va_ten : '' }}"
                                                    class="form-group__input"
                                                        placeholder="Họ Và Tên">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="hidden" name="ma_khach_hang" value="{{ isset($user->id) ? $user->id : '' }}"
                                                    class="form-group__input"
                                                        placeholder="Họ Và Tên">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="hidden" name="ma_san_pham"  value="{{ $san_pham->id }}" class="form-group__input">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <textarea placeholder="Viết bình luận" name="noi_dung" class="form-group__textarea" rows="3"></textarea>
                                        </div>
                                        <div class="form__action">
                                            <button type="submit" class="second-button">Gửi</button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>

        </div>






        <div class="canvas-cart js-canvas-cart">
            <div class="canvas-cart__overlay js-close-canvas-cart"></div>

            <div class="canvas-cart__content">

                <div class="canvas-cart__d-flex">

                    <div class="canvas-cart__top-and-products">

                        <div class="canvas-cart__heading d-flex align-items-center">

                            <h3 class="canvas-cart__h3">Cart (3)</h3>


                            <div class="canvas-cart__close"><a href="#" class="js-close-canvas-cart"><i
                                        class="lnil lnil-close"></i></a></div>

                        </div>


                        <ul class="header-cart__items">

                            <div v-if="ds_cart">
                                <ul class="cart-items">
                                    <li class="cart-item d-flex" v-for="(value, key) in ds_cart" :key="key">

                                        <p class="cart-item__image">
                                            <a>
                                                <img alt="Image" data-sizes="auto"
                                                    :data-srcset="value.hinh_anh + ' 400w, ' + value.hinh_anh + ' 800w'"
                                                    :src="value.hinh_anh" class="lazyload" />
                                            </a>
                                        </p>


                                        <p class="cart-item__details">
                                            <a class="cart-item__title">
                                                @{{ value.ten_san_pham }}
                                            </a>
                                            <span class="cart-item__price">@{{ value.tong_so_luong }} <i>x</i>
                                                @{{ formatCurrency(value.gia_ban) }}</span>
                                        </p>


                                        <div class="cart-item__quantity">
                                            <div class="cart-product__quantity-field">
                                                <div class="quantity-field__minus js-quantity-down">
                                                    <a href="javascript:void(0)"
                                                        v-on:click="tru_so_luong(value.ma_san_pham)">-</a>
                                                </div>
                                                <input type="text" :value="value.tong_so_luong"
                                                    class="quantity-field__input js-quantity-field" readonly />
                                                <div class="quantity-field__plus js-quantity-up">
                                                    <a href="javascript:void(0)"
                                                        v-on:click="addToCart(value.ma_san_pham)">+</a>
                                                </div>
                                            </div>
                                        </div>


                                        <p class="cart-item__delete">
                                            <a href="javascript:void(0)"><i class="lnil lnil-close"
                                                    v-on:click="xoa_san_pham_gio_hang(value.ma_san_pham)"></i></a>
                                        </p>

                                    </li>
                                </ul>
                            </div>
                            <div v-else>
                                <p>Giỏ hàng trống.</p>
                            </div>

                        </ul>

                    </div>


                    <div class="canvas-cart__bottom">

                        <div class="header-cart__subtotal d-flex">

                            <div class="subtotal__title">Subtotal</div>


                            <div class="subtotal__value" v-if="tong_tien > 0">@{{ formatCurrency(tong_tien) }}</div>
                            <div class="subtotal__value" v-else>0₫</div>

                        </div>


                        <div class="header-cart__action">
                            <a href="/gio-hang" class="header-cart__button">View cart</a>
                            <a href="/thanh-toan" class="header-cart__button">Checkout</a>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>
@endsection
@section('js')
    <script>
        new Vue({
            el: "#app",
            data: {
                ds_cart: [],
                tong_tien: 0,
            },
            created() {
                this.loadCart();
            },
            methods: {
                addToCart(id) {
                    axios
                        .post('/client/them-so-luong/' + id)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadCart();
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        })
                },

                loadCart() {
                    axios
                        .get('/client/hien-thi-ds-gio-hang')
                        .then((res) => {
                            this.ds_cart = res.data.gio_hang;
                            this.tong_tien = res.data.tong_tien_tat_ca
                            console.log(this.ds_cart);
                        });
                },
                formatCurrency(value) {
                    const formatter = new Intl.NumberFormat('vi-VN', {
                        style: 'currency',
                        currency: 'VND',
                    });
                    return formatter.format(value);
                },
                tru_so_luong(id) {
                    axios
                        .post('/client/tru-so-luong/' + id)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadCart();
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        });
                },
                xoa_san_pham_gio_hang(id) {
                    axios
                        .post('/client/xoa-san-pham-gio-hang/' + id)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadCart();
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        });
                },
            }
        });
    </script>
@endsection
