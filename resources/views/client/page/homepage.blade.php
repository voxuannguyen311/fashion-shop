@extends('client.share.master')
@section('noi_dung')
    @php
        $check = Auth::guard('customer')->check();
        $user = Auth::guard('customer')->user();
    @endphp
    <style>
        .pagination-wrapper {
            display: flex;
            justify-content: center;
        }

        /* Chỉnh màu nền của các nút phân trang */
        .pagination .page-item:not(.disabled) .page-link {
            background-color: #212529;
        }

        /* Chỉnh màu nền khi hover lên các nút phân trang */
        /* .pagination .page-item:not(.disabled) .page-link:hover {
                                                        background-color: #232324;
                                                        color: #ffffff;

                                                        border-color: #ffffff;
                                                    } */

        /* Chỉnh màu chữ của các nút phân trang */
        .pagination .page-item:not(.disabled) .page-link {
            color: #fff;
        }

        /* Chỉnh màu chữ của nút hiện tại */
        .pagination .page-item.active .page-link {
            background-color: #ffffff;
            border-color: #232324;
            color: #232324;
        }

        /* Chỉnh khoảng cách giữa các nút phân trang */
        .pagination .page-item {
            padding: 5px;
        }

        /* Chỉnh màu chữ của nút disabled */
        /* .pagination .page-item.disabled .page-link {
                                                        color: #6c757d;
                                                    } */
    </style>
    <div id="app">
        <!-- Dark slider -->
        <div class="dark-slider">
            <!-- Slider js -->
            <div class="js-home-dark-slider">
                <!-- Slider item -->
                <div class="dark-slider__item">
                    <!-- Image -->
                    <div class="dark-slider__image js-slider-heading">
                        <img data-id="1" alt="Image" data-sizes="auto"
                            data-srcset="https://drfurithemes.com/durotan/wp-content/uploads/sites/15/2022/12/slider-darkskin-1-1.jpg 800w"
                            src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                            class="lazyload" />
                    </div>
                    <!-- End image -->
                    <!-- Content -->
                    <div class="dark-slider__content">
                        <!-- Tag -->
                        <h2 class="dark-slider__tag font-family-jost js-slider-heading">new arrivals</h2>
                        <!-- End tag -->
                        <!-- Title -->
                        <h2 class="dark-slider__title js-slider-heading">Denim elastic<br>waist Dress</h2>
                        <div class="dark-slider__buttons js-slider-heading">
                            <a href="shop.html" class="third-button">Shop now</a>
                        </div>
                        <!-- End title -->
                    </div>
                    <!-- End content -->
                </div>
                <!-- End slider item -->
                <!-- Slider item -->
                <div class="dark-slider__item">
                    <!-- Image -->
                    <div class="dark-slider__image js-slider-heading">
                        <img data-id="1" alt="Image" data-sizes="auto"
                            data-srcset="https://drfurithemes.com/durotan/wp-content/uploads/sites/15/2022/12/slider-darkskin-2-1.jpg 400w,
                https://drfurithemes.com/durotan/wp-content/uploads/sites/15/2022/12/slider-darkskin-2-1.jpg 800w"
                            src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                            class="lazyload" />
                    </div>
                    <!-- End image -->
                    <!-- Content -->
                    <div class="dark-slider__content">
                        <!-- Tag -->
                        <h2 class="dark-slider__tag font-family-jost js-slider-heading">Trending 2022</h2>
                        <!-- End tag -->
                        <!-- Title -->
                        <h2 class="dark-slider__title js-slider-heading">simple color<br>tote bags</h2>
                        <!-- End title -->
                        <!-- Buttons -->
                        <div class="dark-slider__buttons js-slider-heading">
                            <a href="shop.html" class="third-button">Shop now</a>
                        </div>
                        <!-- End buttons -->
                    </div>
                    <!-- End content -->
                </div>
                <!-- End slider item -->
            </div>
            <!-- End slider js -->
        </div>
        <!-- End dark slider -->



        <!--  products -->
        <div class="our-products modern-our-products">
            <!-- Container -->
            <div class="container container--type-4">
                <!-- Second container -->
                <div class="container">
                    <!-- Title -->
                    <h4 class="our-products__title font-family-jost">New Products</h4>
                    <!-- End title -->
                </div>
                <!-- End second container -->
                <!-- Slick products -->
                <div class="our_products__products js-home-our-products">
                    @foreach ($san_pham_new as $key => $value)
                        @if ($value->tinh_trang == 1)
                            <div class="col-6 col-md-4 col-xl-3 col-xxl-25">
                                <div class="product-grid-item">
                                    <!-- Product images -->
                                    <div class="product-grid-item__images js-product-grid-images" data-current-image="0">
                                        <!-- Product images arrows -->
                                        <div class="product-grid-item__images-arrows">
                                        </div>
                                        <!-- End product images arrows -->
                                        <!-- Product image -->
                                        <div class="product-grid-item__image js-product-grid-image active">
                                            <a href="/chi-tiet-san-pham/{{$value->the_loai}}/{{ $value->slug_san_pham }}/{{ $value->id }}">
                                                <img alt="Image" data-sizes="auto"
                                                    data-srcset="{{ $value->hinh_anh }} 400w, {{ $value->hinh_anh }} 800w"
                                                    src="{{ $value->hinh_anh }}" class="lazyload" />
                                            </a>
                                        </div>
                                        <!-- End product image -->
                                    </div>
                                    <!-- End product images -->
                                    <!-- Product action -->
                                    <div class="product-grid-item__action">
                                        <!-- D-flex -->
                                        <div class="d-flex align-items-center ">
                                            <!-- Add to cart -->
                                            <div class="product-grid-item__add-to-cart">
                                                @if ($check)
                                                    <a href="javascript:void(0)"
                                                        v-on:click="addToCart({{ $value->id }})">Add to cart</a>
                                                @else
                                                    <form method="POST" action="/client/them-so-luong/{{ $value->id }}">
                                                        @csrf
                                                        <button type="submit"
                                                            style="border: none; background-color: transparent; cursor: pointer; text-decoration: none; color: #928656; padding: 0; font-size: inherit; outline: none; ">ADD
                                                            TO CART</button>
                                                    </form>
                                                @endif
                                            </div>

                                            <!-- End add to cart -->
                                            <!-- Quickview -->
                                            {{-- <div class="product-grid-item__quickview">
                                                <a href="#" class="open-tooltip"><span class="custom-tooltip">Quick
                                                        view</span><i class="lnil lnil-full-screen"></i></a>
                                            </div> --}}
                                            <!-- End quickview -->
                                            <!-- Wishlist -->
                                            {{-- <div class="product-grid-item__wishlist">
                                                <a href="#" class="open-tooltip"><span class="custom-tooltip">Add to
                                                        wishlist</span><i class="lnil lnil-heart"></i></a>
                                            </div> --}}
                                            <!-- End wishlist -->
                                            <!-- Compare -->
                                            <div class="product-grid-item__compare">
                                                <a href="#" class="open-tooltip"><span class="custom-tooltip">Add to
                                                        compare</span><i class="fas fa-shopping-cart"></i>
                                                    </a>
                                            </div>
                                            <!-- End Compare -->
                                        </div>
                                        <!-- End d-flex -->
                                    </div>
                                    <!-- End product action -->
                                    <!-- Product name -->
                                    <div class="product-grid-item__name">
                                        <a href="/chi-tiet-san-pham/{{$value->the_loai}}/{{ $value->slug_san_pham }}/{{ $value->id }}">
                                            {{ $value->ten_san_pham }}
                                        </a>
                                    </div>
                                    <!-- End product name -->
                                    <!-- Product price -->
                                    <div class="product-grid-item__price">
                                        <!-- Price new -->
                                        <span class="product-grid-item__price-new">{{ number_format($value->gia_ban) }}
                                            ₫</span>
                                        <!-- End price new -->
                                        {{-- <!-- Price old -->
                            <span class="product-grid-item__price-old">10%</span>
                            <!-- End price old --> --}}
                                    </div>
                                    <!-- End product price -->
                                </div>

                            </div>
                        @endif
                    @endforeach
                </div>
                <!-- End slick products -->
            </div>
            <!-- End container -->
        </div>

        <!-- Features -->
        <div class="about-page__features">
            <!-- Row -->
            <div class="row">
                <!-- Feature -->
                <div class="col-lg-4">
                    <div class="about-feature">
                        <!-- Icon -->
                        <div class="about-feature__icon">
                            <i class="lnil lnil-book"></i>
                        </div>
                        <!-- End icon -->
                        <!-- Text -->
                        <div class="about-feature__text">
                            <!-- Title -->
                            <h3 class="about-feature__title">CHẤT LƯỢNG SÁCH TỐT</h3>
                            <!-- End title -->
                            <!-- Description -->
                            <div class="about-feature__description">100% sách được xuất bản uy tín và chất lượng.</div>
                            <!-- End description -->
                        </div>
                        <!-- End text -->
                    </div>
                </div>
                <!-- End feature -->
                <!-- Feature -->
                <div class="col-lg-4">
                    <div class="about-feature">
                        <!-- Icon -->
                        <div class="about-feature__icon">
                            <i class="lnil lnil-ship"></i>
                        </div>
                        <!-- End icon -->
                        <!-- Text -->
                        <div class="about-feature__text">
                            <!-- Title -->
                            <h3 class="about-feature__title">Giao hàng nhanh</h3>
                            <!-- End title -->
                            <!-- Description -->
                            <div class="about-feature__description">Giao hàng nhanh chóng từ 1 - 3 ngày </div>
                            <!-- End description -->
                        </div>
                        <!-- End text -->
                    </div>
                </div>
                <!-- End feature -->
                <!-- Feature -->
                <div class="col-lg-4">
                    <div class="about-feature">
                        <!-- Icon -->
                        <div class="about-feature__icon">
                            <i class="lnil lnil-money-protection"></i>
                        </div>
                        <!-- End icon -->
                        <!-- Text -->
                        <div class="about-feature__text">
                            <!-- Title -->
                            <h3 class="about-feature__title">Thanh toán an toàn</h3>
                            <!-- End title -->
                            <!-- Description -->
                            <div class="about-feature__description"> Thanh toán trực tuyến an toàn </div>
                            <!-- End description -->
                        </div>
                        <!-- End text -->
                    </div>
                </div>
                <!-- End feature -->
            </div>
            <!-- End row -->
        </div>
        <!-- End features -->

        {{-- shop --}}
        <div class="collection">
            <div class="container">
                <!-- Title -->
                <h4 class="our-products__title font-family-jost text-center border-top">shop</h4>
                <!-- End title -->
            </div>
            <!-- Container -->
            <div class="container container--type-3 pt-5">
                <!-- Products row -->
                <div class="row products-row products-row--type-2">
                    <!-- Product -->
                    @foreach ($Sanpham as $key => $value)
                        <div class="col-6 col-md-4 col-xl-3 col-xxl-25">
                            <div class="product-grid-item">
                                <!-- Product images -->
                                <div class="product-grid-item__images js-product-grid-images" data-current-image="0">
                                    <!-- Product images arrows -->
                                    <div class="product-grid-item__images-arrows">
                                    </div>
                                    <!-- End product images arrows -->
                                    <!-- Product image -->
                                    <div class="product-grid-item__image js-product-grid-image active">
                                        <a href="/chi-tiet-san-pham/{{ $value->the_loai}}/{{ $value->slug_san_pham }}/{{ $value->id }}">
                                            <img alt="Image" data-sizes="auto"
                                                data-srcset="{{ $value->hinh_anh }} 400w, {{ $value->hinh_anh }} 800w"
                                                src="{{ $value->hinh_anh }}" class="lazyload" />
                                        </a>
                                    </div>
                                    <!-- End product image -->
                                </div>
                                <!-- End product images -->
                                <!-- Product action -->
                                <div class="product-grid-item__action">
                                    <!-- D-flex -->
                                    <div class="d-flex align-items-center ">
                                        <!-- Add to cart -->
                                        <div class="product-grid-item__add-to-cart">
                                            @if ($check)
                                                <a href="javascript:void(0)"
                                                    v-on:click="addToCart({{ $value->id }})">Add to cart</a>
                                            @else
                                                <form method="POST" action="/client/them-so-luong/{{ $value->id }}">
                                                    @csrf
                                                    <button type="submit"
                                                        style="border: none; background-color: transparent; cursor: pointer; text-decoration: none; color: #928656; padding: 0; font-size: inherit; outline: none; ">ADD
                                                        TO CART</button>
                                                </form>
                                            @endif
                                        </div>

                                        <!-- End add to cart -->
                                        <!-- Quickview -->
                                        <div class="product-grid-item__quickview">
                                            <a href="#" class="open-tooltip"><span class="custom-tooltip">Quick
                                                    view</span><i class="lnil lnil-full-screen"></i></a>
                                        </div>
                                        <!-- End quickview -->
                                        <!-- Wishlist -->
                                        <div class="product-grid-item__wishlist">
                                            <a href="#" class="open-tooltip"><span class="custom-tooltip">Add to
                                                    wishlist</span><i class="lnil lnil-heart"></i></a>
                                        </div>
                                        <!-- End wishlist -->
                                        <!-- Compare -->
                                        <div class="product-grid-item__compare">
                                            <a href="#" class="open-tooltip"><span class="custom-tooltip">Add to
                                                    compare</span><i class="lnil lnil-reload"></i></a>
                                        </div>
                                        <!-- End Compare -->
                                    </div>
                                    <!-- End d-flex -->
                                </div>
                                <!-- End product action -->
                                <!-- Product name -->
                                <div class="product-grid-item__name">
                                    <a href="/chi-tiet-san-pham/{{$value->the_loai}}/{{ $value->slug_san_pham }}/{{ $value->id }}">
                                        {{ $value->ten_san_pham }}
                                    </a>
                                </div>
                                <!-- End product name -->
                                <!-- Product price -->
                                <div class="product-grid-item__price">
                                    <!-- Price new -->
                                    <span class="product-grid-item__price-new">{{ number_format($value->gia_ban) }}
                                        ₫</span>
                                    <!-- End price new -->
                                    {{-- <!-- Price old -->
                        <span class="product-grid-item__price-old">10%</span>
                        <!-- End price old --> --}}
                                </div>
                                <!-- End product price -->
                            </div>

                        </div>
                    @endforeach
                    <!-- End product -->
                </div>
                <!-- End Products row -->
            </div>
            <!-- End container -->
        </div>
        <div class="pagination-wrapper">
            {{ $Sanpham->links('pagination::bootstrap-4') }}
        </div>
        {{-- shop --}}


        <!-- Canvas cart -->
        <div class="canvas-cart js-canvas-cart">
            <div class="canvas-cart__overlay js-close-canvas-cart"></div>
            <!-- Content -->
            <div class="canvas-cart__content">
                <!-- D-flex -->
                <div class="canvas-cart__d-flex">
                    <!-- Top and products -->
                    <div class="canvas-cart__top-and-products">
                        <!-- Heading -->
                        <div class="canvas-cart__heading d-flex align-items-center">
                            <!-- H3 -->
                            <h3 class="canvas-cart__h3">Cart (3)</h3>
                            <!-- End h3 -->
                            <!-- Close -->
                            <div class="canvas-cart__close"><a href="#" class="js-close-canvas-cart"><i
                                        class="lnil lnil-close"></i></a></div>
                            <!-- End close -->
                        </div>
                        <!-- End heading -->
                        <!-- Cart items -->
                        <ul class="header-cart__items">
                            <!-- Use Vue.js v-if to conditionally render the cart items -->
                            <div v-if="ds_cart">
                                <ul class="cart-items">
                                    <li class="cart-item d-flex" v-for="(value, key) in ds_cart" :key="key">
                                        <!-- Item image -->
                                        <p class="cart-item__image">
                                            <a>
                                                <img alt="Image" data-sizes="auto"
                                                    :data-srcset="value.hinh_anh + ' 400w, ' + value.hinh_anh + ' 800w'"
                                                    :src="value.hinh_anh" class="lazyload" />
                                            </a>
                                        </p>
                                        <!-- End item image -->
                                        <!-- Item details -->
                                        <p class="cart-item__details">
                                            <a class="cart-item__title">
                                                @{{ value.ten_san_pham }}
                                            </a>
                                            <span class="cart-item__price">@{{ value.tong_so_luong }} <i>x</i>
                                                @{{ formatCurrency(value.gia_ban) }}</span>
                                        </p>
                                        <!-- End item details -->
                                        <!-- Item quantity -->
                                        <div class="cart-item__quantity">
                                            <div class="cart-product__quantity-field">
                                                <div class="quantity-field__minus js-quantity-down">
                                                    <a href="javascript:void(0)"
                                                        v-on:click="tru_so_luong(value.ma_san_pham)">-</a>
                                                </div>
                                                <input type="text" :value="value.tong_so_luong"
                                                    class="quantity-field__input js-quantity-field" readonly />
                                                <div class="quantity-field__plus js-quantity-up">
                                                    <a href="javascript:void(0)"
                                                        v-on:click="addToCart(value.ma_san_pham)">+</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End item quantity -->
                                        <!-- Item delete -->
                                        <p class="cart-item__delete">
                                            <a href="javascript:void(0)"><i class="lnil lnil-close"  v-on:click="xoa_san_pham_gio_hang(value.ma_san_pham)"></i></a>
                                        </p>
                                        <!-- End item delete -->
                                    </li>
                                </ul>
                            </div>
                            <div v-else>
                                <p>Giỏ hàng trống.</p>
                            </div>

                        </ul>
                        <!-- End cart items -->
                    </div>
                    <!-- End top and products -->
                    <!-- Bottom -->
                    <div class="canvas-cart__bottom">
                        <!-- Subtotal -->
                        <div class="header-cart__subtotal d-flex">
                            <!-- Title -->
                            <div class="subtotal__title">Subtotal</div>
                            <!-- End title -->
                            <!-- Value -->
                            <div class="subtotal__value" v-if="tong_tien > 0">@{{ formatCurrency(tong_tien) }}</div>
                            <div class="subtotal__value" v-else>0₫</div>
                            <!-- End value -->
                        </div>
                        <!-- End subtotal -->
                        <!-- Header cart action -->
                        <div class="header-cart__action">
                            <a href="/gio-hang" class="header-cart__button">View cart</a>
                            <a href="/thanh-toan" class="header-cart__button">Checkout</a>
                        </div>
                        <!-- End Header cart action -->
                    </div>
                    <!-- End bottom -->
                </div>
                <!-- End d-flex -->
            </div>
            <!-- End content -->
        </div>
        <!-- End canvas cart -->
    </div>
@endsection
@section('js')
    <script>
        new Vue({
            el: "#app",
            data: {
                ds_cart: [],
                tong_tien: 0,
            },
            created() {
                this.loadCart();
            },
            methods: {
                addToCart(id) {
                    axios
                        .post('/client/them-so-luong/' + id)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadCart();
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        })
                },

                loadCart() {
                    axios
                        .get('/client/hien-thi-ds-gio-hang')
                        .then((res) => {
                            this.ds_cart = res.data.gio_hang;
                            this.tong_tien = res.data.tong_tien_tat_ca
                            console.log(this.ds_cart);
                        });
                },
                formatCurrency(value) {
                    const formatter = new Intl.NumberFormat('vi-VN', {
                        style: 'currency',
                        currency: 'VND',
                    });
                    return formatter.format(value);
                },
                tru_so_luong(id) {
                    axios
                        .post('/client/tru-so-luong/' + id)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadCart();
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        });
                },
                xoa_san_pham_gio_hang(id) {
                    axios
                        .post('/client/xoa-san-pham-gio-hang/' + id)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadCart();
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        });
                },
            }
        });
    </script>
@endsection
