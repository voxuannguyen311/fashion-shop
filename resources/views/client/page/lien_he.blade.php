@extends('client.share.master')
@section('noi_dung')
    <!-- Contact page -->
    <div class="contact-page" id="app">

        <!-- Container -->
        <div class="container container--type-2">
            <!-- Title -->
            <h1 class="contact-page__title">Contact Us</h1>
            <!-- End title -->
            <!-- Description -->
            <div class="contact-page__description">We love to hear from you on our customer service, merchandise,<br>website
                or any topics you want to share with us</div>
            <!-- End description -->
            <!-- Google map -->
            <div class="contact-page__google-map">
                <iframe width="600" height="570" id="gmap_canvas"
                    src="https://maps.google.com/maps?q=2880%20Broadway,%20New%20York&amp;t=&amp;z=13&amp;ie=UTF8&amp;iwloc=&amp;output=embed"
                    frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
            </div>
            <!-- End google map -->
            <!-- Section -->
            <div class="contact-page__section">
                <!-- Container -->
                <div class="container">
                    <!-- Row -->
                    <div class="row">
                        <!-- Title -->
                        <div class="col-lg-3">
                            <h3 class="contact-section__title">Contact Information</h3>
                        </div>
                        <!-- End title -->
                        <!-- Content -->
                        <div class="col-lg-9">
                            <!-- Stores list -->
                            <div class="stores-list">
                                <!-- Row -->
                                <div class="row">
                                    <!-- Store -->
                                    <div class="col-lg-6">
                                        <div class="stores-list__item">
                                            <!-- Title -->
                                            <h3 class="store-item__title">New York</h3>
                                            <!-- End title -->
                                            <!-- Address -->
                                            <div class="store-item__address">
                                                <!-- Store 1 -->
                                                <h4 class="address__store-number">Store 1</h4>
                                                <p>
                                                    68 Atlantic Ave St, Brooklyn, NY 90002, USA<br>
                                                    (+005) 5896 72 78 79<br>
                                                    <a href="mailto:hellony@durotan.com.us">hellony@durotan.com.us</a>
                                                </p>
                                                <!-- End store 1 -->
                                                <!-- Store 1 -->
                                                <h4 class="address__store-number">Store 2</h4>
                                                <p>
                                                    172 Richmond Hill Ave St, Stamford, NY 90002, USA <br>
                                                    (+005) 5896 03 04 05
                                                </p>
                                                <!-- End store 1 -->
                                            </div>
                                            <!-- End address -->
                                        </div>
                                    </div>
                                    <!-- End store -->
                                    <!-- Store -->
                                    <div class="col-lg-6">
                                        <div class="stores-list__item">
                                            <!-- Title -->
                                            <h3 class="store-item__title">London</h3>
                                            <!-- End title -->
                                            <!-- Address -->
                                            <div class="store-item__address">
                                                <!-- Store 1 -->
                                                <p>
                                                    88 Landsome Way St, Stockwell, London 534, UK<br>
                                                    (+089) 5896 26 26 27<br>
                                                    <a href="mailto:hellold@durotan.com.uk">hellold@durotan.com.uk</a>
                                                </p>
                                                <!-- End store 1 -->
                                            </div>
                                            <!-- End address -->
                                        </div>
                                    </div>
                                    <!-- End store -->
                                    <!-- Social media -->
                                    <div class="col-lg-6">
                                        <div class="stores-list__item">
                                            <!-- Title -->
                                            <h3 class="store-item__title">Social</h3>
                                            <!-- End title -->
                                            <ul class="footer__socials">
                                                <li><a href="https://twitter.com/" target="_blank"><i
                                                            class="lnil lnil-twitter"></i></a></li>
                                                <li><a href="https://facebook.com/" target="_blank"><i
                                                            class="lnil lnil-facebook"></i></a></li>
                                                <li><a href="https://instagram.com/" target="_blank"><i
                                                            class="lnil lnil-Instagram"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- End social media -->
                                </div>
                                <!-- End row -->
                            </div>
                            <!-- End stores list -->
                        </div>
                        <!-- End content -->
                    </div>
                    <!-- End row -->
                </div>
                <!-- End container -->
            </div>
            <!-- End section -->
            <!-- Section -->
            <div class="contact-page__section">
                <!-- Container -->
                <div class="container">
                    <!-- Line 1 px -->
                    <hr />
                    <!-- End line 1x px -->
                    <!-- Row -->
                    <div class="row">
                        <!-- Title -->
                        <div class="col-lg-3">
                            <h3 class="contact-section__title">Drop Us A<br>Line</h3>
                        </div>
                        <!-- End title -->
                        <!-- Content -->
                        <div class="col-lg-9">
                            <!-- Form -->
                            <form class="contact-page__form">
                                <!-- Required fields -->
                                <div class="form__required-fields">Required fields are marked<span>*</span></div>
                                <!-- End required fields -->
                                <!-- Form group -->
                                <div class="form-group">
                                    <input type="text" name="subject" v-model="themmoi.tieu_de" class="form-group__input"
                                        placeholder="Subject (optional)">
                                </div>
                                <!-- End form group -->
                                <!-- Form group -->
                                <div class="form-group">
                                    <textarea  v-model="themmoi.noi_dung" placeholder="Write your message here" class="form-group__textarea" rows="5"></textarea>
                                </div>
                                <!-- End form group -->
                                <!-- Row -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <!-- Form group -->
                                        <div class="form-group">
                                            <input type="text"  v-model="themmoi.ho_va_ten" name="name" class="form-group__input"
                                                placeholder="Full Name">
                                        </div>
                                        <!-- End form group -->
                                    </div>
                                    <div class="col-md-6">
                                        <!-- Form group -->
                                        <div class="form-group">
                                            <input type="email" v-model="themmoi.email" name="email" class="form-group__input"
                                                placeholder="Your E-mail*">
                                        </div>
                                        <!-- End form group -->
                                    </div>
                                </div>
                                <!-- End row -->
                                <!-- Action -->
                                <div class="form__action">
                                    <button type="submit" v-on:click="sendLienHen()" class="second-button">Send message</button>
                                </div>
                                <!-- End action -->
                            </form>
                            <!-- End form -->
                        </div>
                        <!-- End content -->
                    </div>
                    <!-- End row -->
                </div>
                <!-- End container -->
            </div>
            <!-- End section -->
        </div>
        <!-- End container -->
       <!-- Canvas cart -->
       <div class="canvas-cart js-canvas-cart">
        <div class="canvas-cart__overlay js-close-canvas-cart"></div>
        <!-- Content -->
        <div class="canvas-cart__content">
            <!-- D-flex -->
            <div class="canvas-cart__d-flex">
                <!-- Top and products -->
                <div class="canvas-cart__top-and-products">
                    <!-- Heading -->
                    <div class="canvas-cart__heading d-flex align-items-center">
                        <!-- H3 -->
                        <h3 class="canvas-cart__h3">Cart (3)</h3>
                        <!-- End h3 -->
                        <!-- Close -->
                        <div class="canvas-cart__close"><a href="#" class="js-close-canvas-cart"><i
                                    class="lnil lnil-close"></i></a></div>
                        <!-- End close -->
                    </div>
                    <!-- End heading -->
                    <!-- Cart items -->
                    <ul class="header-cart__items">
                        <!-- Use Vue.js v-if to conditionally render the cart items -->
                        <div v-if="ds_cart">
                            <ul class="cart-items">
                                <li class="cart-item d-flex" v-for="(value, key) in ds_cart" :key="key">
                                    <!-- Item image -->
                                    <p class="cart-item__image">
                                        <a>
                                            <img alt="Image" data-sizes="auto"
                                                :data-srcset="value.hinh_anh + ' 400w, ' + value.hinh_anh + ' 800w'"
                                                :src="value.hinh_anh" class="lazyload" />
                                        </a>
                                    </p>
                                    <!-- End item image -->
                                    <!-- Item details -->
                                    <p class="cart-item__details">
                                        <a class="cart-item__title">
                                            @{{ value.ten_san_pham }}
                                        </a>
                                        <span class="cart-item__price">@{{ value.tong_so_luong }} <i>x</i>
                                            @{{ formatCurrency(value.gia_ban) }}</span>
                                    </p>
                                    <!-- End item details -->
                                    <!-- Item quantity -->
                                    <div class="cart-item__quantity">
                                        <div class="cart-product__quantity-field">
                                            <div class="quantity-field__minus js-quantity-down">
                                                <a href="javascript:void(0)"
                                                    v-on:click="tru_so_luong(value.ma_san_pham)">-</a>
                                            </div>
                                            <input type="text" :value="value.tong_so_luong"
                                                class="quantity-field__input js-quantity-field" readonly />
                                            <div class="quantity-field__plus js-quantity-up">
                                                <a href="javascript:void(0)"
                                                    v-on:click="addToCart(value.ma_san_pham)">+</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End item quantity -->
                                    <!-- Item delete -->
                                    <p class="cart-item__delete">
                                        <a href="javascript:void(0)"><i class="lnil lnil-close"  v-on:click="xoa_san_pham_gio_hang(value.ma_san_pham)"></i></a>
                                    </p>
                                    <!-- End item delete -->
                                </li>
                            </ul>
                        </div>
                        <div v-else>
                            <p>Giỏ hàng trống.</p>
                        </div>

                    </ul>
                    <!-- End cart items -->
                </div>
                <!-- End top and products -->
                <!-- Bottom -->
                <div class="canvas-cart__bottom">
                    <!-- Subtotal -->
                    <div class="header-cart__subtotal d-flex">
                        <!-- Title -->
                        <div class="subtotal__title">Subtotal</div>
                        <!-- End title -->
                        <!-- Value -->
                        <div class="subtotal__value" v-if="tong_tien > 0">@{{ formatCurrency(tong_tien) }}</div>
                        <div class="subtotal__value" v-else>0₫</div>
                        <!-- End value -->
                    </div>
                    <!-- End subtotal -->
                    <!-- Header cart action -->
                    <div class="header-cart__action">
                        <a href="/gio-hang" class="header-cart__button">View cart</a>
                        <a href="/thanh-toan" class="header-cart__button">Checkout</a>
                    </div>
                    <!-- End Header cart action -->
                </div>
                <!-- End bottom -->
            </div>
            <!-- End d-flex -->
        </div>
        <!-- End content -->
    </div>
    <!-- End canvas cart -->
    </div>
    <!-- End contact page -->



@endsection


@section('js')
    <script>
        new Vue({
            el: '#app',
            data: {
                themmoi: {},
                ds_cart: [],
                tong_tien: 0,
            },
            created() {
                this.loadCart();
            },
            methods: {
                sendLienHen() {
                    axios
                        .post('/gui-lien-he', this.themmoi)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.themmoi = {};
                            }
                        })
                        .catch((res) => {
                            $.each(res.response.data.errors, function(k, v) {
                                toastr.error(v[0]);
                            });
                        });
                },
                addToCart(id) {
                    axios
                        .post('/client/them-so-luong/' + id)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadCart();
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        })
                },

                loadCart() {
                    axios
                        .get('/client/hien-thi-ds-gio-hang')
                        .then((res) => {
                            this.ds_cart = res.data.gio_hang;
                            this.tong_tien = res.data.tong_tien_tat_ca
                            console.log(this.ds_cart);
                        });
                },
                formatCurrency(value) {
                    const formatter = new Intl.NumberFormat('vi-VN', {
                        style: 'currency',
                        currency: 'VND',
                    });
                    return formatter.format(value);
                },
                tru_so_luong(id) {
                    axios
                        .post('/client/tru-so-luong/' + id)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadCart();
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        });
                },
                xoa_san_pham_gio_hang(id) {
                    axios
                        .post('/client/xoa-san-pham-gio-hang/' + id)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadCart();
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        });
                },
            },
        });
    </script>
@endsection
