@extends('client.share.master')
@section('noi_dung')
    <!-- Shopping cart -->
    <div class="shopping-cart" id="app">
        <!-- Container -->
        <div class="container container--type-2">
            <!-- Second container -->
            <div class="container">
                <!-- Title -->
                <h1 class="shopping-cart__title">Shopping Cart</h1>
                <!-- End title -->
                <!-- Row -->
                <div class="row">
                    <!-- Left -->
                    <div class="col-lg-7 col-xl-8">
                        <!-- Cart container -->
                        <div class="shopping-cart__container">
                            <!--- Table responsive -->
                            <div class="table-responsive">
                                <!-- Table -->
                                <table class="shopping-cart__table">
                                    <thead>
                                        <tr>
                                            <th>Product</th>
                                            <th>Price</th>
                                            <th>Qty</th>
                                            <th>Subtotal</th>
                                            <th></th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <!-- Cart product item -->
                                        <tr v-for="(value, key) in ds_cart" :key="key">
                                            <td>
                                                <div class="shopping-cart__product">
                                                    <div class="cart-product__image">
                                                        <a>
                                                            <img alt="Image" data-sizes="auto"
                                                                :data-srcset="value.hinh_anh + ' 400w, ' + value.hinh_anh + ' 800w'"
                                                                :src="value.hinh_anh" class="lazyload" />
                                                        </a>
                                                    </div>
                                                    <div class="cart-product__title-and-variant">
                                                        <h3 class="cart-product__title"><a href="product.html">
                                                                @{{ value.ten_san_pham }}</a></h3>
                                                        <div class="cart-product__action"><a href="#">Edit</a></div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="cart-product__price">
                                                    @{{ formatCurrency(value.gia_ban) }}</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="cart-product__quantity-field">
                                                    <div class="quantity-field__minus js-quantity-down">
                                                        <a href="javascript:void(0)" v-on:click="tru_so_luong(value.id)">-</a>
                                                    </div>
                                                    <input type="text" :value="value.tong_so_luong"
                                                        class="quantity-field__input"  readonly/>
                                                        <div class="quantity-field__plus js-quantity-up">
                                                            <a href="javascript:void(0)" v-on:click="addToCart(value.id)">+</a>
                                                        </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="cart-product__price">
                                                    @{{ formatCurrency(value.tong_tien) }}
                                                </div>
                                            </td>
                                            <td>
                                                <div class="cart-product__delete">
                                                    <a href="javascript:void(0)"><i class="lnil lnil-close"  v-on:click="xoa_san_pham_gio_hang(value.ma_san_pham)"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- End cart product item -->

                                    </tbody>
                                </table>
                                <!-- End table -->
                            </div>
                            <!-- End table responsive -->
                            <!-- Cart discount -->
                            <div class="shopping-cart__discount">
                                <!-- Title -->
                                <h3 class="discount__title">Discount Code</h3>
                                <!-- End title -->
                                <!-- Form -->
                                <form>
                                    <!-- Form icon -->
                                    <div class="discount__icon"><i class="lnil lnil-coin"></i></div>
                                    <!-- Form icon -->
                                    <!-- Form input -->
                                    <input type="text" class="discount__input" value=""
                                        placeholder="Enter promo code" />
                                    <!-- End form input -->
                                    <!-- Button -->
                                    <button type="submit" class="discount__submit">Apply coupon</button>
                                    <!-- End button -->
                                </form>
                                <!-- End form -->
                            </div>
                            <!-- End cart discount -->
                        </div>
                        <!-- End cart container -->
                    </div>
                    <!-- End left -->

                    <!-- Right -->
                    <div class="col-lg-5 col-xl-4">
                        <!-- Order summary -->
                        <div class="shopping-cart__order-summary">
                            <!-- Background -->
                            <div class="order-summary__background">
                                <!-- Title -->
                                <h3 class="order-summary__title">Order Summary</h3>
                                <!-- End title -->
                                <!-- Subtotal -->
                                <div class="order-summary__subtotal">
                                    <div class="summary-total__title">Total</div>
                                    <div class="summary-total__price" v-if="tong_tien > 0">@{{ formatCurrency(tong_tien) }}</div>
                                    <div class="summary-total__price" v-else>$0</div>
                                </div>

                                <div class="order-summary__proceed-to-checkout">
                                        <a href="/gio-hang" class="second-button">Thanh toán</a>
                                </div>
                                <!-- End proceed to checkout -->

                                <!-- Accept payment methods -->
                                <div class="order-summary__accept-payment-methods">
                                    <h4 class="accept-payment-methods__title">Accept Payment Methods</h4>
                                    <img src="assets/images/default/payment.png" alt="Payment" />
                                </div>
                                <!-- Accept payment methods -->
                            </div>
                            <!-- End background -->
                            <!-- Action -->
                            <div class="order-summary__action">
                                <a href="shop.html">Continue shopping</a>
                            </div>
                            <!-- End action -->
                        </div>
                        <!-- End order summary -->
                    </div>
                    <!-- End right -->
                </div>
                <!-- End row -->
            </div>
            <!-- End second container -->
        </div>
        <!-- End container -->
        <!-- Canvas cart -->
        <div class="canvas-cart js-canvas-cart">
            <div class="canvas-cart__overlay js-close-canvas-cart"></div>
            <!-- Content -->
            <div class="canvas-cart__content">
                <!-- D-flex -->
                <div class="canvas-cart__d-flex">
                    <!-- Top and products -->
                    <div class="canvas-cart__top-and-products">
                        <!-- Heading -->
                        <div class="canvas-cart__heading d-flex align-items-center">
                            <!-- H3 -->
                            <h3 class="canvas-cart__h3">Cart (3)</h3>
                            <!-- End h3 -->
                            <!-- Close -->
                            <div class="canvas-cart__close"><a href="#" class="js-close-canvas-cart"><i
                                        class="lnil lnil-close"></i></a></div>
                            <!-- End close -->
                        </div>
                        <!-- End heading -->
                        <!-- Cart items -->
                        <ul class="header-cart__items">
                            <!-- Use Vue.js v-if to conditionally render the cart items -->
                            <div v-if="ds_cart">
                                <ul class="cart-items">
                                    <li class="cart-item d-flex" v-for="(value, key) in ds_cart" :key="key">
                                        <!-- Item image -->
                                        <p class="cart-item__image">
                                            <a>
                                                <img alt="Image" data-sizes="auto"
                                                    :data-srcset="value.hinh_anh + ' 400w, ' + value.hinh_anh + ' 800w'"
                                                    :src="value.hinh_anh" class="lazyload" />
                                            </a>
                                        </p>
                                        <!-- End item image -->
                                        <!-- Item details -->
                                        <p class="cart-item__details">
                                            <a class="cart-item__title">
                                                @{{ value.ten_san_pham }}
                                            </a>
                                            <span class="cart-item__price">@{{ value.tong_so_luong }} <i>x</i>
                                                @{{ formatCurrency(value.gia_ban) }}</span>
                                        </p>
                                        <!-- End item details -->
                                        <!-- Item quantity -->
                                        <div class="cart-item__quantity">
                                            <div class="cart-product__quantity-field">
                                                <div class="quantity-field__minus js-quantity-down">
                                                    <a href="javascript:void(0)"
                                                        v-on:click="tru_so_luong(value.ma_san_pham)">-</a>
                                                </div>
                                                <input type="text" :value="value.tong_so_luong"
                                                    class="quantity-field__input js-quantity-field" readonly />
                                                <div class="quantity-field__plus js-quantity-up">
                                                    <a href="javascript:void(0)"
                                                        v-on:click="addToCart(value.ma_san_pham)">+</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End item quantity -->
                                        <!-- Item delete -->
                                        <p class="cart-item__delete">
                                            <a href="javascript:void(0)"><i class="lnil lnil-close"  v-on:click="xoa_san_pham_gio_hang(value.ma_san_pham)"></i></a>
                                        </p>
                                        <!-- End item delete -->
                                    </li>
                                </ul>
                            </div>
                            <div v-else>
                                <p>Giỏ hàng trống.</p>
                            </div>

                        </ul>
                        <!-- End cart items -->
                    </div>
                    <!-- End top and products -->
                    <!-- Bottom -->
                    <div class="canvas-cart__bottom">
                        <!-- Subtotal -->
                        <div class="header-cart__subtotal d-flex">
                            <!-- Title -->
                            <div class="subtotal__title">Subtotal</div>
                            <!-- End title -->
                            <!-- Value -->
                            <div class="subtotal__value" v-if="tong_tien > 0">@{{ formatCurrency(tong_tien) }}</div>
                            <div class="subtotal__value" v-else>0₫</div>
                            <!-- End value -->
                        </div>
                        <!-- End subtotal -->
                        <!-- Header cart action -->
                        <div class="header-cart__action">
                            <a href="/gio-hang" class="header-cart__button">View cart</a>
                            <a href="/thanh-toan" class="header-cart__button">Checkout</a>
                        </div>
                        <!-- End Header cart action -->
                    </div>
                    <!-- End bottom -->
                </div>
                <!-- End d-flex -->
            </div>
            <!-- End content -->
        </div>
        <!-- End canvas cart -->
    </div>
    <!-- End shopping cart -->
@endsection
@section('js')
    <script>
        new Vue({
            el: "#app",
            data: {
                ds_cart: [],
                tong_tien: 0,
            },
            created() {
                this.loadCart();
            },
            methods: {
                addToCart(id) {
                    axios
                        .post('/client/them-so-luong/' + id)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadCart();
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        })
                },

                loadCart() {
                    axios
                        .get('/client/hien-thi-ds-gio-hang')
                        .then((res) => {
                            this.ds_cart = res.data.gio_hang;
                            this.tong_tien = res.data.tong_tien_tat_ca
                            console.log(this.ds_cart);
                        });
                },
                formatCurrency(value) {
                    const formatter = new Intl.NumberFormat('vi-VN', {
                        style: 'currency',
                        currency: 'VND',
                    });
                    return formatter.format(value);
                },
                tru_so_luong(id) {
                    axios
                        .post('/client/tru-so-luong/' + id)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadCart();
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        });
                },
                xoa_san_pham_gio_hang(id) {
                    axios
                        .post('/client/xoa-san-pham-gio-hang/' + id)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadCart();
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        });
                },
            }
        });
    </script>
@endsection

