@extends('client.share.master')
@section('noi_dung')

<!-- Blog page -->
<div class="blog" id="app">
    <!-- Container -->
    <div class="container container--type-2">
      <!-- Title -->
      <h1 class="blog__title">Our Journal</h1>
      <!-- End title -->
      <!-- Description -->
      <div class="blog__description">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humou</div>
      <!-- End description -->
      <!-- Featured articles -->
      <div class="blog__featured-articles js-featured-articles">
        <!-- Article -->
        <div class="featured-article">
          <!-- Image -->
          <div class="featured-article__image">
            <a href="post.html">
              <img
                alt="Image"
                data-sizes="auto"
                data-srcset="assets/images/default/blog_post.jpg 400w,
                assets/images/default/blog_post.jpg 800w"
                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                class="lazyload" />
            </a>
          </div>
          <!-- End image -->
          <!-- Details -->
          <div class="featured-article__details">
            <!-- Container -->
            <div class="container">
              <!-- Meta -->
              <ul class="featured-article__meta">
                <li><a href="#">Inspiration</a></li>
                <li>Dec 24, 2022</li>
              </ul>
              <!-- End meta -->
              <!-- Title -->
              <h3 class="featured-article__title">
                <a href="post.html">Ready for the winter! Discover the new collections of Durotan</a>
              </h3>
              <!-- End title -->
            </div>
            <!-- End container -->
          </div>
          <!-- End details -->
        </div>
        <!-- End article -->
        <!-- Article -->
        <div class="featured-article">
          <!-- Image -->
          <div class="featured-article__image">
            <a href="post.html">
              <img
                alt="Image"
                data-sizes="auto"
                data-srcset="assets/images/default/blog_post.jpg 400w,
                assets/images/default/blog_post.jpg 800w"
                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                class="lazyload" />
            </a>
          </div>
          <!-- End image -->
          <!-- Details -->
          <div class="featured-article__details">
            <!-- Container -->
            <div class="container">
              <!-- Meta -->
              <ul class="featured-article__meta">
                <li><a href="#">Inspiration</a></li>
                <li>Dec 24, 2022</li>
              </ul>
              <!-- End meta -->
              <!-- Title -->
              <h3 class="featured-article__title">
                <a href="post.html">Second for the winter! Discover the new collections of Durotan</a>
              </h3>
              <!-- End title -->
            </div>
            <!-- End container -->
          </div>
          <!-- End details -->
        </div>
        <!-- End article -->
        <!-- Article -->
        <div class="featured-article">
          <!-- Image -->
          <div class="featured-article__image">
            <a href="post.html">
              <img
                alt="Image"
                data-sizes="auto"
                data-srcset="assets/images/default/blog_post.jpg 400w,
                assets/images/default/blog_post.jpg 800w"
                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                class="lazyload" />
            </a>
          </div>
          <!-- End image -->
          <!-- Details -->
          <div class="featured-article__details">
            <!-- Container -->
            <div class="container">
              <!-- Meta -->
              <ul class="featured-article__meta">
                <li><a href="#">Inspiration</a></li>
                <li>Dec 24, 2022</li>
              </ul>
              <!-- End meta -->
              <!-- Title -->
              <h3 class="featured-article__title">
                <a href="post.html">Third for the winter! Discover the new collections of Durotan</a>
              </h3>
              <!-- End title -->
            </div>
            <!-- End container -->
          </div>
          <!-- End details -->
        </div>
        <!-- End article -->
      </div>
      <!-- End featured articles -->
      <!-- Latest articles -->
      <div class="blog__latest-articles">
        <!-- Container -->
        <div class="container">
          <!-- Title -->
          <h4 class="latest-articles__title">Latest Articles</h4>
          <!-- End title -->
          <!-- Articles -->
          <div class="latest-articles js-latest-articles">
            <!-- Post -->
            <div class="our-journal__post">
              <!-- Post image -->
              <div class="our-journal__post-image">
                <a href="post.html">
                  <img
                    alt="Image"
                    data-sizes="auto"
                    data-srcset="assets/images/default/post_1.jpg 400w,
                    assets/images/default/post_1.jpg 800w"
                    src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                    class="lazyload" />
                </a>
              </div>
              <!-- End post image -->
              <!-- Post info -->
              <div class="our-journal__post-info">
                <!-- Post date -->
                <div class="our-journal__post-date">
                  <span>25</span>
                  December<br>
                  2022
                </div>
                <!-- End post date -->
                <!-- Post details -->
                <div class="our-journal__post-details">
                  <!-- Post title -->
                  <h5 class="our-journal__post-title"><a href="post.html">How to choose a sneakers suit for any your style look impressive</a></h5>
                  <!-- End post title -->
                  <!-- Post meta -->
                  <ul class="our-journal__post-meta">
                    <li><a href="blog.html">Inspiration</a></li>
                    <li>By Admin</li>
                  </ul>
                  <!-- End post meta -->
                </div>
                <!-- End post details -->
              </div>
              <!-- End post info -->
            </div>
            <!-- End post -->
            <!-- Post -->
            <div class="our-journal__post">
              <!-- Post image -->
              <div class="our-journal__post-image">
                <a href="post.html">
                  <img
                    alt="Image"
                    data-sizes="auto"
                    data-srcset="assets/images/default/post_2.jpg 400w,
                    assets/images/default/post_2.jpg 800w"
                    src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                    class="lazyload" />
                </a>
              </div>
              <!-- End post image -->
              <!-- Post info -->
              <div class="our-journal__post-info">
                <!-- Post date -->
                <div class="our-journal__post-date">
                  <span>20</span>
                  December<br>
                  2022
                </div>
                <!-- End post date -->
                <!-- Post details -->
                <div class="our-journal__post-details">
                  <!-- Post title -->
                  <h5 class="our-journal__post-title"><a href="post.html">Your checkout now faster at our store with Google Pay</a></h5>
                  <!-- End post title -->
                  <!-- Post meta -->
                  <ul class="our-journal__post-meta">
                    <li><a href="blog.html">Tips & tricks</a></li>
                    <li>By Logan Cee</li>
                  </ul>
                  <!-- End post meta -->
                </div>
                <!-- End post details -->
              </div>
              <!-- End post info -->
            </div>
            <!-- End post -->
            <!-- Post -->
            <div class="our-journal__post">
              <!-- Post image -->
              <div class="our-journal__post-image">
                <a href="post.html">
                  <img
                    alt="Image"
                    data-sizes="auto"
                    data-srcset="assets/images/default/post_1.jpg 400w,
                    assets/images/default/post_1.jpg 800w"
                    src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                    class="lazyload" />
                </a>
              </div>
              <!-- End post image -->
              <!-- Post info -->
              <div class="our-journal__post-info">
                <!-- Post date -->
                <div class="our-journal__post-date">
                  <span>25</span>
                  December<br>
                  2022
                </div>
                <!-- End post date -->
                <!-- Post details -->
                <div class="our-journal__post-details">
                  <!-- Post title -->
                  <h5 class="our-journal__post-title"><a href="post.html">How to choose a sneakers suit for any your style look impressive</a></h5>
                  <!-- End post title -->
                  <!-- Post meta -->
                  <ul class="our-journal__post-meta">
                    <li><a href="blog.html">Inspiration</a></li>
                    <li>By Admin</li>
                  </ul>
                  <!-- End post meta -->
                </div>
                <!-- End post details -->
              </div>
              <!-- End post info -->
            </div>
            <!-- End post -->
          </div>
          <!-- End articles -->
          <!-- Line 1 px -->
          <hr />
          <!-- End line 1 px -->
        </div>
        <!-- End container -->
      </div>
      <!-- End latest articles -->
      <!-- Container -->
      <div class="container">
        <!-- Categories and search -->
        <div class="blog__categories-and-search">
          <!-- Categories -->
          <ul class="blog__categories">
            <li><a href="#" class="active">All</a></li>
            <li><a href="#">Inspiration</a></li>
            <li><a href="#">Lookbook</a></li>
            <li><a href="#">Tips &amp; tricks</a></li>
            <li><a href="#">News</a></li>
            <li><a href="#">Others</a></li>
          </ul>
          <!-- End categories -->
          <!-- Search -->
          <div class="blog__search">
            <form>
              <input type="text" class="blog-search__input" placeholder="Search in blog" />
              <button type="submit" class="blog-search__button"><i class="lnil lnil-search-alt"></i></button>
            </form>
          </div>
          <!-- End search -->
        </div>
        <!-- End Categories and search -->
        <!-- Blog articles -->
        <div class="blog__articles row">
          <!-- Article -->
          <div class="col-lg-4">
            <div class="blog-article">
              <!-- Image -->
              <div class="blog-article__image">
                <a href="post.html">
                  <img
                    alt="Image"
                    data-sizes="auto"
                    data-srcset="assets/images/default/article_1.jpg 1560w,
                    assets/images/default/article_1.jpg 3120w"
                    src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                    class="lazyload" />
                </a>
              </div>
              <!-- End image -->
              <!-- Meta -->
              <ul class="blog-article__meta">
                <li><a href="#">Inspiration</a></li>
                <li>Dec 24, 2022</li>
                <li>By Admin</li>
              </ul>
              <!-- End meta -->
              <!-- Title -->
              <h5 class="blog-article__title">
                <a href="post.html">How to choose a sneakers suit for any your style</a>
              </h5>
              <!-- End Title -->
            </div>
          </div>
          <!-- End article -->
          <!-- Article -->
          <div class="col-lg-4">
            <div class="blog-article">
              <!-- Image -->
              <div class="blog-article__image">
                <a href="post.html">
                  <img
                    alt="Image"
                    data-sizes="auto"
                    data-srcset="assets/images/default/article_2.jpg 1560w,
                    assets/images/default/article_2.jpg 3120w"
                    src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                    class="lazyload" />
                </a>
              </div>
              <!-- End image -->
              <!-- Meta -->
              <ul class="blog-article__meta">
                <li><a href="#">Tips & Tricks</a></li>
                <li>Dec 24, 2022</li>
                <li>By Admin</li>
              </ul>
              <!-- End meta -->
              <!-- Title -->
              <h5 class="blog-article__title">
                <a href="post.html">How to mixed minimalist fashion style with basic items</a>
              </h5>
              <!-- End Title -->
            </div>
          </div>
          <!-- End article -->
          <!-- Article -->
          <div class="col-lg-4">
            <div class="blog-article">
              <!-- Image -->
              <div class="blog-article__image">
                <a href="post.html">
                  <img
                    alt="Image"
                    data-sizes="auto"
                    data-srcset="assets/images/default/article_3.jpg 1560w,
                    assets/images/default/article_3.jpg 3120w"
                    src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                    class="lazyload" />
                </a>
              </div>
              <!-- End image -->
              <!-- Meta -->
              <ul class="blog-article__meta">
                <li><a href="#">Lookbook</a></li>
                <li>Dec 24, 2022</li>
                <li>By Admin</li>
              </ul>
              <!-- End meta -->
              <!-- Title -->
              <h5 class="blog-article__title">
                <a href="post.html">Hello summer, discover the new sunglasses in lookbook #82</a>
              </h5>
              <!-- End Title -->
            </div>
          </div>
          <!-- End article -->
          <!-- Article -->
          <div class="col-lg-4">
            <div class="blog-article">
              <!-- Image -->
              <div class="blog-article__image">
                <a href="post.html">
                  <img
                    alt="Image"
                    data-sizes="auto"
                    data-srcset="assets/images/default/article_4.jpg 1560w,
                    assets/images/default/article_4.jpg 3120w"
                    src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                    class="lazyload" />
                </a>
              </div>
              <!-- End image -->
              <!-- Meta -->
              <ul class="blog-article__meta">
                <li><a href="#">Inspiration</a></li>
                <li>Dec 24, 2022</li>
                <li>By Admin</li>
              </ul>
              <!-- End meta -->
              <!-- Title -->
              <h5 class="blog-article__title">
                <a href="post.html">Ready for the winter! Discover the new collections of Durotan</a>
              </h5>
              <!-- End Title -->
            </div>
          </div>
          <!-- End article -->
          <!-- Article -->
          <div class="col-lg-4">
            <div class="blog-article">
              <!-- Image -->
              <div class="blog-article__image">
                <a href="post.html">
                  <img
                    alt="Image"
                    data-sizes="auto"
                    data-srcset="assets/images/default/article_5.jpg 1560w,
                    assets/images/default/article_5.jpg 3120w"
                    src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                    class="lazyload" />
                </a>
              </div>
              <!-- End image -->
              <!-- Meta -->
              <ul class="blog-article__meta">
                <li><a href="#">Inspiration</a></li>
                <li>Dec 24, 2022</li>
                <li>By Admin</li>
              </ul>
              <!-- End meta -->
              <!-- Title -->
              <h5 class="blog-article__title">
                <a href="post.html">Simple Men #5: Top items essential for any gentleman</a>
              </h5>
              <!-- End Title -->
            </div>
          </div>
          <!-- End article -->
          <!-- Article -->
          <div class="col-lg-4">
            <div class="blog-article">
              <!-- Image -->
              <div class="blog-article__image">
                <a href="post.html">
                  <img
                    alt="Image"
                    data-sizes="auto"
                    data-srcset="assets/images/default/article_6.jpg 1560w,
                    assets/images/default/article_6.jpg 3120w"
                    src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                    class="lazyload" />
                </a>
              </div>
              <!-- End image -->
              <!-- Meta -->
              <ul class="blog-article__meta">
                <li><a href="#">Inspiration</a></li>
                <li>Dec 24, 2022</li>
                <li>By Admin</li>
              </ul>
              <!-- End meta -->
              <!-- Title -->
              <h5 class="blog-article__title">
                <a href="post.html">Irresistible Attraction</a>
              </h5>
              <!-- End Title -->
            </div>
          </div>
          <!-- End article -->
          <!-- Article -->
          <div class="col-lg-4">
            <div class="blog-article">
              <!-- Image -->
              <div class="blog-article__image">
                <a href="post.html">
                  <img
                    alt="Image"
                    data-sizes="auto"
                    data-srcset="assets/images/default/article_7.jpg 1560w,
                    assets/images/default/article_7.jpg 3120w"
                    src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                    class="lazyload" />
                </a>
              </div>
              <!-- End image -->
              <!-- Meta -->
              <ul class="blog-article__meta">
                <li><a href="#">Inspiration</a></li>
                <li>Dec 24, 2022</li>
                <li>By Admin</li>
              </ul>
              <!-- End meta -->
              <!-- Title -->
              <h5 class="blog-article__title">
                <a href="post.html">Downtown</a>
              </h5>
              <!-- End Title -->
            </div>
          </div>
          <!-- End article -->
          <!-- Article -->
          <div class="col-lg-4">
            <div class="blog-article">
              <!-- Image -->
              <div class="blog-article__image">
                <a href="post.html">
                  <img
                    alt="Image"
                    data-sizes="auto"
                    data-srcset="assets/images/default/article_8.jpg 1560w,
                    assets/images/default/article_8.jpg 3120w"
                    src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                    class="lazyload" />
                </a>
              </div>
              <!-- End image -->
              <!-- Meta -->
              <ul class="blog-article__meta">
                <li><a href="#">Inspiration</a></li>
                <li>Dec 24, 2022</li>
                <li>By Admin</li>
              </ul>
              <!-- End meta -->
              <!-- Title -->
              <h5 class="blog-article__title">
                <a href="post.html">For long day activities</a>
              </h5>
              <!-- End Title -->
            </div>
          </div>
          <!-- End article -->
          <!-- Article -->
          <div class="col-lg-4">
            <div class="blog-article">
              <!-- Image -->
              <div class="blog-article__image">
                <a href="post.html">
                  <img
                    alt="Image"
                    data-sizes="auto"
                    data-srcset="assets/images/default/article_9.jpg 1560w,
                    assets/images/default/article_9.jpg 3120w"
                    src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                    class="lazyload" />
                </a>
              </div>
              <!-- End image -->
              <!-- Meta -->
              <ul class="blog-article__meta">
                <li><a href="#">Inspiration</a></li>
                <li>Dec 24, 2022</li>
                <li>By Admin</li>
              </ul>
              <!-- End meta -->
              <!-- Title -->
              <h5 class="blog-article__title">
                <a href="post.html">Jurgen Kloop - Fashion Designer leading the minimalist trend</a>
              </h5>
              <!-- End Title -->
            </div>
          </div>
          <!-- End article -->
        </div>
        <!-- End Blog articles -->
        <!-- Load more -->
        <div class="blog__load-more">
          <a href="#" class="sixth-button">Load more (6)</a>
        </div>
        <!-- End load more -->
      </div>
      <!-- End container -->
    </div>
    <!-- End container -->

            <!-- Canvas cart -->
        <div class="canvas-cart js-canvas-cart">
            <div class="canvas-cart__overlay js-close-canvas-cart"></div>
            <!-- Content -->
            <div class="canvas-cart__content">
                <!-- D-flex -->
                <div class="canvas-cart__d-flex">
                    <!-- Top and products -->
                    <div class="canvas-cart__top-and-products">
                        <!-- Heading -->
                        <div class="canvas-cart__heading d-flex align-items-center">
                            <!-- H3 -->
                            <h3 class="canvas-cart__h3">Cart (3)</h3>
                            <!-- End h3 -->
                            <!-- Close -->
                            <div class="canvas-cart__close"><a href="#" class="js-close-canvas-cart"><i
                                        class="lnil lnil-close"></i></a></div>
                            <!-- End close -->
                        </div>
                        <!-- End heading -->
                        <!-- Cart items -->
                        <ul class="header-cart__items">
                            <!-- Use Vue.js v-if to conditionally render the cart items -->
                            <div v-if="ds_cart">
                                <ul class="cart-items">
                                    <li class="cart-item d-flex" v-for="(value, key) in ds_cart" :key="key">
                                        <!-- Item image -->
                                        <p class="cart-item__image">
                                            <a>
                                                <img alt="Image" data-sizes="auto"
                                                    :data-srcset="value.hinh_anh + ' 400w, ' + value.hinh_anh + ' 800w'"
                                                    :src="value.hinh_anh" class="lazyload" />
                                            </a>
                                        </p>
                                        <!-- End item image -->
                                        <!-- Item details -->
                                        <p class="cart-item__details">
                                            <a class="cart-item__title">
                                                @{{ value.ten_san_pham }}
                                            </a>
                                            <span class="cart-item__price">@{{ value.tong_so_luong }} <i>x</i>
                                                @{{ formatCurrency(value.gia_ban) }}</span>
                                        </p>
                                        <!-- End item details -->
                                        <!-- Item quantity -->
                                        <div class="cart-item__quantity">
                                            <div class="cart-product__quantity-field">
                                                <div class="quantity-field__minus js-quantity-down">
                                                    <a href="javascript:void(0)"
                                                        v-on:click="tru_so_luong(value.ma_san_pham)">-</a>
                                                </div>
                                                <input type="text" :value="value.tong_so_luong"
                                                    class="quantity-field__input js-quantity-field" readonly />
                                                <div class="quantity-field__plus js-quantity-up">
                                                    <a href="javascript:void(0)"
                                                        v-on:click="addToCart(value.ma_san_pham)">+</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End item quantity -->
                                        <!-- Item delete -->
                                        <p class="cart-item__delete">
                                            <a href="javascript:void(0)"><i class="lnil lnil-close"  v-on:click="xoa_san_pham_gio_hang(value.ma_san_pham)"></i></a>
                                        </p>
                                        <!-- End item delete -->
                                    </li>
                                </ul>
                            </div>
                            <div v-else>
                                <p>Giỏ hàng trống.</p>
                            </div>

                        </ul>
                        <!-- End cart items -->
                    </div>
                    <!-- End top and products -->
                    <!-- Bottom -->
                    <div class="canvas-cart__bottom">
                        <!-- Subtotal -->
                        <div class="header-cart__subtotal d-flex">
                            <!-- Title -->
                            <div class="subtotal__title">Subtotal</div>
                            <!-- End title -->
                            <!-- Value -->
                            <div class="subtotal__value" v-if="tong_tien > 0">@{{ formatCurrency(tong_tien) }}</div>
                            <div class="subtotal__value" v-else>0₫</div>
                            <!-- End value -->
                        </div>
                        <!-- End subtotal -->
                        <!-- Header cart action -->
                        <div class="header-cart__action">
                            <a href="/gio-hang" class="header-cart__button">View cart</a>
                            <a href="/thanh-toan" class="header-cart__button">Checkout</a>
                        </div>
                        <!-- End Header cart action -->
                    </div>
                    <!-- End bottom -->
                </div>
                <!-- End d-flex -->
            </div>
            <!-- End content -->
        </div>
        <!-- End canvas cart -->
</div>
<!-- End Blog page -->
@endsection
@section('js')
<script>
    new Vue({
        el: "#app",
        data: {
            ds_cart: [],
            tong_tien: 0,
        },
        created() {
            this.loadCart();
        },
        methods: {
            addToCart(id) {
                axios
                    .post('/client/them-so-luong/' + id)
                    .then((res) => {
                        if (res.data.status) {
                            toastr.success(res.data.message);
                            this.loadCart();
                        } else {
                            toastr.error('Có lỗi không mong muốn!');
                        }
                    })
            },

            loadCart() {
                axios
                    .get('/client/hien-thi-ds-gio-hang')
                    .then((res) => {
                        this.ds_cart = res.data.gio_hang;
                        this.tong_tien = res.data.tong_tien_tat_ca
                        console.log(this.ds_cart);
                    });
            },
            formatCurrency(value) {
                const formatter = new Intl.NumberFormat('vi-VN', {
                    style: 'currency',
                    currency: 'VND',
                });
                return formatter.format(value);
            },
            tru_so_luong(id) {
                axios
                    .post('/client/tru-so-luong/' + id)
                    .then((res) => {
                        if (res.data.status) {
                            toastr.success(res.data.message);
                            this.loadCart();
                        } else {
                            toastr.error('Có lỗi không mong muốn!');
                        }
                    });
            },
            xoa_san_pham_gio_hang(id) {
                axios
                    .post('/client/xoa-san-pham-gio-hang/' + id)
                    .then((res) => {
                        if (res.data.status) {
                            toastr.success(res.data.message);
                            this.loadCart();
                        } else {
                            toastr.error('Có lỗi không mong muốn!');
                        }
                    });
            },
        }
    });
</script>
@endsection
