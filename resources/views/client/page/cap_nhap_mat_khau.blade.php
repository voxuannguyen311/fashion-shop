@extends('client.share.master')

@section('noi_dung')
<div class="container p-5" id="app">
    <div class="form-group required">
        <input v-model="cap_nhat_mat_khau.password" type="password" required="required" placeholder="Password"
            class="form-group__input" autocomplete="current-password" name="password">
        <div v-if="errors.password" class="alert alert-danger">
            @{{ errors.password[0] }}</div>
    </div>

    <div class="form-group required">
        <input v-model="cap_nhat_mat_khau.re_password" type="password" required="required" placeholder="Nhập lại Password"
            class="form-group__input" autocomplete="current-password">
        <div v-if="errors.re_password" class="alert alert-danger">@{{ errors.re_password[0] }}</div>
    </div>
    <!-- Action -->
    <div class="login__action" style="text-align: right;">
        <input v-on:click="action_cap_nhat_mat_khau()" class="second-button" type="submit" value="Đổi Mật Khẩu">
    </div>
    <!-- End action -->




    <!-- Canvas cart -->
    <div class="canvas-cart js-canvas-cart">
        <div class="canvas-cart__overlay js-close-canvas-cart"></div>
        <!-- Content -->
        <div class="canvas-cart__content">
            <!-- D-flex -->
            <div class="canvas-cart__d-flex">
                <!-- Top and products -->
                <div class="canvas-cart__top-and-products">
                    <!-- Heading -->
                    <div class="canvas-cart__heading d-flex align-items-center">
                        <!-- H3 -->
                        <h3 class="canvas-cart__h3">Cart (3)</h3>
                        <!-- End h3 -->
                        <!-- Close -->
                        <div class="canvas-cart__close"><a href="#" class="js-close-canvas-cart"><i
                                    class="lnil lnil-close"></i></a></div>
                        <!-- End close -->
                    </div>
                    <!-- End heading -->
                    <!-- Cart items -->
                    <ul class="header-cart__items">
                        <!-- Use Vue.js v-if to conditionally render the cart items -->
                        <div v-if="ds_cart">
                            <ul class="cart-items">
                                <li class="cart-item d-flex" v-for="(value, key) in ds_cart" :key="key">
                                    <!-- Item image -->
                                    <p class="cart-item__image">
                                        <a>
                                            <img alt="Image" data-sizes="auto"
                                                :data-srcset="value.hinh_anh + ' 400w, ' + value.hinh_anh + ' 800w'"
                                                :src="value.hinh_anh" class="lazyload" />
                                        </a>
                                    </p>
                                    <!-- End item image -->
                                    <!-- Item details -->
                                    <p class="cart-item__details">
                                        <a class="cart-item__title">
                                            @{{ value.ten_san_pham }}
                                        </a>
                                        <span class="cart-item__price">@{{ value.tong_so_luong }} <i>x</i>
                                            @{{ formatCurrency(value.gia_ban) }}</span>
                                    </p>
                                    <!-- End item details -->
                                    <!-- Item quantity -->
                                    <div class="cart-item__quantity">
                                        <div class="cart-product__quantity-field">
                                            <div class="quantity-field__minus js-quantity-down">
                                                <a href="javascript:void(0)" v-on:click="tru_so_luong(value.product_id)">-</a>
                                            </div>
                                            <input type="text" :value="value.tong_so_luong"
                                                class="quantity-field__input js-quantity-field"  readonly/>
                                            <div class="quantity-field__plus js-quantity-up">
                                                <a href="javascript:void(0)" v-on:click="addToCart(value.product_id)">+</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End item quantity -->
                                    <!-- Item delete -->
                                    <p class="cart-item__delete">
                                        <a  href="#"><i class="lnil lnil-close"></i></a>
                                    </p>
                                    <!-- End item delete -->
                                </li>
                            </ul>
                        </div>
                        <div v-else>
                            <p>Giỏ hàng trống.</p>
                        </div>



                    </ul>
                    <!-- End cart items -->
                </div>
                <!-- End top and products -->
                <!-- Bottom -->
                <div class="canvas-cart__bottom">
                    <!-- Subtotal -->
                    <div class="header-cart__subtotal d-flex">
                        <!-- Title -->
                        <div class="subtotal__title">Subtotal</div>
                        <!-- End title -->
                        <!-- Value -->
                        <div class="subtotal__value" v-if="tong_tien > 0">@{{ formatCurrency(tong_tien) }}</div>
                        <div class="subtotal__value" v-else>0₫</div>
                        <!-- End value -->
                    </div>
                    <!-- End subtotal -->
                    <!-- Header cart action -->
                    <div class="header-cart__action">
                        <a href="checkout.html" class="header-cart__button">Checkout</a>
                        <a href="/client/view-cart" class="header-cart__button">View cart</a>
                    </div>
                    <!-- End Header cart action -->
                </div>
                <!-- End bottom -->
            </div>
            <!-- End d-flex -->
        </div>
        <!-- End content -->
    </div>
    <!-- End canvas cart -->
</div>
@endsection
@section('js')
    <script>
        new Vue ({
            el: "#app",
            data: {
                cap_nhat_mat_khau: {},
                errors: {},
                ds_cart: [],
                tong_tien: 0,
            },
            created() {
                this.loadCart();
            },
            methods: {
                action_cap_nhat_mat_khau(){
                    axios
                        .post('/client/cap-nhap-mat-khau', this.cap_nhat_mat_khau)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.cap_nhat_mat_khau = {};
                            } else {
                                toastr.error('Hình như có vấn đề về đổi mật khẩu');
                            }
                        })
                        .catch((error) => {
                            if (error.response && error.response.data && error.response.data.errors) {
                                this.errors = error.response.data.errors;
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        })
                },

                loadCart() {
                    axios
                        .get('/client/show-cart')
                        .then((res) => {
                            this.ds_cart = res.data.cart;
                            this.tong_tien = res.data.tong_tien
                            // console.log(this.tong_tien);
                        });
                },
                formatCurrency(value) {
                    const formatter = new Intl.NumberFormat('vi-VN', {
                        style: 'currency',
                        currency: 'VND',
                    });
                    return formatter.format(value);
                },
                tru_so_luong(id){
                    axios
                        .post('/client/tru-so-luong/' + id)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadCart();
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        });
                },
                addToCart(id) {
                    axios
                        .post('/client/add-to-cart/' + id)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadCart();
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        })
                },
            },

        })
    </script>
@endsection
