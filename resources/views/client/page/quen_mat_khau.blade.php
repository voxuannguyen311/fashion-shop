@extends('client.share.master')
@section('noi_dung')
    <div class="container p-5" id="app">
        <div class="col-md-12">
            <input v-model="doi_mat_khau.hash_reset" name="hash_reset" type="hidden">
        </div>
        <div class="form-group required">
            <input v-model="doi_mat_khau.password" type="password" required="required" placeholder="Password"
                class="form-group__input" autocomplete="current-password" name="password">
            <div v-if="errors.password" class="alert alert-danger">
                @{{ errors.password[0] }}</div>
        </div>

        <div class="form-group required">
            <input v-model="doi_mat_khau.re_password" type="password" required="required" placeholder="Nhập lại Password"
                class="form-group__input" autocomplete="current-password">
            <div v-if="errors.re_password" class="alert alert-danger">@{{ errors.re_password[0] }}</div>
        </div>
        <!-- Action -->
        <div class="login__action" style="text-align: right;">
            <input v-on:click="action_doi_mat_khau()" class="second-button" type="submit" value="Đổi Mật Khẩu">
        </div>
        <!-- End action -->
    </div>
@endsection
@section('js')
    <script>
        new Vue({
            el: "#app",
            data: {
                doi_mat_khau: {
                    hash_reset: "{{ $hash }}"
                },
                errors: {}, // Thêm thuộc tính errors vào data
            },
            methods: {
                action_doi_mat_khau() {
                    axios
                        .post('/update-password', this.doi_mat_khau)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.doi_mat_khau = {};
                                window.location.href = "/login_register";
                            } else {
                                toastr.error('Hình như có vấn đề về đổi mật khẩu');
                            }
                        })
                        .catch((error) => {
                            if (error.response && error.response.data && error.response.data.errors) {
                                this.errors = error.response.data.errors;
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        })
                }
            },
        })
    </script>
@endsection
