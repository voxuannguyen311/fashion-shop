@extends('client.share.master')
@section('noi_dung')
    <!-- Container -->
    <div class="container" id="app">
        <!-- Categories and search -->
        <div class="blog__categories-and-search">
            <!-- Categories -->
            <ul class="blog__categories">
                <li><a href="#" class="active">All</a></li>
                <li><a href="#">Inspiration</a></li>
                <li><a href="#">Lookbook</a></li>
                <li><a href="#">Tips &amp; tricks</a></li>
                <li><a href="#">News</a></li>
                <li><a href="#">Others</a></li>
            </ul>
            <!-- End categories -->
            <!-- Search -->
            <div class="blog__search">
                <form>
                    <input type="text" class="blog-search__input" placeholder="Search in blog" />
                    <button type="submit" class="blog-search__button"><i class="lnil lnil-search-alt"></i></button>
                </form>
            </div>
            <!-- End search -->
        </div>
        <!-- End Categories and search -->
        <!-- Blog articles -->
        <div class="blog__articles row">
            @foreach ($BaiViet as $key => $value)
                <!-- Article -->
                <div class="col-lg-4">
                    <div class="blog-article">
                        <!-- Image -->
                        <div class="blog-article__image">
                            <a href="/chi-tiet-bai-viet/{{ $value->id }}">
                                <img alt="Image" data-sizes="auto"
                                    data-srcset="{{ $value->hinh_anh }} 1560w,
                            {{ $value->hinh_anh }} 3120w"
                                    class="lazyload" />
                            </a>
                        </div>
                        <!-- End image -->
                        <!-- Meta -->
                        <ul class="blog-article__meta">
                            <li><a>{{ $value->created_at }}</a></li>
                            <li>By Admin</li>
                        </ul>
                        <!-- End meta -->
                        <!-- Title -->
                        <h5 class="blog-article__title">
                            <a href="/chi-tiet-bai-viet/{{ $value->id }}">{{ $value->tieu_de }}</a>
                        </h5>
                        <!-- End Title -->
                    </div>
                </div>
                <!-- End article -->
            @endforeach

        </div>
        <!-- End Blog articles -->
        <!-- Load more -->
        <div class="blog__load-more">
            <a href="#" class="sixth-button">Load more (6)</a>
        </div>
        <!-- End load more -->

        <!-- Canvas cart -->
        <div class="canvas-cart js-canvas-cart">
            <div class="canvas-cart__overlay js-close-canvas-cart"></div>
            <!-- Content -->
            <div class="canvas-cart__content">
                <!-- D-flex -->
                <div class="canvas-cart__d-flex">
                    <!-- Top and products -->
                    <div class="canvas-cart__top-and-products">
                        <!-- Heading -->
                        <div class="canvas-cart__heading d-flex align-items-center">
                            <!-- H3 -->
                            <h3 class="canvas-cart__h3">Cart (3)</h3>
                            <!-- End h3 -->
                            <!-- Close -->
                            <div class="canvas-cart__close"><a href="#" class="js-close-canvas-cart"><i
                                        class="lnil lnil-close"></i></a></div>
                            <!-- End close -->
                        </div>
                        <!-- End heading -->
                        <!-- Cart items -->
                        <ul class="header-cart__items">
                            <!-- Use Vue.js v-if to conditionally render the cart items -->
                            <div v-if="ds_cart">
                                <ul class="cart-items">
                                    <li class="cart-item d-flex" v-for="(value, key) in ds_cart" :key="key">
                                        <!-- Item image -->
                                        <p class="cart-item__image">
                                            <a>
                                                <img alt="Image" data-sizes="auto"
                                                    :data-srcset="value.hinh_anh + ' 400w, ' + value.hinh_anh + ' 800w'"
                                                    :src="value.hinh_anh" class="lazyload" />
                                            </a>
                                        </p>
                                        <!-- End item image -->
                                        <!-- Item details -->
                                        <p class="cart-item__details">
                                            <a class="cart-item__title">
                                                @{{ value.ten_san_pham }}
                                            </a>
                                            <span class="cart-item__price">@{{ value.tong_so_luong }} <i>x</i>
                                                @{{ formatCurrency(value.gia_ban) }}</span>
                                        </p>
                                        <!-- End item details -->
                                        <!-- Item quantity -->
                                        <div class="cart-item__quantity">
                                            <div class="cart-product__quantity-field">
                                                <div class="quantity-field__minus js-quantity-down">
                                                    <a href="javascript:void(0)"
                                                        v-on:click="tru_so_luong(value.ma_san_pham)">-</a>
                                                </div>
                                                <input type="text" :value="value.tong_so_luong"
                                                    class="quantity-field__input js-quantity-field" readonly />
                                                <div class="quantity-field__plus js-quantity-up">
                                                    <a href="javascript:void(0)"
                                                        v-on:click="addToCart(value.ma_san_pham)">+</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End item quantity -->
                                        <!-- Item delete -->
                                        <p class="cart-item__delete">
                                            <a href="javascript:void(0)"><i class="lnil lnil-close"  v-on:click="xoa_san_pham_gio_hang(value.ma_san_pham)"></i></a>
                                        </p>
                                        <!-- End item delete -->
                                    </li>
                                </ul>
                            </div>
                            <div v-else>
                                <p>Giỏ hàng trống.</p>
                            </div>

                        </ul>
                        <!-- End cart items -->
                    </div>
                    <!-- End top and products -->
                    <!-- Bottom -->
                    <div class="canvas-cart__bottom">
                        <!-- Subtotal -->
                        <div class="header-cart__subtotal d-flex">
                            <!-- Title -->
                            <div class="subtotal__title">Subtotal</div>
                            <!-- End title -->
                            <!-- Value -->
                            <div class="subtotal__value" v-if="tong_tien > 0">@{{ formatCurrency(tong_tien) }}</div>
                            <div class="subtotal__value" v-else>0₫</div>
                            <!-- End value -->
                        </div>
                        <!-- End subtotal -->
                        <!-- Header cart action -->
                        <div class="header-cart__action">
                            <a href="/gio-hang" class="header-cart__button">View cart</a>
                            <a href="/thanh-toan" class="header-cart__button">Checkout</a>
                        </div>
                        <!-- End Header cart action -->
                    </div>
                    <!-- End bottom -->
                </div>
                <!-- End d-flex -->
            </div>
            <!-- End content -->
        </div>
        <!-- End canvas cart -->
    </div>
    <!-- End container -->
    </div>
@endsection
@section('js')
    <script>
        new Vue({
            el: "#app",
            data: {
                ds_cart: [],
                tong_tien: 0,
            },
            created() {
                this.loadCart();
            },
            methods: {
                addToCart(id) {
                    axios
                        .post('/client/them-so-luong/' + id)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadCart();
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        })
                },

                loadCart() {
                    axios
                        .get('/client/hien-thi-ds-gio-hang')
                        .then((res) => {
                            this.ds_cart = res.data.gio_hang;
                            this.tong_tien = res.data.tong_tien_tat_ca
                            console.log(this.ds_cart);
                        });
                },
                formatCurrency(value) {
                    const formatter = new Intl.NumberFormat('vi-VN', {
                        style: 'currency',
                        currency: 'VND',
                    });
                    return formatter.format(value);
                },
                tru_so_luong(id) {
                    axios
                        .post('/client/tru-so-luong/' + id)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadCart();
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        });
                },
                xoa_san_pham_gio_hang(id) {
                    axios
                        .post('/client/xoa-san-pham-gio-hang/' + id)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadCart();
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        });
                },
            }
        });
    </script>
@endsection
