@extends('client.share.master')
@section('noi_dung')
    <!-- Login -->
    <div id="app" class="login">
        <!-- Container -->
        <div class="container container--type-2">
            <!-- Container -->
            <div class="container">
                <!-- Login container -->
                <div class="login__container">
                    <!-- Login d-flex -->
                    <div class="login__d-flex d-flex">
                        <!-- Login left -->
                        <div class="login__left">
                            <!-- Login box -->
                            <div class="login__box active js-login-in">
                                <!-- Login heading -->
                                <h4 class="login__h4">Existing customers</h4>
                                <!-- End login heading -->
                                <!-- Login description -->
                                <p class="login__description">Sign in to your account below:</p>
                                <!-- End login description -->
                                <!-- Form -->

                                <!-- Form group -->
                                <div class="form-group required">
                                    <input v-model="user_login.email" type="email" required="required" placeholder="Email"
                                        class="form-group__input" autocomplete="email" name="email">
                                    <div v-if="isLogin && errors.email" class="alert alert-danger">@{{ errors.email[0] }}
                                    </div>
                                </div>

                                <div class="form-group required">
                                    <input v-model="user_login.password"type="password" required="required"
                                        placeholder="Password" class="form-group__input" autocomplete="current-password"
                                        name="password">
                                    <div v-if="isLogin && errors.password" class="alert alert-danger">@{{ errors.password[0] }}
                                    </div>
                                </div>

                                <!-- End form group -->
                                <!-- Forgot password -->
                                <div class="login__forgot-password"><a href="#" data-bs-toggle="modal"
                                        data-bs-target="#exampleModal" class="text-secondary">forgot password?</a></div>
                                <!-- End forgot password -->
                                <!-- Action -->
                                <div class="login__action"><input v-on:click="action_dang_nhap()" class="second-button"
                                        type="submit" value="Sign in">
                                </div>
                                <!-- End action -->
                                <!-- End form -->


                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h1 class="modal-title fs-5" id="exampleModalLabel">Nhập Vào Email Để Khôi
                                                    Phục Mật Khẩu</h1>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">

                                                <div class="row justify-content-center">

                                                    <div class="form-group required">
                                                        <input v-model="quen_mk.email" type="email" required="required"
                                                            placeholder="Email" class="form-group__input"
                                                            autocomplete="email" name="email">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-bs-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary" class="btn btn-secondary"
                                                    data-bs-dismiss="modal" v-on:click="action_quen_mat_khau()">Gửi</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- End login left -->



                        <!-- Đăngh Ký bên phải  -->
                        <div class="login__right">
                            <!-- Login box -->
                            <div class="login__box active">
                                <!-- Login heading -->
                                <h4 class="login__h4">New customers</h4>
                                <!-- End login heading -->
                                <!-- Login description -->
                                <p class="login__description">Create an account below:</p>
                                <!-- End login description -->
                                <div class="form-group required">
                                    <input v-model="dang_ky.ho_va_ten" type="text" required="required"
                                        placeholder="Họ và tên" class="form-group__input">
                                    <div v-if="errors.ho_va_ten" class="alert alert-danger">@{{ errors.ho_va_ten[0] }}</div>
                                </div>

                                <div class="form-group required">
                                    <input v-model="dang_ky.email" type="email" required="required" placeholder="Email"
                                        class="form-group__input" autocomplete="email" name="email">
                                    <div v-if="isRegister && errors.email" class="alert alert-danger">@{{ errors.email[0] }}
                                    </div>
                                </div>

                                <div class="form-group required">
                                    <input v-model="dang_ky.password" type="password" required="required"
                                        placeholder="Password" class="form-group__input" autocomplete="current-password"
                                        name="password">
                                    <div v-if="isRegister && errors.password" class="alert alert-danger">
                                        @{{ errors.password[0] }}</div>
                                </div>

                                <div class="form-group required">
                                    <input v-model="dang_ky.re_password" type="password" required="required"
                                        placeholder="Nhập lại Password" class="form-group__input"
                                        autocomplete="current-password">
                                    <div v-if="errors.re_password" class="alert alert-danger">@{{ errors.re_password[0] }}</div>
                                </div>

                                <div class="form-group required">
                                    <input v-model="dang_ky.so_dien_thoai" type="tel" required="required"
                                        placeholder="Số điện thoại" class="form-group__input">
                                    <div v-if="errors.so_dien_thoai" class="alert alert-danger">@{{ errors.so_dien_thoai[0] }}
                                    </div>

                                </div>

                                <div class="form-group required">
                                    <input v-model="dang_ky.dia_chi" type="text" required="required"
                                        placeholder="Địa chỉ" class="form-group__input">
                                    <div v-if="errors.dia_chi" class="alert alert-danger">@{{ errors.dia_chi[0] }}</div>

                                </div>

                                <div class="form-group required">
                                    <input v-model="dang_ky.ngay_sinh" type="date" required="required"
                                        placeholder="Ngày sinh" class="form-group__input">
                                    <div v-if="errors.ngay_sinh" class="alert alert-danger">@{{ errors.ngay_sinh[0] }}</div>
                                </div>

                                <div class="form-group required">
                                    <select v-model="dang_ky.gioi_tinh" required="required" class="form-group__input">

                                        <option value="1">Nam</option>
                                        <option value="0">Nữ</option>
                                    </select>
                                    <div v-if="errors.gioi_tinh" class="alert alert-danger">@{{ errors.gioi_tinh[0] }}</div>
                                </div>
                                <!-- Action -->
                                <div class="login__action "><input v-on:click="create_dang_ky() " class="second-button "
                                        type="submit" value="Create an account"></div>
                                <!-- End action -->

                            </div>
                            <!-- End login box -->
                        </div>
                        <!-- End login right -->
                    </div>
                    <!-- End login d-flex -->
                </div>
                <!-- End login container -->
            </div>
            <!-- End container -->
        </div>
        <!-- End container -->
    </div>
    <!-- End login -->
@endsection
@section('js')
    <script>
        new Vue({
            el: "#app",
            data: {
                dang_ky: {},
                user_login: {},
                errors: {},
                //việc thêm các biến isRegister và isLogin vào phần data giúp quản lý trạng thái và điều khiển việc hiển thị các phần tử trong giao diện dựa trên trạng thái đó.
                isRegister: false, // Thêm isRegister vào phần data
                isLogin: false, // Thêm isLogin vào phần data
                quen_mk: {},
            },
            methods: {
                create_dang_ky() {
                    axios
                        .post('/register', this.dang_ky)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.dang_ky = {};
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        })
                        .catch((error) => {
                            if (error.response && error.response.data && error.response.data.errors) {
                                this.errors = error.response.data.errors;
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        })
                        // được thực thi sau khi yêu cầu đăng ký đã hoàn thành (bất kể thành công hay thất bại). Trong phần xử lý này:
                        .finally(() => {
                            this.isRegister = true;
                        });
                },

                action_dang_nhap() {
                    axios
                        .post('/login', this.user_login)
                        .then((res) => {
                            console.log(res.data);
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.user_login = {};
                                window.location.href = "/client/cap-nhap-thong-tin";
                            } else {
                                toastr.error(res.data.message);
                            }
                        })
                        .catch((error) => {
                            if (error.response && error.response.data && error.response.data.errors) {
                                this.errors = error.response.data.errors;
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        })
                        .finally(() => {
                            this.isLogin = true;
                        });
                },

                action_quen_mat_khau() {
                    axios
                        .post('/reset-password', this.quen_mk)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);

                            } else {
                                toastr.error(res.data.message);
                            }
                        })
                }
            }
        });
    </script>
@endsection
