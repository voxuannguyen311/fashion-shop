@extends('client.share.master')
@section('noi_dung')
    <!-- Post -->
    <div class="post" id="app">
        <!-- Post heading -->
        <div class="post__heading">
            <!-- Container -->
            <div class="container container--type-2">
                <!-- Post meta -->
                <ul class="post__meta">
                    <li>{{ $ChiTietBaiViet->created_at }}</li>
                </ul>
                <!-- End post meta -->
                <!-- Post title -->
                <h1 class="post__title">{{ $ChiTietBaiViet->tieu_de }}</h1>
                <!-- End post title -->
            </div>
            <!-- End container -->
        </div>
        <!-- End post heading -->
        <!-- Post image -->
        <div class="post__image">
            <!-- Container -->
            <div class="container container--type-2" style="width:60%; !important">
                <img alt="Image" data-sizes="auto"
                    data-srcset="
            {{ $ChiTietBaiViet->hinh_anh }} 400w,
                        {{ $ChiTietBaiViet->hinh_anh }} 800w"
                    src="{{ $ChiTietBaiViet->hinh_anh }}" class="lazyload" />

            </div>
            <!-- End container -->
        </div>
        <!-- End post image -->
        <!-- Post content -->
        <div class="post__content">
            <!-- Container -->
            <div class="container container--type-2">
                <!-- Post container -->
                <div class="post__container">
                    <p> {{ $ChiTietBaiViet->noi_dung }}</p>
                </div>
                <!-- End post container -->
            </div>
            <!-- End container -->
        </div>
        <!-- End post content -->
        <!-- Post tags and share -->
        <div class="post__tags-share">
            <!-- Container -->
            <div class="container container--type-2">
                <!-- Post container -->
                <div class="tags-share__container">
                    <!-- Tags and share -->
                    <div class="tags-share">
                        <!-- Tags -->
                        <ul class="post__tags">
                            <li><a href="#">shopify</a></li>
                            <li><a href="#">theme</a></li>
                            <li><a href="#">live editor</a></li>
                        </ul>
                        <!-- End tags -->
                        <!-- Share -->
                        <div class="post__share">
                            <!-- Title -->
                            <div class="share__title">Share on</div>
                            <!-- End title -->
                            <!-- Options -->
                            <ul class="share__options">
                                <li><a href="https://twitter.com/" target="_blank" class="share-option__twitter"><i
                                            class="lnir lnir-twitter-original"></i></a></li>
                                <li><a href="https://facebook.com/" target="_blank" class="share-option__facebook"><i
                                            class="lnir lnir-facebook-filled"></i></a></li>
                                <li><a href="https://instagram.com/" target="_blank" class="share-option__instagram"><i
                                            class="lnil lnil-Instagram"></i></a></li>
                            </ul>
                            <!-- End options -->
                        </div>
                        <!-- End share -->
                    </div>
                    <!-- End Tags and share -->
                </div>
                <!-- End post container -->
            </div>
            <!-- End container -->
        </div>
        <!-- End post tags and share -->

        <!-- Post comments -->
        <div class="post__comments">
            <!-- Container -->
            <div class="container container--type-2">
                <!-- Container -->
                <div class="post-comments__container">
                    <!-- Line 1 px -->
                    <hr />
                    <!-- End line 1 px -->
                    <!-- Title -->
                    <h4 class="post-comments__title">02 Comments</h4>
                    <!-- End title -->
                    <!-- Comment -->
                    <div class="post-comment">
                        <!-- Avatar -->
                        <div class="post-comment__avatar">
                            <img alt="Image" data-sizes="auto"
                                data-srcset="assets/images/default/avatar_1.jpg 1560w,
                    assets/images/default/avatar_1.jpg 3120w"
                                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                class="lazyload" />
                        </div>
                        <!-- End avatar -->
                        <!-- Details -->
                        <div class="post-comment__details">
                            <!-- Name and date -->
                            <div class="post-comment__name-and-date">
                                <!-- Name -->
                                <div class="post-comment__name">Andy Robertson</div>
                                <!-- End name -->
                                <!-- Date -->
                                <div class="post-comment__date">on 25 April, 2022</div>
                                <!-- End date -->
                            </div>
                            <!-- End name and date -->
                            <!-- Content -->
                            <div class="post-comment__content">Thanks to the precious advice of the store owner, I choose
                                this wonderful product. I absolutely love it! Additionally, my order was sent very quickly.
                                I'm a happy customer and I'll order again!</div>
                            <!-- End content -->
                        </div>
                        <!-- End details -->
                    </div>
                    <!-- End comment -->
                    <!-- Comment -->
                    <div class="post-comment">
                        <!-- Avatar -->
                        <div class="post-comment__avatar">
                            <img alt="Image" data-sizes="auto"
                                data-srcset="assets/images/default/avatar_2.jpg 1560w,
                    assets/images/default/avatar_2.jpg 3120w"
                                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                class="lazyload" />
                        </div>
                        <!-- End avatar -->
                        <!-- Details -->
                        <div class="post-comment__details">
                            <!-- Name and date -->
                            <div class="post-comment__name-and-date">
                                <!-- Name -->
                                <div class="post-comment__name">Alexander Arnold</div>
                                <!-- End name -->
                                <!-- Date -->
                                <div class="post-comment__date">on 25 April, 2022</div>
                                <!-- End date -->
                            </div>
                            <!-- End name and date -->
                            <!-- Content -->
                            <div class="post-comment__content">I love it & certainly that i’ll buy it once again. Perfection
                                experience!</div>
                            <!-- End content -->
                        </div>
                        <!-- End details -->
                    </div>
                    <!-- End comment -->
                </div>
                <!-- End container -->
            </div>
            <!-- End container -->
        </div>
        <!-- End post comments -->
        <!-- Post comments -->
        <div class="post__leave-comment">
            <!-- Container -->
            <div class="container container--type-2">
                <!-- Container -->
                <div class="leave-comment__container">
                    <!-- Title -->
                    <h4 class="leave-comment__title">Leave A Comment</h4>
                    <!-- End title -->
                    <!-- Form -->
                    <form class="comments__form">
                        <!-- Required fields -->
                        <div class="form__required-fields">Your email address will not be published. Required fields are
                            marked<span>*</span></div>
                        <!-- End required fields -->
                        <!-- Form group -->
                        <div class="form-group">
                            <textarea placeholder="Write your message here" class="form-group__textarea" rows="5"></textarea>
                        </div>
                        <!-- End form group -->
                        <!-- Row -->
                        <div class="row">
                            <div class="col-md-6">
                                <!-- Form group -->
                                <div class="form-group">
                                    <input type="text" name="name" class="form-group__input" placeholder="Full Name">
                                </div>
                                <!-- End form group -->
                            </div>
                            <div class="col-md-6">
                                <!-- Form group -->
                                <div class="form-group">
                                    <input type="email" name="email" class="form-group__input"
                                        placeholder="Your E-mail*">
                                </div>
                                <!-- End form group -->
                            </div>
                        </div>
                        <!-- End row -->
                        <!-- Action -->
                        <div class="form__action">
                            <button type="submit" class="second-button">Post comment</button>
                        </div>
                        <!-- End action -->
                    </form>
                    <!-- End form -->
                </div>
                <!-- End container -->
            </div>
            <!-- End Container -->
        </div>
        <!-- End post comments -->
       <!-- Canvas cart -->
       <div class="canvas-cart js-canvas-cart">
        <div class="canvas-cart__overlay js-close-canvas-cart"></div>
        <!-- Content -->
        <div class="canvas-cart__content">
            <!-- D-flex -->
            <div class="canvas-cart__d-flex">
                <!-- Top and products -->
                <div class="canvas-cart__top-and-products">
                    <!-- Heading -->
                    <div class="canvas-cart__heading d-flex align-items-center">
                        <!-- H3 -->
                        <h3 class="canvas-cart__h3">Cart (3)</h3>
                        <!-- End h3 -->
                        <!-- Close -->
                        <div class="canvas-cart__close"><a href="#" class="js-close-canvas-cart"><i
                                    class="lnil lnil-close"></i></a></div>
                        <!-- End close -->
                    </div>
                    <!-- End heading -->
                    <!-- Cart items -->
                    <ul class="header-cart__items">
                        <!-- Use Vue.js v-if to conditionally render the cart items -->
                        <div v-if="ds_cart">
                            <ul class="cart-items">
                                <li class="cart-item d-flex" v-for="(value, key) in ds_cart" :key="key">
                                    <!-- Item image -->
                                    <p class="cart-item__image">
                                        <a>
                                            <img alt="Image" data-sizes="auto"
                                                :data-srcset="value.hinh_anh + ' 400w, ' + value.hinh_anh + ' 800w'"
                                                :src="value.hinh_anh" class="lazyload" />
                                        </a>
                                    </p>
                                    <!-- End item image -->
                                    <!-- Item details -->
                                    <p class="cart-item__details">
                                        <a class="cart-item__title">
                                            @{{ value.ten_san_pham }}
                                        </a>
                                        <span class="cart-item__price">@{{ value.tong_so_luong }} <i>x</i>
                                            @{{ formatCurrency(value.gia_ban) }}</span>
                                    </p>
                                    <!-- End item details -->
                                    <!-- Item quantity -->
                                    <div class="cart-item__quantity">
                                        <div class="cart-product__quantity-field">
                                            <div class="quantity-field__minus js-quantity-down">
                                                <a href="javascript:void(0)"
                                                    v-on:click="tru_so_luong(value.ma_san_pham)">-</a>
                                            </div>
                                            <input type="text" :value="value.tong_so_luong"
                                                class="quantity-field__input js-quantity-field" readonly />
                                            <div class="quantity-field__plus js-quantity-up">
                                                <a href="javascript:void(0)"
                                                    v-on:click="addToCart(value.ma_san_pham)">+</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End item quantity -->
                                    <!-- Item delete -->
                                    <p class="cart-item__delete">
                                        <a href="javascript:void(0)"><i class="lnil lnil-close"  v-on:click="xoa_san_pham_gio_hang(value.ma_san_pham)"></i></a>
                                    </p>
                                    <!-- End item delete -->
                                </li>
                            </ul>
                        </div>
                        <div v-else>
                            <p>Giỏ hàng trống.</p>
                        </div>

                    </ul>
                    <!-- End cart items -->
                </div>
                <!-- End top and products -->
                <!-- Bottom -->
                <div class="canvas-cart__bottom">
                    <!-- Subtotal -->
                    <div class="header-cart__subtotal d-flex">
                        <!-- Title -->
                        <div class="subtotal__title">Subtotal</div>
                        <!-- End title -->
                        <!-- Value -->
                        <div class="subtotal__value" v-if="tong_tien > 0">@{{ formatCurrency(tong_tien) }}</div>
                        <div class="subtotal__value" v-else>0₫</div>
                        <!-- End value -->
                    </div>
                    <!-- End subtotal -->
                    <!-- Header cart action -->
                    <div class="header-cart__action">
                        <a href="/gio-hang" class="header-cart__button">View cart</a>
                        <a href="/thanh-toan" class="header-cart__button">Checkout</a>
                    </div>
                    <!-- End Header cart action -->
                </div>
                <!-- End bottom -->
            </div>
            <!-- End d-flex -->
        </div>
        <!-- End content -->
    </div>
    <!-- End canvas cart -->
    </div>
    <!-- End post -->
@endsection
@section('js')
<script>
    new Vue({
        el: "#app",
        data: {
            ds_cart: [],
            tong_tien: 0,
        },
        created() {
            this.loadCart();
        },
        methods: {
            addToCart(id) {
                axios
                    .post('/client/them-so-luong/' + id)
                    .then((res) => {
                        if (res.data.status) {
                            toastr.success(res.data.message);
                            this.loadCart();
                        } else {
                            toastr.error('Có lỗi không mong muốn!');
                        }
                    })
            },

            loadCart() {
                axios
                    .get('/client/hien-thi-ds-gio-hang')
                    .then((res) => {
                        this.ds_cart = res.data.gio_hang;
                        this.tong_tien = res.data.tong_tien_tat_ca
                        console.log(this.ds_cart);
                    });
            },
            formatCurrency(value) {
                const formatter = new Intl.NumberFormat('vi-VN', {
                    style: 'currency',
                    currency: 'VND',
                });
                return formatter.format(value);
            },
            tru_so_luong(id) {
                axios
                    .post('/client/tru-so-luong/' + id)
                    .then((res) => {
                        if (res.data.status) {
                            toastr.success(res.data.message);
                            this.loadCart();
                        } else {
                            toastr.error('Có lỗi không mong muốn!');
                        }
                    });
            },
            xoa_san_pham_gio_hang(id) {
                axios
                    .post('/client/xoa-san-pham-gio-hang/' + id)
                    .then((res) => {
                        if (res.data.status) {
                            toastr.success(res.data.message);
                            this.loadCart();
                        } else {
                            toastr.error('Có lỗi không mong muốn!');
                        }
                    });
            },
        }
    });
</script>
@endsection
