@extends('client.share.master')

@section('noi_dung')
    <div class="row pt-5">
        <div class="col text-center">
            <div class="alert d-block w-50 h-100 mx-auto" style="color: #666666" role="alert">
                Thông Tin Cá Nhân
            </div>
        </div>
    </div>
    <div class="row justify-content-center" id="app">
        <div class="col-md-4 pt-5">
            <!-- Hiển thị thông tin người đăng nhập -->
            <input type="hidden" name="id" v-model="ng_dang_nhap.id">
            <div class="form-group required">
                <input type="text" required="required" v-model="ng_dang_nhap.ho_va_ten" placeholder="Họ và tên"
                    class="form-group__input">
                <div v-if="errors.ho_va_ten" class="alert alert-danger">@{{ errors.ho_va_ten[0] }}</div>
            </div>
            <div class="form-group required">
                <input type="tel" required="required" v-model="ng_dang_nhap.so_dien_thoai" placeholder="Số điện thoại"
                    class="form-group__input">
                <div v-if="errors.so_dien_thoai" class="alert alert-danger">@{{ errors.so_dien_thoai[0] }}</div>
            </div>
            <div class="form-group required">
                <input type="text" required="required" v-model="ng_dang_nhap.dia_chi" placeholder="Địa chỉ"
                    class="form-group__input">
                <div v-if="errors.dia_chi" class="alert alert-danger">@{{ errors.dia_chi[0] }}</div>
            </div>
            <div class="form-group required">
                <input type="date" required="required" v-model="ng_dang_nhap.ngay_sinh" placeholder="Ngày sinh"
                    class="form-group__input">
                <div v-if="errors.ngay_sinh" class="alert alert-danger">@{{ errors.ngay_sinh[0] }}</div>
            </div>
            <div class="form-group required">
                <select required="required" v-model="ng_dang_nhap.gioi_tinh" class="form-group__input">
                    <option value="1">Nam</option>
                    <option value="0">Nữ</option>
                </select>
                <div v-if="errors.gioi_tinh" class="alert alert-danger">@{{ errors.gioi_tinh[0] }}</div>
            </div>
            <!-- Action -->
            <div class="login__action text-end">
                <input v-on:click="capNhaphongTin()" class="second-button" type="submit" value="Update account">
            </div>
            <!-- End action -->
        </div>




       <!-- Canvas cart -->
       <div class="canvas-cart js-canvas-cart">
        <div class="canvas-cart__overlay js-close-canvas-cart"></div>
        <!-- Content -->
        <div class="canvas-cart__content">
            <!-- D-flex -->
            <div class="canvas-cart__d-flex">
                <!-- Top and products -->
                <div class="canvas-cart__top-and-products">
                    <!-- Heading -->
                    <div class="canvas-cart__heading d-flex align-items-center">
                        <!-- H3 -->
                        <h3 class="canvas-cart__h3">Cart (3)</h3>
                        <!-- End h3 -->
                        <!-- Close -->
                        <div class="canvas-cart__close"><a href="#" class="js-close-canvas-cart"><i
                                    class="lnil lnil-close"></i></a></div>
                        <!-- End close -->
                    </div>
                    <!-- End heading -->
                    <!-- Cart items -->
                    <ul class="header-cart__items">
                        <!-- Use Vue.js v-if to conditionally render the cart items -->
                        <div v-if="ds_cart">
                            <ul class="cart-items">
                                <li class="cart-item d-flex" v-for="(value, key) in ds_cart" :key="key">
                                    <!-- Item image -->
                                    <p class="cart-item__image">
                                        <a>
                                            <img alt="Image" data-sizes="auto"
                                                :data-srcset="value.hinh_anh + ' 400w, ' + value.hinh_anh + ' 800w'"
                                                :src="value.hinh_anh" class="lazyload" />
                                        </a>
                                    </p>
                                    <!-- End item image -->
                                    <!-- Item details -->
                                    <p class="cart-item__details">
                                        <a class="cart-item__title">
                                            @{{ value.ten_san_pham }}
                                        </a>
                                        <span class="cart-item__price">@{{ value.tong_so_luong }} <i>x</i>
                                            @{{ formatCurrency(value.gia_ban) }}</span>
                                    </p>
                                    <!-- End item details -->
                                    <!-- Item quantity -->
                                    <div class="cart-item__quantity">
                                        <div class="cart-product__quantity-field">
                                            <div class="quantity-field__minus js-quantity-down">
                                                <a href="javascript:void(0)"
                                                    v-on:click="tru_so_luong(value.ma_san_pham)">-</a>
                                            </div>
                                            <input type="text" :value="value.tong_so_luong"
                                                class="quantity-field__input js-quantity-field" readonly />
                                            <div class="quantity-field__plus js-quantity-up">
                                                <a href="javascript:void(0)"
                                                    v-on:click="addToCart(value.ma_san_pham)">+</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End item quantity -->
                                    <!-- Item delete -->
                                    <p class="cart-item__delete">
                                        <a href="javascript:void(0)"><i class="lnil lnil-close"  v-on:click="xoa_san_pham_gio_hang(value.ma_san_pham)"></i></a>
                                    </p>
                                    <!-- End item delete -->
                                </li>
                            </ul>
                        </div>
                        <div v-else>
                            <p>Giỏ hàng trống.</p>
                        </div>

                    </ul>
                    <!-- End cart items -->
                </div>
                <!-- End top and products -->
                <!-- Bottom -->
                <div class="canvas-cart__bottom">
                    <!-- Subtotal -->
                    <div class="header-cart__subtotal d-flex">
                        <!-- Title -->
                        <div class="subtotal__title">Subtotal</div>
                        <!-- End title -->
                        <!-- Value -->
                        <div class="subtotal__value" v-if="tong_tien > 0">@{{ formatCurrency(tong_tien) }}</div>
                        <div class="subtotal__value" v-else>0₫</div>
                        <!-- End value -->
                    </div>
                    <!-- End subtotal -->
                    <!-- Header cart action -->
                    <div class="header-cart__action">
                        <a href="/gio-hang" class="header-cart__button">View cart</a>
                        <a href="/thanh-toan" class="header-cart__button">Checkout</a>
                    </div>
                    <!-- End Header cart action -->
                </div>
                <!-- End bottom -->
            </div>
            <!-- End d-flex -->
        </div>
        <!-- End content -->
    </div>
    <!-- End canvas cart -->
    </div>
@endsection

@section('js')
    <script>
        new Vue({
            el: "#app",
            data: {
                ng_dang_nhap: {},
                errors: {},
                ds_cart: [],
                tong_tien: 0,
            },
            created() {
                this.load_ng_dang_nhap();
                this.loadCart();
            },
            methods: {
                load_ng_dang_nhap() {
                    axios
                        .get('/client/thong-tin-nguoi-dang-nhap')
                        .then((res) => {
                            this.ng_dang_nhap = res.data.user;
                        })
                        .catch((error) => {
                            if (error.response && error.response.data && error.response.data.errors) {
                                this.errors = error.response.data.errors;
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        });
                },
                capNhaphongTin() {
                    axios
                        .post('/client/cap-nhap-thong-tin', this.ng_dang_nhap)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.load_ng_dang_nhap();
                            } else {
                                toastr.error(res.data.message);
                            }
                        })
                        .catch((error) => {
                            if (error.response && error.response.data && error.response.data.errors) {
                                this.errors = error.response.data.errors;
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        });
                },
                loadCart() {
                    axios
                        .get('/client/show-cart')
                        .then((res) => {
                            this.ds_cart = res.data.cart;
                            this.tong_tien = res.data.tong_tien
                            this.tong_san_pham_gio_hang = this.ds_cart.reduce((total, item) => total + item.tong_so_luong, 0);
                        });
                },
                formatCurrency(value) {
                    const formatter = new Intl.NumberFormat('vi-VN', {
                        style: 'currency',
                        currency: 'VND',
                    });
                    return formatter.format(value);
                },
                tru_so_luong(id){
                    axios
                        .post('/client/tru-so-luong/' + id)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadCart();
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        });
                },
                addToCart(id) {
                    axios
                        .post('/client/add-to-cart/' + id)
                        .then((res) => {
                            if (res.data.status) {
                                toastr.success(res.data.message);
                                this.loadCart();
                            } else {
                                toastr.error('Có lỗi không mong muốn!');
                            }
                        })
                },
            }
        });
    </script>
@endsection
