 <!-- Canvas cart -->
 <div class="canvas-cart js-canvas-cart">
    <div class="canvas-cart__overlay js-close-canvas-cart"></div>
    <!-- Content -->
    <div class="canvas-cart__content">
        <!-- D-flex -->
        <div class="canvas-cart__d-flex">
            <!-- Top and products -->
            <div class="canvas-cart__top-and-products">
                <!-- Heading -->
                <div class="canvas-cart__heading d-flex align-items-center">
                    <!-- H3 -->
                    <h3 class="canvas-cart__h3">Cart (3)</h3>
                    <!-- End h3 -->
                    <!-- Close -->
                    <div class="canvas-cart__close"><a href="#" class="js-close-canvas-cart"><i
                                class="lnil lnil-close"></i></a></div>
                    <!-- End close -->
                </div>
                <!-- End heading -->
                <!-- Cart items -->
                <ul class="header-cart__items">
                    @if ($cart != null)
                        <ul class="cart-items">
                            @foreach ($cart->sanphams as $key => $value)
                                <li class="cart-item d-flex">
                                    <!-- Item image -->
                                    <p class="cart-item__image">
                                        <a href="product.html">
                                            <img alt="Image" data-sizes="auto"
                                                data-srcset="assets_client/assets/products/1/10a.jpg 400w,
                                  assets_client/assets/products/1/10a.jpg 800w"
                                                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                                class="lazyload" />
                                        </a>
                                    </p>
                                    <!-- End item image -->
                                    <!-- Item details -->
                                    <p class="cart-item__details">
                                        <a href="product.html"
                                            class="cart-item__title">{{ $value['thong_tin_san_pham']->ten_san_pham }}</a>
                                        <span class="cart-ietm__price">{{ $value['so_luong'] }} <i>x</i>
                                            ${{ $value['gia_ban'] }}</span>
                                    </p>
                                    <!-- End item details -->
                                    <!-- Item quantity -->
                                    <div class="cart-item__quantity">
                                        <div class="cart-product__quantity-field">
                                            <div class="quantity-field__minus js-quantity-down"><a
                                                    href="#">-</a></div>
                                            <input type="text" value="{{ $value['so_luong'] }}"
                                                class="quantity-field__input js-quantity-field" />
                                            <div class="quantity-field__plus js-quantity-up"><a
                                                    href="#">+</a></div>
                                        </div>
                                    </div>
                                    <!-- End item quantity -->
                                    <!-- Item delete -->
                                    <p class="cart-item__delete">
                                        <a href="#"><i class="lnil lnil-close"></i></a>
                                    </p>
                                    <!-- End item delete -->
                                </li>
                            @endforeach
                        </ul>
                    @else
                        <p>Giỏ hàng trống.</p>
                    @endif


                </ul>
                <!-- End cart items -->
            </div>
            <!-- End top and products -->
            <!-- Bottom -->
            <div class="canvas-cart__bottom">
                <!-- Subtotal -->
                <div class="header-cart__subtotal d-flex">
                    <!-- Title -->
                    <div class="subtotal__title">Subtotal</div>
                    <!-- End title -->
                    <!-- Value -->
                    @if (isset($cart) && $cart && count($cart->sanphams) > 0)
                    <div class="subtotal__value">${{ $cart->tong_tien }}</div>
                    @else
                        <div class="subtotal__value">$0</div>
                    @endif
                <!-- End value -->
                    <!-- End value -->
                </div>
                <!-- End subtotal -->
                <!-- Header cart action -->
                <div class="header-cart__action">
                    <a href="checkout.html" class="header-cart__button">Checkout</a>
                    <a href="cart.html" class="header-cart__button">View cart</a>
                </div>
                <!-- End Header cart action -->
            </div>
            <!-- End bottom -->
        </div>
        <!-- End d-flex -->
    </div>
    <!-- End content -->
</div>
<!-- End canvas cart -->
