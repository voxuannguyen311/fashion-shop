
  <!-- Footer -->
  <footer class="dark-footer">
    <!-- Container -->
    <div class="container container--type-2">
        <!-- Footer top -->
        <div class="dark-footer__top">
            <!-- Row -->
            <div class="row">
                <div class="col-lg-4">
                    <!-- About -->
                    <div class="dark-footer__about">
                        <!-- Title -->
                        <h4 class="dark-footer__title">about durotan</h4>
                        <!-- End title -->
                        <!-- Description -->
                        <div class="dark-footer__description">
                            The inspiration got from natural, color pastel and activities the daily.
                        </div>
                        <!-- End description -->
                    </div>
                    <!-- End about -->
                </div>
                <div class="col-lg-4">
                    <!-- Social -->
                    <div class="dark-footer__social">
                        <!-- Title -->
                        <h4 class="dark-footer__title">Our social</h4>
                        <!-- End title -->
                        <!-- Description -->
                        <div class="dark-footer__description">
                            <a href="#" class="dark-footer__social-item"><i
                                    class="lnil lnil-twitter"></i></a>
                            <a href="#" class="dark-footer__social-item"><i
                                    class="lnil lnil-facebook"></i></a>
                            <a href="#" class="dark-footer__social-item"><i
                                    class="lnil lnil-Instagram"></i></a>
                        </div>
                        <!-- End description -->
                    </div>
                    <!-- End social -->
                </div>
                <div class="col-lg-4">
                    <!-- Newsletter -->
                    <div class="dark-footer__newsletter">
                        <!-- Title -->
                        <h4 class="dark-footer__title">Newsletter</h4>
                        <!-- End title -->
                        <form class="dark-footer__newsletter-form">
                            <!-- Footer newsletter input -->
                            <input type="email" class="dark-footer__newsleter-input"
                                placeholder="Email Address">
                            <!-- End footer newsletter input -->
                            <!-- Footer newsletter button -->
                            <button type="submit" class="dark-footer__newsletter-button">Subscribe</button>
                            <!-- End footer newsletter button -->
                        </form>
                    </div>
                    <!-- End newsletter -->
                </div>
            </div>
            <!-- End row -->
        </div>
        <!-- End footer top -->
        <!-- Footer menu -->
        <ul class="dark-footer__menu">
            <li><a href="#">About</a></li>
            <li><a href="#">Blog </a></li>
            <li><a href="#">Contact</a></li>
            <li><a href="#">Policy</a></li>
            <li><a href="#">Shipping</a></li>
            <li><a href="#">Help center</a></li>
        </ul>
        <!-- End footer menu -->
        <!-- Copyright -->
        <div class="dark-footer__copyright">
            © 2022 <span>durotan.</span> all rights reserved
        </div>
        <!-- End copyright -->
    </div>
    <!-- End container -->
</footer>
<!-- End footer -->
<!-- Search popup -->
<div class="search-popup js-search-popup">
    <!-- Search close -->
    <div class="search-popup__close">
        <a href="#" class="js-close-search-popup"><i class="lnil lnil-close"></i></a>
    </div>
    <!-- End search close -->
    <!-- Container -->
    <div class="container container--type-2">
        <!-- Search title -->
        <h5 class="search-popup__title">Search</h5>
        <!-- End search title -->
        <!-- Search categories -->
        <ul class="search-popup__categories">
            <li><a href="#" class="active">All</a></li>
            <li><a href="#">Clothings</a></li>
            <li><a href="#">Shoes</a></li>
            <li><a href="#">Bags</a></li>
            <li><a href="#">Accessories</a></li>
        </ul>
        <!-- End search categories -->
        <!-- Search form -->
        <form class="search-popup__form">
            <!-- Search input -->
            <input type="text" class="search-popup__input" placeholder="Search here..." />
            <!-- End search input -->
        </form>
        <!-- End search form -->
        <!-- Search results -->
        <div class="search-popups__results">
            <!-- Results heading -->
            <h6 class="search-popup__results-heading">Search results</h6>
            <!-- End results heading -->
            <!-- Results -->
            <div class="search-popups__results-products d-flex">
                <!-- Product -->
                <div class="result-product">
                    <!-- Image -->
                    <div class="result-product__image">
                        <a href="product.html">
                            <img src="assets_client/assets/products/1/2_1-a.jpg" alt="Product image" />
                        </a>
                    </div>
                    <!-- End image -->
                    <!-- Product name -->
                    <div class="result-product__name"><a href="product.html">Double-breasted wool Tailored
                            coat</a></div>
                    <!-- End product name -->
                    <!-- Product price -->
                    <div class="result-product__price">$56.99</div>
                    <!-- End product price -->
                </div>
                <!-- End product -->
                <!-- Product -->
                <div class="result-product">
                    <!-- Image -->
                    <div class="result-product__image">
                        <a href="product.html">
                            <img src="assets_client/assets/products/1/2_2-a.jpg" alt="Product image" />
                        </a>
                    </div>
                    <!-- End image -->
                    <!-- Product name -->
                    <div class="result-product__name"><a href="product.html">Slim fit modal cotton shirt</a>
                    </div>
                    <!-- End product name -->
                    <!-- Product price -->
                    <div class="result-product__price">$59.99</div>
                    <!-- End product price -->
                </div>
                <!-- End product -->
                <!-- Product -->
                <div class="result-product">
                    <!-- Image -->
                    <div class="result-product__image">
                        <a href="product.html">
                            <img src="assets_client/assets/products/1/2_3-a.jpg" alt="Product image" />
                        </a>
                    </div>
                    <!-- End image -->
                    <!-- Product name -->
                    <div class="result-product__name"><a href="product.html">Wool/Cashmera basic cardigan</a>
                    </div>
                    <!-- End product name -->
                    <!-- Product price -->
                    <div class="result-product__price">$49.5</div>
                    <!-- End product price -->
                </div>
                <!-- End product -->
                <!-- Product -->
                <div class="result-product">
                    <!-- Image -->
                    <div class="result-product__image">
                        <a href="product.html">
                            <img src="assets_client/assets/products/1/2_4-a.jpg" alt="Product image" />
                        </a>
                    </div>
                    <!-- End image -->
                    <!-- Product name -->
                    <div class="result-product__name"><a href="product.html">Regular fit striped cotton
                            shirt</a></div>
                    <!-- End product name -->
                    <!-- Product price -->
                    <div class="result-product__price">$79.99</div>
                    <!-- End product price -->
                </div>
                <!-- End product -->
                <!-- Product -->
                <div class="result-product">
                    <!-- Image -->
                    <div class="result-product__image">
                        <a href="product.html">
                            <img src="assets_client/assets/products/1/3_1-a.jpg" alt="Product image" />
                        </a>
                    </div>
                    <!-- End image -->
                    <!-- Product name -->
                    <div class="result-product__name"><a href="product.html">Slim fit modal cotton shity</a>
                    </div>
                    <!-- End product name -->
                    <!-- Product price -->
                    <div class="result-product__price">
                        <!-- Price new -->
                        <span class="result-product__price-new">$79.99</span>
                        <!-- End price new -->
                        <!-- Price old -->
                        <span class="result-product__price-old">$99.99</span>
                        <!-- End price old -->
                    </div>
                    <!-- End product price -->
                </div>
                <!-- End product -->
            </div>
            <!-- End results -->
            <!-- Results action -->
            <div class="search-popup__results-action">
                <a href="#" class="fifth-button">All results (12)</a>
            </div>
            <!-- End results actions -->
        </div>
        <!-- End search results -->
    </div>
    <!-- End container -->
</div>
<!-- End search popup -->
