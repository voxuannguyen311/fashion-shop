 <!-- Header -->
 <header class="header header--type-5">

     @php
         $check = Auth::guard('customer')->check();
         $user = Auth::guard('customer')->user();
     @endphp
     <!-- Promo bar -->
     <div class="header__promo-bar">
         @if ($check)
             <li><a href="#">Chào Bạn, {{ $user->ho_va_ten }} đến với cửa hàng của DUROTAN</a></li>
         @else
             <a href="#"><i class="lnil lnil-tag"></i> <span>mega sale</span> discount all item up to 30% for
                 christmas event</a>
         @endif
     </div>
     <!-- End promo bar -->
     <!-- Container -->
     <div class="container container--type-2">
         <!-- Header container -->
         <div class="header__container d-flex align-items-center">
             <!-- Mobile menu -->
             {{-- <div class="header__mobile-menu">
                <!-- Open mobile menu -->
                <div class="mobile-menu__open">
                    <a href="#" class="js-open-mobile-menu"><i class="lnil lnil-menu"></i></a>
                </div>
                <!-- End open mobile menu -->
                <!-- Mobile menu -->
                <div class="mobile-menu js-mobile-menu">
                    <!-- Overlay -->
                    <div class="mobile-menu__overlay js-close-mobile-menu"></div>
                    <!-- End overlay -->
                    <!-- Content -->
                    <div class="mobile-menu__content">
                        <!-- Close mobile menu -->
                        <div class="mobile-menu__close">
                            <a href="#" class="js-close-mobile-menu"><i class="lnil lnil-close"></i></a>
                        </div>
                        <!-- End close mobile menu -->
                        <!-- Mobile logo -->
                        <h3 class="mobile-menu__logo">DUROTAN</h3>
                        <!-- End mobile logo -->
                        <!-- Mobile Nav -->
                        <ul class="mobile-menu__nav">
                            <li class="mobile-menu__dropdown">
                                <a href="index.html">Home</a>
                                <ul class="mobile-menu__dropdown-menu js-mobile-menu-dropdown-menu">
                                    <li><a href="index.html">Home Page 1</a></li>
                                    <li><a href="index-2.html">Home Page 2</a></li>
                                    <li><a href="index-3.html">Home Page 3</a></li>
                                    <li><a href="index-4.html">Home Page 4</a></li>
                                    <li><a href="index-5.html">Home Page 5</a></li>
                                    <li><a href="index-6.html">Home Page 6</a></li>
                                    <li><a href="index-7.html">Home Page 7</a></li>
                                    <li><a href="index-8.html">Home Page 8</a></li>
                                    <li><a href="index-9.html">Home Page 9</a></li>
                                    <li><a href="index-10.html">Home Page 10</a></li>
                                </ul>
                                <div class="mobile-menu__dropdown-btn js-mobile-menu-dropdown-btn"><span
                                        class="lnil lnil-chevron-down"></span></div>
                            </li>
                            <li><a href="about.html">About</a></li>
                            <li class="mobile-menu__dropdown">
                                <a href="shop.html">Shop</a>
                                <ul class="mobile-menu__dropdown-menu js-mobile-menu-dropdown-menu">
                                    <li><a href="shop.html">Shop Page </a></li>
                                    <li><a href="product.html">Shop Details</a></li>
                                    <li><a href="cart.html">Cart Page</a></li>
                                </ul>
                                <div class="mobile-menu__dropdown-btn js-mobile-menu-dropdown-btn"><span
                                        class="lnil lnil-chevron-down"></span></div>
                            </li>
                            <li class="mobile-menu__dropdown">
                                <a href="404.html">Pages</a>
                                <ul class="mobile-menu__dropdown-menu js-mobile-menu-dropdown-menu">
                                    <li><a href="404.html">404 Page </a></li>
                                    <li><a href="about.html">About</a></li>
                                    <li><a href="cart.html">Cart</a></li>
                                    <li><a href="checkout.html">Checkout</a></li>
                                    <li><a href="coming-soon.html">Coming Soon</a></li>
                                    <li><a href="contact.html">Contact</a></li>
                                    <li><a href="wishlist.html">Wishlist</a></li>
                                </ul>
                                <div class="mobile-menu__dropdown-btn js-mobile-menu-dropdown-btn"><span
                                        class="lnil lnil-chevron-down"></span></div>
                            </li>
                            <li class="dropdown">
                                <a href="blog.html">News</a>
                                <ul class="mobile-menu__dropdown-menu js-mobile-menu-dropdown-menu">
                                    <li><a href="blog.html">Blog 1</a></li>
                                    <li><a href="blog-with-sidebar.html">Blog 2</a></li>
                                    <li><a href="post.html">Blog Single</a></li>
                                </ul>
                                <div class="mobile-menu__dropdown-btn js-mobile-menu-dropdown-btn"><span
                                        class="lnil lnil-chevron-down"></span></div>
                            </li>
                        </ul>
                        <!-- End Mobile nav -->
                    </div>
                    <!-- End content -->
                </div>
                <!-- End mobile menu -->
            </div> --}}
             <!-- End mobile menu -->
             <!-- Logo -->
             <h1 class="header__logo">
                 <a href="/">
                     DUROTAN
                 </a>
             </h1>
             <!-- End logo -->
             <!-- Navigation -->
             <ul class="header__nav">
                 <li>
                     <a href="/" class="nav__item">Home</a>
                 </li>

                 <li>
                     <a href="/cua-hang" class="nav__item">Shop</a>
                     <!-- MegaMenu -->
                     <div class="nav__mega-menu">
                         <!-- Column -->
                         <div class="mega-menu__standard-column">
                             <!-- Column title -->
                             <div class="standard-column__title">Sách</div>

                             <ul class="standard-column__nav">
                                 @foreach ($theLoai as $theloai)
                                     <li><a href="/the-loai/{{ $theloai->id }}">{{ $theloai->the_loai }}</a></li>
                                 @endforeach
                             </ul>
                             <!-- End column nav -->
                         </div>
                     </div>
                     <!-- End MegaMenu -->
                 </li>



                 <li>
                     <a href="/bai-viet" class="nav__item">Bài Viết</a>
                 </li>
                 <li>
                     <a href="/lien-he" class="nav__item">Liên Hệ</a>
                 </li>
                 <li>
                     <a href="/gioi-thieu" class="nav__item">Giới Thiểu</a>
                 </li>

             </ul>
             <!-- End navigation -->
             <!-- Header right -->
             <ul class="header__right">
                 <li>
                     <form action="/tim-kiem" method="POST" class="d-flex" role="search">
                         @csrf
                         <input class="form-control me-2" name="search" type="search" placeholder="Search"
                             aria-label="Search">
                         <button class="btn btn-outline-success" type="submit">Search</button>
                     </form>

                 </li>
                 <li class="header__cart">
                     <a href="cart.html" class="js-open-canvas-cart" style="padding-right: 34px"><i
                             class="lnil lnil-cart"></i></a>
                 </li>
                 @if ($check)
                     <div class="header__nav">
                         <li> <a href="#" class="nav__item"><i class="fa-solid fa-user"
                                     style="color: #eaddc7; font-size: 26px;"></i></a>
                             <ul class="nav__dropdown-menu">
                                 <li><a href="/client/cap-nhap-thong-tin">Trang Cá Nhân</a></li>
                                 <li><a href="/client/cap-nhap-mat-khau">Đổi Mật Khẩu</a></li>
                                 <li><a href="/client/lich-su-mua-hang">Lịch sử mua hàng</a></li>
                                 <li><a href="/logout">Đăng Xuất</a></li>
                             </ul>
                         </li>
                     </div>
                 @else
                     <li class="d-none d-lg-block"><a href="/login_register">Sign in</a></li>
                 @endif
             </ul>
             <!-- End header right -->
         </div>
         <!-- End header container -->
     </div>
     <!-- End container -->
 </header>
 <!-- End header -->
