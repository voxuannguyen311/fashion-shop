<!DOCTYPE html>
<html lang="en">
{{--
@if ($cart != null)
    <p>ok</p>
@endif --}}

<head>
    @include('client.share.css')
</head>

<body>

    <div id="main">
        @include('client.share.header')
        @yield('noi_dung')
        @include('client.share.footer')
        @include('client.share.js')
        @yield('js')
    </div>
</body>
<!-- Mirrored from demo2.ninethemes.net/durotan20/html/index-5.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 03 Feb 2023 04:05:21 GMT -->

</html>
