<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sanpham extends Model
{
    use HasFactory;

    protected $table = 'sanphams';
    protected $fillable = [
      'ten_san_pham',
      'slug_san_pham',
      'the_loai',
      'tinh_trang',
      'gia_ban',
      'hinh_anh',
      'mo_ta',
      'so_luong'
    ];
}

