<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class KichHoatDoiMatKhau extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        return $this->subject('Kích Hoạt Đổi Mật Khẩu Tài Khoản Tại Website....')
                    ->view('client.page.kich_hoat_doi_mat_khau', [
                        'data'   => $this->data,
                    ]);
    }


}
