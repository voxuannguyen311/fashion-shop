<?php
// Trong thư mục app/Http/ViewComposers
namespace App\Http\ViewComposers;

use Illuminate\View\View;

use App\Models\TheLoai;
use Illuminate\Support\Facades\Auth;



class headerComposer
{
    public function compose(View $view)
    {
        // Truyền dữ liệu vào view
        $theLoai = TheLoai::all();
        $view->with('theLoai', $theLoai);

    }
}
