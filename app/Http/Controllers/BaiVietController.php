<?php

namespace App\Http\Controllers;

use App\Models\QuanLyBaiViet;
use Illuminate\Http\Request;

class BaiVietController extends Controller
{
    public function BaiViet(){

        $BaiViet = QuanLyBaiViet::where('is_open', 1)->paginate(6);

        return view("client.page.bai_viet", compact('BaiViet'));
    }


    public function ChiTietBaiViet($id){

        $ChiTietBaiViet = QuanLyBaiViet::find($id);

        return view("client.page.bai_viet_chi_tiet", compact('ChiTietBaiViet'));
    }
}
