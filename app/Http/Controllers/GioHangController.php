<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\GiohangModel;
use App\Models\Sanpham;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class GioHangController extends Controller
{
    public function ThemSoLuong($id)
    {
        $sanpham = Sanpham::where('id', $id)->first();

        if ($sanpham) {
            $user = Auth::guard('customer')->user();
            $ma_khach_hang = $user->id;
            $existingCartItem = GiohangModel::where('ma_khach_hang', $ma_khach_hang)
                ->where('ma_san_pham', $id)
                ->first();
            if ($existingCartItem) {
                // Cập nhật số lượng nếu sản phẩm đã có trong giỏ hàng
                $existingCartItem->tong_so_luong += 1;
                $existingCartItem->tong_tien = $sanpham->gia_ban * $existingCartItem->tong_so_luong;
                $existingCartItem->save();
            } else {
                // Thêm sản phẩm mới vào giỏ hàng nếu chưa tồn tại
                GiohangModel::create([
                    'ma_khach_hang' => $ma_khach_hang,
                    'ma_san_pham' => $id,
                    'tong_so_luong' => 1,
                    'tong_tien' => $sanpham->gia_ban
                ]);
            }
            return response()->json([
                'status' => true,
                'message' => 'thêm giỏ hàng thành công !!',
            ]);
        }
    }


    public function TruSoLuong($id)
    {

        $sanpham = Sanpham::where('id', $id)->first();

        if ($sanpham) {
            $user = Auth::guard('customer')->user();
            $ma_khach_hang = $user->id;
            $existingCartItem = GiohangModel::where('ma_khach_hang', $ma_khach_hang)
                ->where('ma_san_pham', $id)
                ->first();
            if ($existingCartItem) {
                // Giảm số lượng nếu sản phẩm đã có trong giỏ hàng
                $existingCartItem->tong_so_luong -= 1;
                if ($existingCartItem->tong_so_luong <= 0) {
                    // Xóa sản phẩm nếu số lượng giảm xuống 0 hoặc âm
                    $existingCartItem->delete();
                    return response()->json([
                        'status' => true,
                        'message' => 'Xóa sản phẩm khỏi giỏ hàng !!',
                    ]);
                } else {
                    // Cập nhật tổng tiền nếu số lượng còn lại
                    $existingCartItem->tong_tien = $sanpham->gia_ban * $existingCartItem->tong_so_luong;
                    $existingCartItem->save();
                    return response()->json([
                        'status' => true,
                        'message' => 'Giảm số lượng thành công !!',
                    ]);
                }
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Sản phẩm không tồn tại trong giỏ hàng !!',
                ]);
            }
        }
    }

    public function HienThiDsGioHang()
    {
        $khach_hang = Auth::guard('customer')->user();
        $ma_khach_hang = $khach_hang->id;

        $gio_hang = GiohangModel::where('ma_khach_hang', $ma_khach_hang)
            ->join('sanphams', 'gio_hang.ma_san_pham', '=', 'sanphams.id')
            ->get();

        $tinh_tong_tong_tien = $gio_hang->sum('tong_tien');

        // Đếm tổng số lượng sản phẩm
        $tong_so_luong_san_pham = $gio_hang->sum('tong_so_luong');
        return response()->json([
            'status'         => true,
            'gio_hang'       => $gio_hang,
            'tong_tien_tat_ca'      => $tinh_tong_tong_tien,
            'tong_so_luong'  => $tong_so_luong_san_pham,
        ]);
    }

    public function XoaSanPhamGioHang($id)
    {
        $sanpham = Sanpham::where('id', $id)->first();

        if ($sanpham) {
            $user = Auth::guard('customer')->user();
            $ma_khach_hang = $user->id;
            $existingCartItem = GiohangModel::where('ma_khach_hang', $ma_khach_hang)
                ->where('ma_san_pham', $id)
                ->first();

            if ($existingCartItem) {
                // Xóa sản phẩm khỏi giỏ hàng
                $existingCartItem->delete();

                return response()->json([
                    'status'  => true,
                    'message' => 'Xóa sản phẩm khỏi giỏ hàng thành công !!',
                ]);
            } else {
                return response()->json([
                    'status'  => false,
                    'message' => 'Sản phẩm không tồn tại trong giỏ hàng !!',
                ]);
            }
        }
    }

    public function GioHang()
    {
        return view('client.page.view_cart');
    }
    public function ThanhToan()
    {
        return view('client.page.thanh_toan');
    }


    public function MuaHangNgay($id)
    {
        $sanpham = Sanpham::where('id', $id)->first();


        if ($sanpham) {
            $user = Auth::guard('customer')->user();
            $ma_khach_hang = $user->id;
            $existingCartItem = GiohangModel::where('ma_khach_hang', $ma_khach_hang)
                ->where('ma_san_pham', $id)
                ->first();
            if ($existingCartItem) {
                // Cập nhật số lượng nếu sản phẩm đã có trong giỏ hàng
                $existingCartItem->tong_so_luong += 1;
                $existingCartItem->tong_tien = $sanpham->gia_ban * $existingCartItem->tong_so_luong;
                $existingCartItem->save();
            } else {
                // Thêm sản phẩm mới vào giỏ hàng nếu chưa tồn tại
                GiohangModel::create([
                    'ma_khach_hang' => $ma_khach_hang,
                    'ma_san_pham' => $id,
                    'tong_so_luong' => 1,
                    'tong_tien' => $sanpham->gia_ban
                ]);
            }
            return view('client.page.thanh_toan');
        }
    }
}
