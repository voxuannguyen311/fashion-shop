<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\HoadonModel;
use App\Models\Sanpham;
use Illuminate\Http\Request;

class ThongKe extends Controller
{
    public function index(){
        return view("AdminRocker.page.ThongKe.index");
    }


    public function tongSoDonHang(){
        $tong_don_hang = HoadonModel::count();

        // dd($tong_don_hang);
        return response()->json([
            'status'            => true,
            'message'           => 'lấy dữ liệu Thành Công',
            'tong_don_hang'           => $tong_don_hang,
        ]);
    }
    public function tongSoSanPham(){
        $tong_so_luong_san_pham = Sanpham::count();

        // dd($tong_so_luong_san_pham);
        return response()->json([
            'status'            => true,
            'message'           => 'lấy dữ liệu Thành Công',
            'tong_so_luong_san_pham'           => $tong_so_luong_san_pham,
        ]);
    }

    public function tongSoKhachHang(){
        $Tong_khach_hang = Customer::count();

        // dd($Tong_khach_hang);
        return response()->json([
            'status'            => true,
            'message'           => 'lấy dữ liệu Thành Công',
            'Tong_khach_hang'           => $Tong_khach_hang,
        ]);
    }

    public function TongDoanhThu(){
        $tong_doanh_thu = HoadonModel::sum('tong_tien_tat_ca');

        // Trả về JSON chứa tổng doanh thu
        return response()->json([
            'status'        => true,
            'message'       => 'Lấy dữ liệu thành công',
            'tong_doanh_thu'=> $tong_doanh_thu,
        ]);
    }

}
