<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Models\GiohangModel;
use App\Models\HoadonchitietModel;
use App\Models\HoadonModel;
use App\Models\Sanpham;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ThanhToanControllerr extends Controller
{
    public function ThanhToan(Request $request)
    {

        $now = Carbon::now();
        $date = $now->toDateString();
        $time = $now->toTimeString();
        $dateWithoutSpecialChars = str_replace(['-', ':'], '', $date);
        $timeWithoutSpecialChars = str_replace(['-', ':'], '', $time);
        $ma_don_hang = "DH$dateWithoutSpecialChars$timeWithoutSpecialChars";

        $user = Auth::guard('customer')->user();
        $user_id = $user->id;
        $ho_va_ten = $request->ho_va_ten;
        $so_dien_thoai = $request->so_dien_thoai;
        $dia_chi = $request->dia_chi;
        $tinh_tong_tong_tien = ceil($request->tong_tien_tat_ca);
        // dd( $tinh_tong_tong_tien);
        // $ma_don_hang = Str::uuid();
        $vnp_OrderInfo = "cam on quy khach da dat hang";

        $vnp_Url = "https://sandbox.vnpayment.vn/paymentv2/vpcpay.html";
        $vnp_Returnurl = "http://127.0.0.1:8000/client/lich-su-mua-hang";
        $vnp_TmnCode = "3J0W4BYL"; //Mã website tại VNPAY
        $vnp_HashSecret = "SXSKZJKWQWYXHKTRWYJFQYUUIBXNABMO"; //Chuỗi bí mật

        $vnp_TxnRef = $ma_don_hang; //Mã đơn hàng. Trong thực tế Merchant cần insert đơn hàng vào DB và gửi mã này sang VNPAY
        $vnp_OrderInfo = $vnp_OrderInfo;
        $vnp_OrderType = 'billpayment';
        $vnp_Amount = $tinh_tong_tong_tien * 100;
        $vnp_Locale = 'vn';

        $vnp_IpAddr = $_SERVER['REMOTE_ADDR'];

        $inputData = array(
            "vnp_Version" => "2.1.0",
            "vnp_TmnCode" => $vnp_TmnCode,
            "vnp_Amount" => $vnp_Amount,
            "vnp_Command" => "pay",
            "vnp_CreateDate" => date('YmdHis'),
            "vnp_CurrCode" => "VND",
            "vnp_IpAddr" => $vnp_IpAddr,
            "vnp_Locale" => $vnp_Locale,
            "vnp_OrderInfo" => $vnp_OrderInfo,
            "vnp_OrderType" => $vnp_OrderType,
            "vnp_ReturnUrl" => $vnp_Returnurl,
            "vnp_TxnRef" => $vnp_TxnRef,
        );

        if (isset($vnp_BankCode) && $vnp_BankCode != "") {
            $inputData['vnp_BankCode'] = $vnp_BankCode;
        }

        ksort($inputData);
        $query = "";
        $i = 0;
        $hashdata = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashdata .= '&' . urlencode($key) . "=" . urlencode($value);
            } else {
                $hashdata .= urlencode($key) . "=" . urlencode($value);
                $i = 1;
            }
            $query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vnp_Url = $vnp_Url . "?" . $query;
        if (isset($vnp_HashSecret)) {
            $vnpSecureHash = hash_hmac('sha512', $hashdata, $vnp_HashSecret);
            $vnp_Url .= 'vnp_SecureHash=' . $vnpSecureHash;
        }

        $returnData = array(
            'code' => '00', 'message' => 'success', 'data' => $vnp_Url
        );
        if (isset($_POST['redirect'])) {

            HoadonModel::create([
                'id'   => $ma_don_hang,
                'ma_khach_hang' => $user_id,
                'ho_va_ten' =>  $ho_va_ten,
                'so_dien_thoai' => $so_dien_thoai,
                'dia_chi' => $dia_chi,
                'tong_tien_tat_ca' => $tinh_tong_tong_tien,
                'trang_thai_thanh_toan' => 1
            ]);

            $gio_hang = GiohangModel::where('ma_khach_hang', $user_id)->get();
            foreach ($gio_hang as $item) {
                HoadonchitietModel::create([
                    'ma_hoa_don' => $ma_don_hang,
                    'ma_san_pham' => $item->ma_san_pham,
                    'tong_so_luong' => $item->tong_so_luong,
                    'tong_tien' => $item->tong_tien
                ]);
                // Trừ số lượng sản phẩm đã bán từ bảng sản phẩm
                $san_pham = Sanpham::find($item->ma_san_pham);
                $san_pham->so_luong -= $item->tong_so_luong;

                $san_pham->save();
            }
            GioHangModel::where('ma_khach_hang', $user_id)->delete();

            header('Location: ' . $vnp_Url);
            die();
        } else {
            return response()->json($returnData);
        }
    }


    public function LichSuMuaHang()
    {
        return view('client.page.LichSuMuaHang');
    }
    public function DsLichSuMuaHang()
    {
        $user = Auth::guard('customer')->user();
        $user_id = $user->id;
        $ds_hoa_don = HoadonModel::where('ma_khach_hang', $user_id)->get();

        foreach ($ds_hoa_don as $hoa_don) {
            $hoa_don_chi_tiet = HoadonchitietModel::where('ma_hoa_don', $hoa_don->id)
                ->join('sanphams', 'hoa_don_chi_tiet.ma_san_pham', '=', 'sanphams.id')

                ->select(
                    'hoa_don_chi_tiet.id',
                    'hoa_don_chi_tiet.tong_so_luong',
                    'hoa_don_chi_tiet.tong_tien',
                    'hoa_don_chi_tiet.ma_hoa_don',
                    'hoa_don_chi_tiet.created_at',
                    'sanphams.ten_san_pham',
                    'sanphams.gia_ban',
                    'sanphams.hinh_anh',

                )
                ->get();
            // Gán dữ liệu chi tiết vào trường mới trong mỗi phần tử hóa đơn
            $hoa_don->ds_hoa_don_chi_tiet = $hoa_don_chi_tiet;
        }
        // dd($ds_hoa_don);
        return response()->json([
            'status'            => true,
            'message'           => 'lấy dữ liệu Thành Công',
            'du_lieu'           => $ds_hoa_don,
        ]);
    }

}
