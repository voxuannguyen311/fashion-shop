<?php

namespace App\Http\Controllers;

use App\Http\Requests\CapNhapMatKhauRequest;
use App\Jobs\SendMailDoiMatKhau;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CapNhapThongTinRequest;
use App\Http\Requests\RegisterAccountRequest;
use App\Http\Requests\UpdatePasswordRequest;
use Illuminate\Support\Str;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Jobs\SendMailJob;

class CustomerController extends Controller
{
    public function viewRegister()
    {
        return view('client.page.login_register');
    }

    public function actionRegister(RegisterAccountRequest $request)
    {
        $data = $request->all();
        $hash = Str::uuid(); // tạo ra 1 biến tên hash kiểu string có 36 ký tự không trùng với nhau
        $data['hash_mail'] = $hash;
        $data['password']  = bcrypt($data['password']);
        Customer::create($data);

        // // Phân cụm này qua JOB
        $dataMail['ho_va_ten'] = $request->ho_va_ten;
        $dataMail['email']     = $request->email;
        $dataMail['hash_mail'] = $hash;

        SendMailJob::dispatch($dataMail);
        // SendMailJob::dispatch($dataMail);
        // // End Phân JOB
        return response()->json([
            'status'    =>  true,
            'message'   =>  'Đăng Ký thành công'
        ]);
    }

    public function actionActive($hash)
    {
        $account = Customer::where('hash_mail', $hash)->first();
        if ($account && $account->loai_tai_khoan == 0) {
            $account->loai_tai_khoan = 1;
            $account->hash_mail = '';
            $account->save();
            toastr()->success('Đã kích hoạt tài khoản thành công!');
        } else {
            toastr()->error('Thông tin không chính xác!');
        }
        return redirect('/login_register');
    }

    public function actionLogin(LoginRequest $request)
    {
        $data['email']      = $request->email;
        $data['password']   = $request->password;
        $check = Auth::guard('customer')->attempt($data);
        if ($check) {
            $customer = Auth::guard('customer')->user();
            if ($customer->loai_tai_khoan == -1) {
                toastr()->error("Tài khoản đã bị khóa!");
                Auth::guard('customer')->logout();
                return response()->json([
                    'status' => false,
                    'message' => 'Tài khoản đã bị khóa',
                ]);
            } else if ($customer->loai_tai_khoan == 0) {
                toastr()->warning("Tài khoản chưa được kích hoạt!");
                Auth::guard('customer')->logout();
                return response()->json([
                    'status' => false,
                    'message' => 'Tài khoản chưa được kích hoạt',
                ]);
            }
            return response()->json([
                'status' => true,
                'message' => 'Đăng nhập thành công',
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Tài khoản hoặc mật khẩu không đúng',
            ]);
        }
    }
    // quên mật khẩu
    public function actionResetPassword(Request $request)
    {
        $customer = Customer::where('email', $request->email)->first();
        $hash     = Str::uuid();

        $customer->hash_reset = $hash;
        $customer->save();

        // Phân cụm này qua JOB
        $dataMail['ho_va_ten'] = $request->ho_va_ten;
        $dataMail['hash_reset'] = $hash;
        $dataMail['email']     = $request->email;
        SendMailDoiMatKhau::dispatch($dataMail);

        //  SendMailDoiMatKhau::dispatch($dataMail);
        // End Phân JOB
        return response()->json([
            'status' => true,
            'message' => 'Vui lòng kiểm tra email',
        ]);
    }
    public function viewUpdatePassword($hash)
    {
        $customer = Customer::where('hash_reset', $hash)->first();
        if ($customer) {
            return view('client.page.quen_mat_khau', compact('hash'));
        } else {
            toastr()->error('Liên kết không tồn tại!');
            return redirect('/');
        }
    }
    public function actionUpdatePassword(UpdatePasswordRequest $request)
    {

        $customer = Customer::where('hash_reset', $request->hash_reset)->first();
        $customer->hash_reset = '';
        $customer->password = bcrypt($request->password);
        $customer->save();

        return response()->json([
            'status' => true,
            'message' => 'Đã cập nhật mật khẩu thành công!',
        ]);
    }
    // profile
    public function viewCapNhapThongTin()
    {
        return view('client.page.profile');
    }

    public function capNhapThongTin(CapNhapThongTinRequest $request)
    {

        // Dòng đầu tiên: Lấy "id" của người dùng đã xác thực từ guard "customer" (giả định là khách hàng) và gán vào biến $id.
        // Dòng thứ hai: Sử dụng giá trị $id để tìm thông tin khách hàng tương ứng trong cơ sở dữ liệu và gán vào biến $user.
        $data = $request->all();
        $id = Auth::guard('customer')->user()->id;
        $user = Customer::find($id);
        $user->update($data);

        return response()->json([
            'status' => true,
            'message' => 'Đã cập nhật thông tin thành công!',
        ]);
    }

    public function thong_tin_nguoi_dang_nhap()
    {
        $id = Auth::guard('customer')->user()->id;
        $user = Customer::find($id);

        return response()->json([
            'user'  => $user
        ]);
    }

    // đổi mật khâu bên trong trang cá nhân
    public function viewCapNhapMatKhau()
    {
        return view('client.page.cap_nhap_mat_khau');
    }

    public function capNhapMatKhau(CapNhapMatKhauRequest $request)
    {

        $id = Auth::guard('customer')->user()->id;
        $user = Customer::find($id);
        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json([
            'status' => true,
            'message' => 'Đã cập nhật mật khẩu thành công!',
        ]);
    }

    // logout
    public function actionLogout()
    {
        Auth::guard('customer')->logout();
        toastr()->success('Đăng Xuất Thành Công');
        return redirect('/login_register');
    }



    // quản lý khách hàng

    public function viewThongTin()
    {
        return view('AdminRocker.page.KhachHang.index');
    }

    public function getData()
    {
        $data = Customer::get();
        return response()->json([
            'data'  => $data,
        ]);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $phim = Customer::where('id', $request->id)->first();
        $phim->update($data);

        return response()->json([
            'status'    => true,
        ]);
    }

    public function destroy(Request $request)
    {
        Customer::where('id', $request->id)->first()->delete();

        return response()->json([
            'status'    => true,
        ]);
    }

    public function changeStatus($id)
    {
        $change = Customer::find($id);
        if($change->loai_tai_khoan == -1) {
            $change->loai_tai_khoan = 1;
        } else  {
            $change->loai_tai_khoan = -1;
        }
        $change->save();
    }
    public function kichHoat($id)
    {
        $kickHoat = Customer::find($id);
        if($kickHoat->loai_tai_khoan == 0) {
            $kickHoat->loai_tai_khoan = 1;
            $kickHoat['hash_mail'] = Str::uuid();
        }
        else if($kickHoat->loai_tai_khoan == 1) {
            $kickHoat->loai_tai_khoan = 0;
            $kickHoat['hash_mail'] = '';
        }
        $kickHoat->save();
    }

    public function changePassword(Request $request)
    {
        $data = $request->all();
        $khachHang = Customer::find($data['id']);
        $khachHang->password = bcrypt($data['password']);
        $khachHang->save();

        return response()->json([
            'status'    => true,
        ]);
    }
}
