<?php

namespace App\Http\Controllers;

use App\Models\BinhLuan;
use Illuminate\Http\Request;

class BinhLuanController extends Controller
{
    public function BinhLuan(Request $request)
    {
        BinhLuan::create($request->all());
        return redirect()->back();
    }

    public function index(){
        return view("AdminRocker.page.BinhLuan.index");
    }


    public function getData ()
    {
        $ds_binh_luan = BinhLuan::join('sanphams', 'sanphams.id', '=', 'binh_luans.ma_san_pham')
        ->join('customers', 'customers.id', '=', 'binh_luans.ma_khach_hang')
        ->get();
// dd($ds_binh_luan);
        return response()->json([
            'status'            => true,
            'message'           => 'lấy dữ liệu Thành Công',
            'du_lieu'           => $ds_binh_luan,
        ]);
    }
}
