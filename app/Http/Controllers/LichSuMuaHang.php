<?php

namespace App\Http\Controllers;

use App\Models\HoadonchitietModel;
use App\Models\HoadonModel;
use Illuminate\Http\Request;

class LichSuMuaHang extends Controller
{
    public function index(){
        return view('AdminRocker.page.LichSuMuaHang.index');
    }

    public function getData()
{
    // Lấy tất cả các hóa đơn và chi tiết hóa đơn
    $ds_hoa_don = HoadonModel::all();

    foreach ($ds_hoa_don as $hoa_don) {
        $hoa_don_chi_tiet = HoadonchitietModel::where('ma_hoa_don', $hoa_don->id)
            ->join('sanphams', 'hoa_don_chi_tiet.ma_san_pham', '=', 'sanphams.id')
            ->select(
                'hoa_don_chi_tiet.id',
                'hoa_don_chi_tiet.tong_so_luong',
                'hoa_don_chi_tiet.tong_tien',
                'hoa_don_chi_tiet.ma_hoa_don',
                'hoa_don_chi_tiet.created_at',
                'sanphams.ten_san_pham',
                'sanphams.gia_ban',
                'sanphams.hinh_anh',
            )
            ->get();

        // Gán dữ liệu chi tiết vào trường mới trong mỗi phần tử hóa đơn
        $hoa_don->ds_hoa_don_chi_tiet = $hoa_don_chi_tiet;
    }

    // dd($ds_hoa_don);
    return response()->json([
        'status'            => true,
        'message'           => 'lấy dữ liệu Thành Công',
        'du_lieu'           => $ds_hoa_don,
    ]);
}


}
