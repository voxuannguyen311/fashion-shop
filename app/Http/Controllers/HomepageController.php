<?php

namespace App\Http\Controllers;

use App\Jobs\SendMailLienHe;
use App\Models\BinhLuan;
use App\Models\LienHe;
use App\Models\Sanpham;
use App\Models\TheLoai;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class HomepageController extends Controller
{
    public function home()
    {
        $threeDaysAgo = Carbon::now()->subDays(3)->toDateString();

        $san_pham_new = Sanpham::where('sanphams.tinh_trang', '>', 0)
            ->whereDate('sanphams.created_at', '>=', $threeDaysAgo)
            ->get();

        $Sanpham = Sanpham::where('tinh_trang', 1)->paginate(10);


        return view('client.page.homepage', compact('san_pham_new', 'Sanpham'));
    }

    public function chiTietSanPham($theloai, $slug, $id)
    {
        $san_pham = Sanpham::where('id', $id)->first();

        $ds_binh_luan = BinhLuan::where('binh_luans.ma_san_pham', $id)
        ->join('sanphams', 'sanphams.id', '=', 'binh_luans.ma_san_pham')
        ->join('customers', 'customers.id', '=', 'binh_luans.ma_khach_hang')
        ->get();
// dd($ds_binh_luan);
        return view('client.page.chi_tiet_product', compact('san_pham','ds_binh_luan'));
    }

    public function SanPham()
    {
        $Sanpham = Sanpham::where('tinh_trang', 1)->paginate(10);
        return view("client.page.san_pham", compact('Sanpham'));
    }

    //
    public function GioiThieu()
    {
        return view("client.page.gioi_thieu");
    }


    public function LienHe()
    {
        return view("client.page.lien_he");
    }

    public function GuiLienHe(Request $request)
    {
        $data = $request->all();
        LienHe::create($data);
        // Phân cụm này qua JOB
        $dataMail['ho_va_ten'] = $request->ho_va_ten;
        $dataMail['email']     = $request->email;
        $dataMail['tieu_de']   = $request->tieu_de;
        $dataMail['noi_dung']  = $request->noi_dung;

        SendMailLienHe::dispatch($dataMail);

        return response()->json([
            'status'    =>   true,
            'message'   =>   'Đã gửi liên hệ chúng tôi sẽ phản hồi sớm !'
        ]);
    }



    public function actionTimKiem(Request $request)
    {
        $search = $request->search;
        $cleaned_search = preg_replace('/[^\p{L}\p{N}\s]/u', '', $search);

        $ds_sanpham = Sanpham::where('ten_san_pham', 'like', '%' . $cleaned_search . '%')->paginate(6);

        return view("client.page.ds_san_pham", compact('ds_sanpham'));
    }

    public function TheLoai($id)
    {
        $Sanpham = TheLoai::join('sanphams', 'the_loais.id', '=', 'sanphams.the_loai')
                    ->where('the_loais.id', $id) // Lọc theo id của thể loại
                    ->where('sanphams.tinh_trang', 1) // Lọc theo tình trạng
                    ->paginate(10); // Phân trang

        return view("client.page.san_pham_the_loai", compact('Sanpham'));
    }



}
