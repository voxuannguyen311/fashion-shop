<?php

namespace App\Http\Controllers;

use App\Models\TheLoai;
use Illuminate\Http\Request;

class TheLoaiController extends Controller
{
    public function index()
    {
        return view('AdminRocker.page.TheLoai.index');
    }

    public function store(Request $request)
    {
        $data =  $request->all();
        TheLoai::create($data);
        return response()->json([
            'status'    =>  true,
            'message'   =>  'Thêm thành công'
        ]);
    }

    public function getData()
    {
        $data = TheLoai::all();
        return response()->json(['data' => $data]);
    }

    public function deleteLienHe(Request $request)
    {
        TheLoai::find($request->id)->delete();
        return response()->json([
            'status'    =>      true,
            'message'   =>      'Đã xóa thể loại thành công !'
        ]);
    }

}
