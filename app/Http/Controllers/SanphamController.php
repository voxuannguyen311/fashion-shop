<?php

namespace App\Http\Controllers;

use App\Models\Sanpham;
use Illuminate\Http\Request;
use Illuminate\Container\Container;

class SanphamController extends Controller
{
    public function index()
    {
        return view('AdminRocker.page.SanPham.index');
    }

    public function store(Request $request)
    {
        $data =  $request->all();
        Sanpham::create($data);
        return response()->json([
            'status'    =>  true,
            'message'   =>  'Thêm thành công'
        ]);
    }

    public function getData(){
        $data = Sanpham::get();
        return response()->json([
            'list'  => $data
        ]);
    }

    public function update(Request $request){
        $sanpham = Sanpham::where('id', $request->id)->first();
        if($sanpham){
            $sanpham->update($request->all());
        }
        return response()->json([
            'status' => true,
            'message'  => "Cập nhập sản phẩm  thành công!",
        ]);
    }

    public function destroy(Request $request){
        $sanpham = Sanpham::where('id', $request->id)->first();
        if($sanpham){
            $sanpham->delete();
        }
        return response()->json([
            'status'    => true,
            'message'  => "Xóa sản phẩm thành công!",
        ]);
    }

    public function changeStatus($id){
        $sanpham = Sanpham::where('id', $id)->first();
        if ($sanpham) {
            $sanpham->tinh_trang =! $sanpham->tinh_trang;
            $sanpham->save();
        }
    }




}
