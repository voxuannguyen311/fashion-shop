<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePasswordRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'password'              =>  'required|min:6|max:30',
            're_password'           =>  'required|same:password',
            'hash_reset'            =>  'required|exists:customers,hash_reset',
        ];
    }

    public function messages()
    {
        return [
            'hash_reset.*'              =>  'Liên kết không tồn tại',
            'password.required'         => 'Mật khẩu không được bỏ trống',
            'password.min'              => 'Mật khẩu phải từ 6 ký tự trở lên',
            'password.max'              => 'Mật khẩu không được vượt quá 30 ký tự',
            're_password.required'      => 'Vui lòng nhập lại mật khẩu',
            're_password.same'          => 'Mật khẩu nhập lại không khớp',

        ];
    }
}
