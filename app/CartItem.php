<?php
namespace App;

class CartItem{
    public $sanphams = null;
    public $tong_tien = 0;
    public $tong_so_luong = 0;

// Tóm lại, việc sử dụng __construct là quan trọng để thực hiện các tác vụ khởi tạo, thiết lập trạng thái ban đầu cho đối tượng và đảm bảo tính toàn vẹn của constructor khi được kế thừa trong lớp con.
    public function __construct ($cart){
        if ($cart) {
            $this->sanphams = $cart->sanphams;
            $this->tong_tien = $cart->tong_tien;
            $this->tong_so_luong = $cart->tong_so_luong;
        }
    }

    public function AddCart($sanpham, $id){
        // dd($sanpham);
        $san_pham_moi = ['so_luong' => 0, 'gia_ban' => $sanpham->gia_ban ,'thong_tin_san_pham' => $sanpham ];
        if ($this->sanphams) {
            if (array_key_exists($id, $this->sanphams)) {
                $san_pham_moi = $this->sanphams[$id];
            }
        }
        $san_pham_moi['so_luong']++;
        $san_pham_moi['gia_ban'] = $san_pham_moi['so_luong'] * $sanpham->gia_ban;
        $this->sanphams[$id] =  $san_pham_moi;
        $this->tong_tien += $sanpham->gia_ban;
        $this->tong_so_luong++;
    }
}

?>
