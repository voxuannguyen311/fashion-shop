<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class productSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // 1. Khi seeder thì ta muốn xóa toàn bộ dữ liệu ở table đó
        DB::table('sanphams')->delete();

        // Reset id về lại 1
        DB::table('sanphams')->truncate();

        DB::table('sanphams')->insert([
                [
                    'ten_san_pham'  =>"Slim fit modal cotton shirt",
                    'slug_san_pham' =>"Slim-fit-modal-cotton-shirt",
                    'tinh_trang'    =>"1",
                    'gia_ban'       =>59.99,
                    'hinh_anh'      =>"",
                    'mo_ta'         =>"",
                ]
        ]);
    }
}
