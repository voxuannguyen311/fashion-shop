<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sanphams', function (Blueprint $table) {
            $table->id();
            $table->string('ten_san_pham');
            $table->integer('the_loai');
            $table->string('slug_san_pham')->unique();
            $table->integer('tinh_trang');
            $table->integer('gia_ban');
            $table->string('hinh_anh');
            $table->text('mo_ta');
            $table->integer('so_luong');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sanphams');
    }
};
