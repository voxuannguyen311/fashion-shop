<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('binh_luans', function (Blueprint $table) {
            $table->id();
            $table->string('noi_dung');
            $table->unsignedBigInteger('ma_khach_hang');
            $table->unsignedBigInteger('ma_san_pham');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('binh_luans');
    }
};
